import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacultyAllresultdetailPageRoutingModule } from './faculty-allresultdetail-routing.module';

import { FacultyAllresultdetailPage } from './faculty-allresultdetail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacultyAllresultdetailPageRoutingModule
  ],
  declarations: [FacultyAllresultdetailPage]
})
export class FacultyAllresultdetailPageModule {}
