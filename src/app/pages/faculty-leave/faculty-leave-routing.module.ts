import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacultyLeavePage } from './faculty-leave.page';

const routes: Routes = [
  {
    path: '',
    component: FacultyLeavePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacultyLeavePageRoutingModule {}
