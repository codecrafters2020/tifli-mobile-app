import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacultyTimetablePageRoutingModule } from './faculty-timetable-routing.module';

import { FacultyTimetablePage } from './faculty-timetable.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacultyTimetablePageRoutingModule
  ],
  declarations: [FacultyTimetablePage]
})
export class FacultyTimetablePageModule {}
