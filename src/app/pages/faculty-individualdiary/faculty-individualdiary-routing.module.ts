import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacultyIndividualdiaryPage } from './faculty-individualdiary.page';

const routes: Routes = [
  {
    path: '',
    component: FacultyIndividualdiaryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacultyIndividualdiaryPageRoutingModule {}
