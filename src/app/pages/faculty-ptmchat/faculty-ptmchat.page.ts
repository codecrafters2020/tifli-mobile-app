import { Component,OnInit } from '@angular/core';
import { NavController,LoadingController, ToastController } from '@ionic/angular';
import { Api } from '../../providers/api/api.service';
import { FacultyParamsService } from '../../providers/faculty-params.service';
@Component({
  selector: 'app-faculty-ptmchat',
  templateUrl: './faculty-ptmchat.page.html',
  styleUrls: ['./faculty-ptmchat.page.scss'],
})
export class FacultyPtmchatPage implements OnInit{
  meetingcmnts: any;
  cmntsobject;
  cmntShow: any;
  comment: any;
  meetingid: any;
  subject: any;
  bgparent='assets/img/meeting/surveybox-1.png';
  bgteacher='assets/img/meeting/surveybox.png';
  bgtypemessage='assets/img/meeting/mesagetype.png';

  constructor(
    public navCtrl: NavController,
    public api: Api,
    private loadingController: LoadingController,
    public toastCtrl: ToastController,
    public facultyparams: FacultyParamsService
    ) {
      this.meetingcmnts=[];
      this.cmntShow=[];
      this.cmntsobject=[];
    }

  async ngOnInit() {
    const loading = await this.loadingController.create({
      // content:"loading data..."
    });
    await loading.present();
    if(this.facultyparams.ptm_discussionsubject)
    {
      this.subject=this.facultyparams.ptm_discussionsubject;
    }
    console.log('ionViewDidLoad TeacherLeavemodalPage');
    this.meetingid=this.facultyparams.ptm_discussionmeetingid;
    this.api.get("faculty/parent_teacher_meeting_comments/show_all_comments.json",
    {ptm_id:this.facultyparams.ptm_discussionmeetingid}).subscribe(data=>{
      console.log(data);
      loading.dismiss();
      this.meetingcmnts=data["parent_teacher_meeting_comments"];
      if(this.meetingcmnts)
      {
      this.subject=this.meetingcmnts[0].parent_teacher_meeting.subject;
        for(var i=0;i<this.meetingcmnts.length;i++)
        {


          this.cmntsobject={
            comment:this.meetingcmnts[i].message,sentby:this.meetingcmnts[i].sent_by,
            commentTime:this.meetingcmnts[i].created_at
          };
          this.cmntShow.push(this.cmntsobject);
        }

      }
    },async err=>{
      loading.dismiss();
      let toast = await this.toastCtrl.create({
        message: "Error loading discussion",
        duration: 4000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger",
        color:"danger"
      });
      await toast.present();
    })

  }
 previouspage()
 {
   this.navCtrl.navigateRoot('faculty-ptm');
 }

  async submit()
  {
    const loading = await this.loadingController.create({
      // content:"loading data..."
    });
    await loading.present();

    this.api.post("faculty/parent_teacher_meeting_comments.json",{parent_teacher_meeting_id:this.meetingid,
        sent_by:"teacher",message:this.comment}).subscribe(data=>{
            console.log(data);
            this.meetingcmnts=[];
            this.cmntShow=[];
            this.cmntsobject=[];
            this.comment=""

            this.api.get("faculty/parent_teacher_meeting_comments/show_all_comments.json",
            {ptm_id:this.meetingid}).subscribe(data=>{
              console.log(data);
              loading.dismiss();
              this.meetingcmnts=data["parent_teacher_meeting_comments"];
              if(this.meetingcmnts)
              {
                      this.subject=this.meetingcmnts[0].parent_teacher_meeting.subject;
                for(var i=0;i<this.meetingcmnts.length;i++)
                {


                  this.cmntsobject={
                    comment:this.meetingcmnts[i].message,sentby:this.meetingcmnts[i].sent_by,
                    commentTime:this.meetingcmnts[i].created_at
                  };
                  this.cmntShow.push(this.cmntsobject);
                }

              }
            },async err=>{
              loading.dismiss();
              let toast = await this.toastCtrl.create({
                message: "Error loading discussion",
                duration: 4000,
                position: 'top',
                showCloseButton: true,
                closeButtonText: "x",
                cssClass: "toast-danger",
                color:"danger"
              });
              await toast.present();
            })

          },async err=>{
            loading.dismiss();
            let toast = await this.toastCtrl.create({
              message: "Error sending comment.",
              duration: 4000,
              position: 'top',
              showCloseButton: true,
              closeButtonText: "x",
              cssClass: "toast-danger",
              color:"danger"
            });
            await toast.present();

          });
  }

}
