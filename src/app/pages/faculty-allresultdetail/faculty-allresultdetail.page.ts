import { Component,OnInit } from '@angular/core';
import { NavController, ToastController, LoadingController } from '@ionic/angular';
import { Api } from '../../providers/api/api.service';
import { GlobalVars } from '../../providers/global-vars.service';
import { FacultyParamsService } from 'src/app/providers/faculty-params.service';

@Component({
  selector: 'app-faculty-allresultdetail',
  templateUrl: './faculty-allresultdetail.page.html',
  styleUrls: ['./faculty-allresultdetail.page.scss'],
})
export class FacultyAllresultdetailPage implements OnInit{

  show_result:any=[];
  courseimg='assets/img/course_name.png';
  studentcard='assets/img/student_card.png';
  editResultisTrue=false;
  studentarray:any;
  studentmarksarray: any;
  today: number;
  from_date;
  category: any;
  TotalMarks: any;
  constructor(
    public navCtrl: NavController,
    public api: Api,
    public toastCtrl: ToastController,
    private loadingController: LoadingController,
    public globalVar: GlobalVars,
    public facultyparam: FacultyParamsService

)
    {
      this.show_result=this.facultyparam.result_detail;
      this.studentarray=[];
      this.studentmarksarray=[];
}

  ngOnInit() {
    this.today = Date.now();
    this.category=this.show_result.result_category;
    this.TotalMarks=this.show_result.total_marks;
    this.from_date=this.show_result.created_at;
    console.log(this.show_result.id);
    for(var i=0;i<this.show_result.student_results.length;i++)
    {
        this.studentarray={student_id:this.show_result.student_results[i].student.id,marks_obtained:this.show_result.student_results[i].marks_obtained,
          id:this.show_result.student_results[i].id,student_attendance:this.show_result.student_results[i].student_attendance};
        this.studentmarksarray.push(this.studentarray);
    }
  }
  previouspage()
  {
    this.navCtrl.navigateRoot('faculty-allresult');
  }
  editResult()
  {
    this.editResultisTrue=!this.editResultisTrue;
  }
  async submit_result()
  {
    const loading = await this.loadingController.create({
      // content:"loading data..."
    });
    await loading.present();

    this.api.patch("faculty/result.json",{student_results:this.studentmarksarray,
      result_category:this.category,
      total_marks:this.TotalMarks,
      result_id:this.show_result.id,
      result_date:this.from_date
    })
    .subscribe(data=>{
      console.log(data);
      loading.dismiss();
      this.navCtrl.navigateRoot("faculty-home");
    },async err=>{
      loading.dismiss();
      let toast = await  this.toastCtrl.create({
        message: "Something went wrong. Please Try Later",
        duration: 4000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger",
        color:"danger"
      });
      await toast.present();

    })

  }
}
