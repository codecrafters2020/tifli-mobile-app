import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentDiarydetailPageRoutingModule } from './parent-diarydetail-routing.module';

import { ParentDiarydetailPage } from './parent-diarydetail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentDiarydetailPageRoutingModule
  ],
  declarations: [ParentDiarydetailPage]
})
export class ParentDiarydetailPageModule {}
