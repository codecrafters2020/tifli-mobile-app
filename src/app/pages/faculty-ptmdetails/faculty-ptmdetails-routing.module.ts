import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacultyPtmdetailsPage } from './faculty-ptmdetails.page';

const routes: Routes = [
  {
    path: '',
    component: FacultyPtmdetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacultyPtmdetailsPageRoutingModule {}
