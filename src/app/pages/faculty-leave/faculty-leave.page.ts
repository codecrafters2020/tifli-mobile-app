import { Component,OnInit } from '@angular/core';
import { NavController, ModalController, ToastController } from '@ionic/angular';
import { Api } from '../../providers/api/api.service';
import { GlobalVars } from '../../providers/global-vars.service';
import { ParentParamsService } from '../../providers/parent-params.service';
@Component({
  selector: 'app-faculty-leave',
  templateUrl: './faculty-leave.page.html',
  styleUrls: ['./faculty-leave.page.scss'],
})
export class FacultyLeavePage implements OnInit{
  requestedleave: any;
  showgrid: any;
  leavestatus: any;
  someAutoFormattedInput: string;

  constructor(
    public navCtrl: NavController,
    public globalVar: GlobalVars,
    public api: Api,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public facultyparams: ParentParamsService
  ) {
  }

  ngOnInit() {
    console.log('ionViewDidLoad TeacherLeavePage');
    this.api.get("faculty/leave_applications/show_all_requested_leaves.json",
      { class_section_id: this.globalVar.current_user.class_section.id }).subscribe(data => {
        console.log(data);
        this.requestedleave = data["leave_applications"];
      })
  }
  showModal(i) {
    // this.showgrid = this.modalCtrl.create("TeacherLeavemodalPage", { request_leave: this.requestedleave[i] })
    // this.showgrid.present();

  }
  get sortData() {
    return this.requestedleave.sort((a, b) => {
      return <any>new Date(b.start_date) - <any>new Date(a.start_date);
    });
  }
  changestatus(status, i) {
    this.leavestatus = status;
    this.api.patch("faculty/leave_applications/update_leave_status.json",
      { leave_application_id: this.requestedleave[i].id, leave_status: status }).subscribe(async data => {
        console.log(data);
        let toast = await this.toastCtrl.create({
          message: "Leave Status Updated",
          duration: 4000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-success",
          color:"success"
        });
        toast.present();
        this.api.get("faculty/leave_applications/show_all_requested_leaves.json",
          { class_section_id: this.globalVar.current_user.class_section.id }).subscribe(data => {
            console.log(data);
            this.requestedleave = data["leave_applications"];
          })
      }, async err => {
        let toast = await this.toastCtrl.create({
          message: "Something went wrong",
          duration: 4000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger",
          color:"danger"
        });
        toast.present();
      })
  }

  imageShow(img) {
    this.facultyparams.doc_img = img;
    this.navCtrl.navigateRoot('document-image');
  }
  onAutoFormatChanged() {
    this.someAutoFormattedInput = this.setFirstLetterToUppercase(this.someAutoFormattedInput);
  }

  private setFirstLetterToUppercase(string: string): string {
    // https://dzone.com/articles/how-to-capitalize-the-first-letter-of-a-string-in
    return string.charAt(0).toUpperCase() + string.slice(1);
  }
  openPage()
  {
    this.navCtrl.navigateRoot('notifications');
  }
}
