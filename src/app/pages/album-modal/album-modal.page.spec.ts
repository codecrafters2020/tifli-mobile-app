import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AlbumModalPage } from './album-modal.page';

describe('AlbumModalPage', () => {
  let component: AlbumModalPage;
  let fixture: ComponentFixture<AlbumModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlbumModalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AlbumModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
