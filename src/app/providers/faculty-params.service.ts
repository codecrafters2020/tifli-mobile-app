import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FacultyParamsService {

  album_array;
  album_create_array;
  create_result_students_array;
  result_id;
  result_category;
  result_totalmarks;
  result_coursename;
  result_date;
  result_classsectionid;
  previousdiarystatus;
  notice_individualcourseid;
  notice_individualstudents;
  notice_individualclass;
  notice_individualsection;
  notice_individual;
  ptm_details;
  ptm_discussionmeetingid;
  ptm_discussionsubject;
  event_detail;
  albummodal_img;
  result_detail;
  addmoreimagesalbum;
  constructor() {
    this.album_array = [];
    this.album_create_array = [];
    this.create_result_students_array = [];
    this.result_id = '';
    this.result_category = '';
    this.result_totalmarks = '';
    this.result_coursename = '';
    this.result_date = '';
    this.result_classsectionid = '';
    this.previousdiarystatus = '';
    this.notice_individualclass = '';
    this.notice_individualsection = '';
    this.notice_individualstudents = [];
    this.notice_individualcourseid = '';
    this.ptm_details = [];
    this.ptm_discussionmeetingid = '';
    this.ptm_discussionsubject = '';
    this.event_detail = [];
    this.albummodal_img = [];
    this.result_detail=[];
    this.addmoreimagesalbum="";
  }
}
