import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AttachedimageshowPageRoutingModule } from './attachedimageshow-routing.module';

import { AttachedimageshowPage } from './attachedimageshow.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AttachedimageshowPageRoutingModule
  ],
  declarations: [AttachedimageshowPage]
})
export class AttachedimageshowPageModule {}
