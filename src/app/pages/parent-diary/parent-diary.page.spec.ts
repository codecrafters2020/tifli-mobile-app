import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ParentDiaryPage } from './parent-diary.page';

describe('ParentDiaryPage', () => {
  let component: ParentDiaryPage;
  let fixture: ComponentFixture<ParentDiaryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentDiaryPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ParentDiaryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
