import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacultyNewresultPageRoutingModule } from './faculty-newresult-routing.module';

import { FacultyNewresultPage } from './faculty-newresult.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacultyNewresultPageRoutingModule
  ],
  declarations: [FacultyNewresultPage]
})
export class FacultyNewresultPageModule {}
