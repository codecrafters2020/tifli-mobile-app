import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParentMapPage } from './parent-map.page';

const routes: Routes = [
  {
    path: '',
    component: ParentMapPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParentMapPageRoutingModule {}
