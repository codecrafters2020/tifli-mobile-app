import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ParentStudentsummaryPage } from './parent-studentsummary.page';

describe('ParentStudentsummaryPage', () => {
  let component: ParentStudentsummaryPage;
  let fixture: ComponentFixture<ParentStudentsummaryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentStudentsummaryPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ParentStudentsummaryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
