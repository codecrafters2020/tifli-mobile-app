import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParentPtmchatPage } from './parent-ptmchat.page';

const routes: Routes = [
  {
    path: '',
    component: ParentPtmchatPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParentPtmchatPageRoutingModule {}
