import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacultyLeavePage } from './faculty-leave.page';

describe('FacultyLeavePage', () => {
  let component: FacultyLeavePage;
  let fixture: ComponentFixture<FacultyLeavePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacultyLeavePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacultyLeavePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
