import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentDiaryPageRoutingModule } from './parent-diary-routing.module';

import { ParentDiaryPage } from './parent-diary.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentDiaryPageRoutingModule
  ],
  declarations: [ParentDiaryPage]
})
export class ParentDiaryPageModule {}
