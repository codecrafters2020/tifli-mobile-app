import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacultyLeavePageRoutingModule } from './faculty-leave-routing.module';

import { FacultyLeavePage } from './faculty-leave.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacultyLeavePageRoutingModule
  ],
  declarations: [FacultyLeavePage]
})
export class FacultyLeavePageModule {}
