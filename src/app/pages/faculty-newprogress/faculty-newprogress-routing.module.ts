import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacultyNewprogressPage } from './faculty-newprogress.page';

const routes: Routes = [
  {
    path: '',
    component: FacultyNewprogressPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacultyNewprogressPageRoutingModule {}
