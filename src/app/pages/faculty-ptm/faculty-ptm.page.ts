import { Component,OnInit } from '@angular/core';
import { NavController,ModalController } from '@ionic/angular';
import { Api } from '../../providers/api/api.service';
import { GlobalVars } from '../../providers/global-vars.service';
import { FacultyParamsService } from '../../providers/faculty-params.service';
import * as moment from 'moment';
@Component({
  selector: 'app-faculty-ptm',
  templateUrl: './faculty-ptm.page.html',
  styleUrls: ['./faculty-ptm.page.scss'],
})
export class FacultyPtmPage implements OnInit{
  today: number;
  showgrid: any;
  apigetptm: any;
  objectget
  ptmShow: any;
  doodle ='assets/img/bgcover.png';
  subjectCover='assets/img/meeting/statusapp.png';
  filtermonthwise;
  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public api: Api,
    public globalVar: GlobalVars,
    public facultyparams: FacultyParamsService
    ) {
      this.apigetptm=[];
      this.objectget=[];
      this.ptmShow=[];

  }

  ngOnInit() {
    console.log('ionViewDidLoad TeacherCommentPage');
    this.today=Date.now();
    var date=new Date();
    var d=moment(date);
    this.loadResource(d.month()+1);

  }
  loadResource(month)
  {
    this.ptmShow=[];
    this.api.get("faculty/parent_teacher_meetings/show_all_requested_meetings.json",
    {faculty_id:this.globalVar.current_user.information.id,month:month}).subscribe(data=>{
      console.log(data);
      this.apigetptm= data["parent_teacher_meetings"];
      var appointmentdate,appointmenttime
      for(var i=0;i<this.apigetptm.length;i++)
      {
        if(this.apigetptm[i].appointment_date==null)
        {
            appointmentdate="NA";
            appointmenttime="NA"
        }
        else
        {
          appointmentdate=this.apigetptm[i].appointment_date;
          var t = this.apigetptm[i].appointment_time.split("T")
          var x = t[1].split(".")
          appointmenttime=x[0];
        }

          this.objectget = {subject:this.apigetptm[i].subject,createdat:this.apigetptm[i].created_at,
          description:this.apigetptm[i].description,
          studentname:this.apigetptm[i].student_name,
          facultyname:this.apigetptm[i].faculty.faculty_name,
          id:this.apigetptm[i].id,appointmentdate:appointmentdate,appointmenttime:appointmenttime};
          this.ptmShow.push(this.objectget);
      }
    });
  }
  get sortData() {
    return this.ptmShow.sort((a, b) => {
      return <any>new Date(b.createdat) - <any>new Date(a.createdat);
    });
  }

  showModal(i)
  {
      this.facultyparams.ptm_details = this.ptmShow[i];
      this.navCtrl.navigateRoot('faculty-ptmdetails');
  }
  newMeeting()
  {
    this.navCtrl.navigateRoot('faculty-newptm');
  }

}
