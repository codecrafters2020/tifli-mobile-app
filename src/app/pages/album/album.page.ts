import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { NavController, LoadingController, ToastController, AlertController } from '@ionic/angular';
import { Api } from '../../providers/api/api.service';
import { GlobalVars } from '../../providers/global-vars.service';
import * as moment from 'moment';
import { FacultyParamsService } from '../../providers/faculty-params.service';
@Component({
  selector: 'app-album',
  templateUrl: './album.page.html',
  styleUrls: ['./album.page.scss'],
})
export class AlbumPage implements OnInit{

  showalbumcover: any;
  server: any;
  albumcover: {};
  albumcovershowonpage;
  edit: boolean;
  id: any;
  alert: any;
  filtermonthwise;
  constructor(
    public navCtrl: NavController,
    public api: Api,
    private loadingController: LoadingController,
    public globalVar: GlobalVars,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public facultyparams:FacultyParamsService
  ) {
    this.showalbumcover = [];
    this.albumcover = [];
    this.albumcovershowonpage = [];
    this.edit = false;
  }

  async ngOnInit() {

    console.log('ionViewDidLoad AlbumPage');
    this.server = this.globalVar.server_link;
    var date=new Date();
    var d=moment(date);
    this.getAlbum(d.month()+1)
  }
  async getAlbum(month)
  {
    const loading = await this.loadingController.create({
      // content:"loading data..."
    });
    await loading.present();

    this.albumcovershowonpage=[];
    this.api.get("faculty/albums/show_all_albums.json",{month:month}).subscribe(async data=>{
      console.log(data);
      loading.dismiss();
      if(data["albums"])
      {
          this.showalbumcover = data["albums"];
          for(var i=0;i<this.showalbumcover.length;i++)
          {
            if(this.showalbumcover[i].pictures[0]!=undefined)
            {
              this.albumcover = {image:this.showalbumcover[i].pictures[0].image_url,
                albumname:this.showalbumcover[i].album_name,id:this.showalbumcover[i].id,images:
                this.showalbumcover[i].pictures};
                this.albumcovershowonpage.push(this.albumcover);

            }

          }
      }
      else{
        let toast = await this.toastCtrl.create({
          message:"No Albums Found For This Month",
          duration: 4000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger",
          color:"danger"
        });
        await toast.present();

      }
    },async err=>{
      loading.dismiss();
      let toast = await this.toastCtrl.create({
        message:"Something went wrong",
        duration: 4000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger",
        color:"danger"
      });
      await toast.present();

    });
  }
  albumDetail() {
    this.navCtrl.navigateRoot('album-detail-faculty');
  }
  albumShow(i) {
    if (this.edit == false) {
      this.facultyparams.album_array=this.albumcovershowonpage[i];
      this.navCtrl.navigateRoot('album-detail-show');
    }
  }
  // selectPhoto(id)
  // {
  //      this.id=id;
  // }
  async deletePhoto(id) {
    this.alert = await this.alertCtrl.create({
      header: "Delete Album",
      message: "Do you want to delete the album?",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            this.alert = "";
          }
        },
        {
          text: "Delete Album",
          handler: async () => {
            const loading = await this.loadingController.create({
            });
            loading.present();
            let now = moment().format('DD-MMM-YYYY HH:mm:ss');
            this.api.patch("faculty/albums/delete_album", { current_time: now, album_id: id }).subscribe(async data => {
              console.log(data);
              loading.dismiss()
              this.navCtrl.navigateRoot('faculty-home');
              let toast = await this.toastCtrl.create({
                message: "Album deleted successfully",
                duration: 4000,
                position: 'top',
                showCloseButton: true,
                closeButtonText: "x",
                cssClass: "toast-success",
                color:"success"

              });
              toast.present();
            }, async err => {
              loading.dismiss()
              let toast = await this.toastCtrl.create({
                message: "Something went wrong",
                duration: 4000,
                position: 'top',
                showCloseButton: true,
                closeButtonText: "x",
                cssClass: "toast-danger",
                color:"danger"
              });
              toast.present();

            })

          }
        }
      ],
      // enableBackdropDismiss: false
    });
    this.alert.present();


  }
  editTrue() {
    if (this.edit == true) {
      this.edit = false
    }
    else {
      this.edit = true
    }
  }


}
