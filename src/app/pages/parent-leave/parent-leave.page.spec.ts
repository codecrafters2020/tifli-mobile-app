import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ParentLeavePage } from './parent-leave.page';

describe('ParentLeavePage', () => {
  let component: ParentLeavePage;
  let fixture: ComponentFixture<ParentLeavePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentLeavePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ParentLeavePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
