import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { Api } from '../../providers/api/api.service';
import { GlobalVars } from '../../providers/global-vars.service';
import { FacultyParamsService } from '../../providers/faculty-params.service';
import * as moment from 'moment';
import { ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-faculty-timetable',
  templateUrl: './faculty-timetable.page.html',
  styleUrls: ['./faculty-timetable.page.scss'],
})
export class FacultyTimetablePage implements OnInit {

  @ViewChild('slides1') slides: IonSlides;
  timetable: any;
  starttiembg = 'assets/img/startingtimecard.png';
  endtimecardbg = 'assets/img/endtimecard.png';
  middletimebg = 'assets/img/middletimecard.png';
  length: any;
  today: number;
  timetableshowmonday = [];
  timetableshowtuesday = [];
  timetableshowwednesday = [];
  timetableshowthursday = [];
  timetableshowfriday = [];
  timetableshowsaturday = [];
  timetableshowsunday = [];
  timetableshow;
  initialSlide: number;
  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public api: Api,
    public globalVar: GlobalVars,
    public facultyparams: FacultyParamsService
  ) {
    this.timetable = [];
    this.timetableshowmonday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T08:00:00.000Z", end_time: "0000-00-00T09:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowmonday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T09:00:00.000Z", end_time: "0000-00-00T10:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowmonday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T10:00:00.000Z", end_time: "0000-00-00T11:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowmonday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T11:00:00.000Z", end_time: "0000-00-00T12:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowmonday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T12:00:00.000Z", end_time: "0000-00-00T01:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowmonday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T01:00:00.000Z", end_time: "0000-00-00T02:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowmonday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T02:00:00.000Z", end_time: "0000-00-00T03:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowmonday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T03:00:00.000Z", end_time: "0000-00-00T04:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowmonday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T04:00:00.000Z", end_time: "0000-00-00T05:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowtuesday.push({
      day: 1, slot: 0,
      start_time: "0000-00-00T08:00:00.000Z", end_time: "0000-00-00T09:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowtuesday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T09:00:00.000Z", end_time: "0000-00-00T10:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowtuesday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T10:00:00.000Z", end_time: "0000-00-00T11:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowtuesday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T11:00:00.000Z", end_time: "0000-00-00T12:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowtuesday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T12:00:00.000Z", end_time: "0000-00-00T01:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowtuesday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T01:00:00.000Z", end_time: "0000-00-00T02:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowtuesday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T02:00:00.000Z", end_time: "0000-00-00T03:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowtuesday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T03:00:00.000Z", end_time: "0000-00-00T04:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowtuesday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T04:00:00.000Z", end_time: "0000-00-00T05:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowwednesday.push({
      day: 2, slot: 0,
      start_time: "0000-00-00T08:00:00.000Z", end_time: "0000-00-00T09:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowwednesday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T09:00:00.000Z", end_time: "0000-00-00T10:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowwednesday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T10:00:00.000Z", end_time: "0000-00-00T11:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowwednesday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T11:00:00.000Z", end_time: "0000-00-00T12:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowwednesday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T12:00:00.000Z", end_time: "0000-00-00T01:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowwednesday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T01:00:00.000Z", end_time: "0000-00-00T02:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowwednesday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T02:00:00.000Z", end_time: "0000-00-00T03:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowwednesday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T03:00:00.000Z", end_time: "0000-00-00T04:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowwednesday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T04:00:00.000Z", end_time: "0000-00-00T05:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowthursday.push({
      day: 3, slot: 0,
      start_time: "0000-00-00T08:00:00.000Z", end_time: "0000-00-00T09:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowthursday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T09:00:00.000Z", end_time: "0000-00-00T10:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowthursday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T10:00:00.000Z", end_time: "0000-00-00T11:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowthursday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T11:00:00.000Z", end_time: "0000-00-00T12:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowthursday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T12:00:00.000Z", end_time: "0000-00-00T01:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowthursday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T01:00:00.000Z", end_time: "0000-00-00T02:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowthursday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T02:00:00.000Z", end_time: "0000-00-00T03:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowthursday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T03:00:00.000Z", end_time: "0000-00-00T04:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowthursday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T04:00:00.000Z", end_time: "0000-00-00T05:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowfriday.push({
      day: 4, slot: 0,
      start_time: "0000-00-00T08:00:00.000Z", end_time: "0000-00-00T09:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowfriday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T09:00:00.000Z", end_time: "0000-00-00T10:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowfriday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T10:00:00.000Z", end_time: "0000-00-00T11:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowfriday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T11:00:00.000Z", end_time: "0000-00-00T12:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowfriday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T12:00:00.000Z", end_time: "0000-00-00T01:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowfriday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T01:00:00.000Z", end_time: "0000-00-00T02:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowfriday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T02:00:00.000Z", end_time: "0000-00-00T03:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowfriday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T03:00:00.000Z", end_time: "0000-00-00T04:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowfriday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T04:00:00.000Z", end_time: "0000-00-00T05:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowsaturday.push({
      day: 5, slot: 0,
      start_time: "0000-00-00T08:00:00.000Z", end_time: "0000-00-00T09:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowsaturday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T09:00:00.000Z", end_time: "0000-00-00T10:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowsaturday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T10:00:00.000Z", end_time: "0000-00-00T11:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowsaturday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T11:00:00.000Z", end_time: "0000-00-00T12:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowsaturday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T12:00:00.000Z", end_time: "0000-00-00T01:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowsaturday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T01:00:00.000Z", end_time: "0000-00-00T02:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowsaturday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T02:00:00.000Z", end_time: "0000-00-00T03:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowsaturday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T03:00:00.000Z", end_time: "0000-00-00T04:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowsaturday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T04:00:00.000Z", end_time: "0000-00-00T05:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowsunday.push({
      day: 6, slot: 0,
      start_time: "0000-00-00T08:00:00.000Z", end_time: "0000-00-00T09:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowsunday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T09:00:00.000Z", end_time: "0000-00-00T10:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowsunday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T10:00:00.000Z", end_time: "0000-00-00T11:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowsunday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T11:00:00.000Z", end_time: "0000-00-00T12:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowsunday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T12:00:00.000Z", end_time: "0000-00-00T01:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowsunday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T01:00:00.000Z", end_time: "0000-00-00T02:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowsunday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T02:00:00.000Z", end_time: "0000-00-00T03:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowsunday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T03:00:00.000Z", end_time: "0000-00-00T04:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshowsunday.push({
      day: 0, slot: 0,
      start_time: "0000-00-00T04:00:00.000Z", end_time: "0000-00-00T05:00:00.000Z",
      class_name: "0", subject: "FREE"
    });
    this.timetableshow = [];
  }

  ngOnInit() {
    this.today = Date.now();
    console.log('ionViewDidLoad FacultyTimetablePage');
    this.api.get("faculty/faculty_timetable/get_faculty_timetable.json",
      { faculty_id: this.globalVar.current_user.information.id }).subscribe(data => {
        console.log(data);
        this.timetable = data["time_tables"];
        this.length = this.timetable.length;
        for (var i = 0; i < this.length; i++) {
          if (this.timetable[i].x == 0) {
            this.timetableshowmonday[this.timetable[i].y].start_time = this.timetable[i].start_time;
            this.timetableshowmonday[this.timetable[i].y].end_time = this.timetable[i].end_time;
            this.timetableshowmonday[this.timetable[i].y].class_name = this.timetable[i].class_name;
            this.timetableshowmonday[this.timetable[i].y].subject = this.timetable[i].section_course.course.course_name;
            // var timetableobj = {day:0,slot:this.timetable[i].y,
            //   start_time:this.timetable[i].start_time,end_time:this.timetable[i].end_time,
            //   class_name:this.timetable[i].class_name,subject:this.timetable[i].section_course.course.course_name};
            // this.timetableshowmonday.push(timetableobj);
          }
          else if (this.timetable[i].x == 1) {
            this.timetableshowtuesday[this.timetable[i].y].start_time = this.timetable[i].start_time;
            this.timetableshowtuesday[this.timetable[i].y].end_time = this.timetable[i].end_time;
            this.timetableshowtuesday[this.timetable[i].y].class_name = this.timetable[i].class_name;
            this.timetableshowtuesday[this.timetable[i].y].subject = this.timetable[i].section_course.course.course_name;
          }
          else if (this.timetable[i].x == 2) {
            this.timetableshowwednesday[this.timetable[i].y].start_time = this.timetable[i].start_time;
            this.timetableshowwednesday[this.timetable[i].y].end_time = this.timetable[i].end_time;
            this.timetableshowwednesday[this.timetable[i].y].class_name = this.timetable[i].class_name;
            this.timetableshowwednesday[this.timetable[i].y].subject = this.timetable[i].section_course.course.course_name;
          }
          else if (this.timetable[i].x == 3) {
            this.timetableshowthursday[this.timetable[i].y].start_time = this.timetable[i].start_time;
            this.timetableshowthursday[this.timetable[i].y].end_time = this.timetable[i].end_time;
            this.timetableshowthursday[this.timetable[i].y].class_name = this.timetable[i].class_name;
            this.timetableshowthursday[this.timetable[i].y].subject = this.timetable[i].section_course.course.course_name;
          }
          else if (this.timetable[i].x == 4) {
            this.timetableshowfriday[this.timetable[i].y].start_time = this.timetable[i].start_time;
            this.timetableshowfriday[this.timetable[i].y].end_time = this.timetable[i].end_time;
            this.timetableshowfriday[this.timetable[i].y].class_name = this.timetable[i].class_name;
            this.timetableshowfriday[this.timetable[i].y].subject = this.timetable[i].section_course.course.course_name;
          }
          else if (this.timetable[i].x == 5) {
            this.timetableshowsaturday[this.timetable[i].y].start_time = this.timetable[i].start_time;
            this.timetableshowsaturday[this.timetable[i].y].end_time = this.timetable[i].end_time;
            this.timetableshowsaturday[this.timetable[i].y].class_name = this.timetable[i].class_name;
            this.timetableshowsaturday[this.timetable[i].y].subject = this.timetable[i].section_course.course.course_name;
          }
          else if (this.timetable[i].x == 6) {
            this.timetableshowsunday[this.timetable[i].y].start_time = this.timetable[i].start_time;
            this.timetableshowsunday[this.timetable[i].y].end_time = this.timetable[i].end_time;
            this.timetableshowsunday[this.timetable[i].y].class_name = this.timetable[i].class_name;
            this.timetableshowsunday[this.timetable[i].y].subject = this.timetable[i].section_course.course.course_name;
          }
        }
        this.timetableshow.push(this.timetableshowmonday);
        this.timetableshow.push(this.timetableshowtuesday);
        this.timetableshow.push(this.timetableshowwednesday);
        this.timetableshow.push(this.timetableshowthursday);
        this.timetableshow.push(this.timetableshowfriday);
        this.timetableshow.push(this.timetableshowsaturday);
        this.timetableshow.push(this.timetableshowsunday);
        console.log(this.timetableshow);
        this.initialSlide = moment().isoWeekday();
        this.nextslide((this.initialSlide-1));

        // var startTime = this.timetable[2].start_time;
        //     var endTime = this.timetable[2].end_time;

        //     var currentDate = new Date()

        //     var startDate = new Date(currentDate.getTime());
        //     startDate.setHours(startTime.split(":")[0]);
        //     startDate.setMinutes(startTime.split(":")[1]);
        //     startDate.setSeconds(startTime.split(":")[2]);

        //     var endDate = new Date(currentDate.getTime());
        //     endDate.setHours(endTime.split(":")[0]);
        //     endDate.setMinutes(endTime.split(":")[1]);
        //     endDate.setSeconds(endTime.split(":")[2]);


        //     var valid = startDate < currentDate && endDate > currentDate
      });

  }
  nextslide(index) {
    this.slides.slideTo(index, 2000);
    // this.slides.initialSlide=index;
  }

}
