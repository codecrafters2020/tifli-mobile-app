import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentSurveydetailPageRoutingModule } from './parent-surveydetail-routing.module';

import { ParentSurveydetailPage } from './parent-surveydetail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentSurveydetailPageRoutingModule
  ],
  declarations: [ParentSurveydetailPage]
})
export class ParentSurveydetailPageModule {}
