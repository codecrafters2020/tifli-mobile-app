import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacultyPtmdetailsPageRoutingModule } from './faculty-ptmdetails-routing.module';

import { FacultyPtmdetailsPage } from './faculty-ptmdetails.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacultyPtmdetailsPageRoutingModule
  ],
  declarations: [FacultyPtmdetailsPage]
})
export class FacultyPtmdetailsPageModule {}
