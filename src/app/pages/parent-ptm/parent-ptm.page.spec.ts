import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ParentPtmPage } from './parent-ptm.page';

describe('ParentPtmPage', () => {
  let component: ParentPtmPage;
  let fixture: ComponentFixture<ParentPtmPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentPtmPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ParentPtmPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
