import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParentNoticePage } from './parent-notice.page';

const routes: Routes = [
  {
    path: '',
    component: ParentNoticePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParentNoticePageRoutingModule {}
