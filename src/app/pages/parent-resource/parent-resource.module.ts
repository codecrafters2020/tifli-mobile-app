import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentResourcePageRoutingModule } from './parent-resource-routing.module';

import { ParentResourcePage } from './parent-resource.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentResourcePageRoutingModule
  ],
  declarations: [ParentResourcePage]
})
export class ParentResourcePageModule {}
