import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParentPaymentPage } from './parent-payment.page';

const routes: Routes = [
  {
    path: '',
    component: ParentPaymentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParentPaymentPageRoutingModule {}
