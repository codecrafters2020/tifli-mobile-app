import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentAlbumdetailPageRoutingModule } from './parent-albumdetail-routing.module';

import { ParentAlbumdetailPage } from './parent-albumdetail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentAlbumdetailPageRoutingModule
  ],
  declarations: [ParentAlbumdetailPage]
})
export class ParentAlbumdetailPageModule {}
