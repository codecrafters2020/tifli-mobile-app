import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentStudentsummaryPageRoutingModule } from './parent-studentsummary-routing.module';

import { ParentStudentsummaryPage } from './parent-studentsummary.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentStudentsummaryPageRoutingModule
  ],
  declarations: [ParentStudentsummaryPage]
})
export class ParentStudentsummaryPageModule {}
