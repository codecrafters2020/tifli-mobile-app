import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ParentMapPage } from './parent-map.page';

describe('ParentMapPage', () => {
  let component: ParentMapPage;
  let fixture: ComponentFixture<ParentMapPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentMapPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ParentMapPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
