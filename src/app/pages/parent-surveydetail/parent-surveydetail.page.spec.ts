import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ParentSurveydetailPage } from './parent-surveydetail.page';

describe('ParentSurveydetailPage', () => {
  let component: ParentSurveydetailPage;
  let fixture: ComponentFixture<ParentSurveydetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentSurveydetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ParentSurveydetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
