import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DocumentImagePage } from './document-image.page';

const routes: Routes = [
  {
    path: '',
    component: DocumentImagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DocumentImagePageRoutingModule {}
