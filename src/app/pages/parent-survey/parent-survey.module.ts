import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentSurveyPageRoutingModule } from './parent-survey-routing.module';

import { ParentSurveyPage } from './parent-survey.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentSurveyPageRoutingModule
  ],
  declarations: [ParentSurveyPage]
})
export class ParentSurveyPageModule {}
