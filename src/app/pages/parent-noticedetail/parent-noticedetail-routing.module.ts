import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParentNoticedetailPage } from './parent-noticedetail.page';

const routes: Routes = [
  {
    path: '',
    component: ParentNoticedetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParentNoticedetailPageRoutingModule {}
