import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AlbumDetailFacultyPage } from './album-detail-faculty.page';

const routes: Routes = [
  {
    path: '',
    component: AlbumDetailFacultyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AlbumDetailFacultyPageRoutingModule {}
