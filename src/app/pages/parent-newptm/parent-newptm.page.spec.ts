import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ParentNewptmPage } from './parent-newptm.page';

describe('ParentNewptmPage', () => {
  let component: ParentNewptmPage;
  let fixture: ComponentFixture<ParentNewptmPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentNewptmPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ParentNewptmPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
