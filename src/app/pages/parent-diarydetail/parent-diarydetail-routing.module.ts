import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParentDiarydetailPage } from './parent-diarydetail.page';

const routes: Routes = [
  {
    path: '',
    component: ParentDiarydetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParentDiarydetailPageRoutingModule {}
