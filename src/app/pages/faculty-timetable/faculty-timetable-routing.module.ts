import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacultyTimetablePage } from './faculty-timetable.page';

const routes: Routes = [
  {
    path: '',
    component: FacultyTimetablePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacultyTimetablePageRoutingModule {}
