import { Component,OnInit } from '@angular/core';
import { NavController, LoadingController, ToastController } from '@ionic/angular';
import { GlobalVars } from '../../providers/global-vars.service';
import { Api } from '../../providers/api/api.service';
import * as moment from 'moment';

@Component({
  selector: 'app-faculty-attendance',
  templateUrl: './faculty-attendance.page.html',
  styleUrls: ['./faculty-attendance.page.scss'],
})
export class FacultyAttendancePage implements OnInit{
  search;
  today: any;
  StudentNames: any;

  student_attendance: any;
  students: any;
  global: any;
  len: any;
  class: any;
  section: any;
  leavebydate: any;
  attendance_updated_earlier: any;
  student_attendance_updated: any;
  student_attendance_updated_show: any;
  filtermonthwise;
  constructor(
    public navCtrl: NavController,
    public globalVar: GlobalVars,
    public api: Api,
    private loadingController: LoadingController,
    public toastCtrl: ToastController,
  ) {
    this.student_attendance_updated = [];
    this.student_attendance_updated_show = [];
    this.StudentNames = [];
    this.student_attendance = [];
    this.students = [];
    this.global = [];
    this.leavebydate = [];
    this.attendance_updated_earlier = false;
  }

  async ngOnInit() {
    let now = moment().format('DD-MMM-YYYY ');
    this.today = Date.now();
    this.filtermonthwise=this.today;
    if (this.globalVar.current_user.section_students != null) {
      var len = this.globalVar.current_user.section_students.length
      this.students = this.globalVar.current_user.section_students;
      this.global = this.globalVar.current_user.class_section;
      this.class = this.globalVar.current_user.class_section.school_class.class_name;
      this.section = this.globalVar.current_user.class_section.section.section_name;
      this.len = this.student_attendance.length;
      this.api.get("faculty/student_attendance/get_attendance_by_date.json", {
        current_date: now,
        class_section_id: this.globalVar.current_user.class_section.id
      }).subscribe(data => {
        console.log(data);
        this.student_attendance_updated_show = data["student_attendance"];
        if (data["attendance_for_today"] != false) {
          this.attendance_updated_earlier = true;
        }
        else {
          this.attendance_updated_earlier = false;
        }
        if (data["attendance_for_today"] == false) {
          this.api.get("faculty/leave_applications/get_leaves_by_date.json", { leave_date: now }).subscribe(data => {
            console.log(data["leave_applications"]);
            this.leavebydate = data["leave_applications"];
            if (this.leavebydate.length != 0) {
              for (var i = 0; i < len; i++) {
                if (this.leavebydate[i] != null) {
                  if (this.leavebydate[i].student.id == this.globalVar.current_user.section_students[i].id) {
                    this.StudentNames = { attendance: '3', student_id: this.globalVar.current_user.section_students[i].id };
                    this.student_attendance.push(this.StudentNames);
                  }
                  else {
                    this.StudentNames = { attendance: '2', student_id: this.globalVar.current_user.section_students[i].id };
                    this.student_attendance.push(this.StudentNames);
                  }
                }
                else {
                  this.StudentNames = { attendance: '2', student_id: this.globalVar.current_user.section_students[i].id };
                  this.student_attendance.push(this.StudentNames);
                }

              }
            }
            else {
              for (var i = 0; i < len; i++) {
                this.StudentNames = { attendance: '2', student_id: this.globalVar.current_user.section_students[i].id };
                this.student_attendance.push(this.StudentNames);
              }
            }

          });

        }
      });

    }
    else {
      let toast = await this.toastCtrl.create({
        message: "No section has been assigned to you ",
        duration: 4000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
//        cssClass: "toast-danger",
        color:"danger"

      });
      await toast.present();
      this.navCtrl.navigateRoot('faculty-home');

    }

    console.log(this.StudentNames);
  }
  attendanceKeeper(index, attendance) {
    console.log("index: " + index + " " + "attendance: " + attendance);
    this.student_attendance[index].attendance = attendance;
    console.log(this.student_attendance);
  }
  async submitAttendance() {
    const loading = await this.loadingController.create({
    });
    await loading.present();

    this.api.post("faculty/student_attendance/create_all.json", {
      student_attendance: this.student_attendance,
      id: this.globalVar.current_user.id
    })
      .subscribe((data) => {
        console.log(data);
        this.navCtrl.navigateRoot('faculty-home');
        loading.dismiss();

      }, err => {
        loading.dismiss();
      });
  }
  async updateAttendance() {
    const loading = await this.loadingController.create({
    });
    await loading.present();
    this.api.patch("faculty/student_attendance/update_attendance.json", {
      student_attendance: this.student_attendance_updated_show
    }).subscribe(data => {
      console.log(data);
      loading.dismiss();
      this.navCtrl.navigateRoot('faculty-home');
      // this.navCtrl.setRoot("HomePage")
    }, err => {
      loading.dismiss();
    })

  }
  attendanceupdater(index, attendance) {
    console.log("index: " + index + " " + "attendance: " + attendance);
    this.student_attendance_updated_show[index].attendance = attendance;
    console.log(this.student_attendance_updated_show);
  }
  loadAttendance(date)
  {
    var datetosend = moment(date).format('DD-MMM-YYYY');
    this.api.get("faculty/student_attendance/get_attendance_by_date.json",{current_date:datetosend,
      class_section_id:this.globalVar.current_user.class_section.id }).subscribe(async data=>{
      this.student_attendance_updated_show=data["student_attendance"];
      console.log(data)
      if(data["attendance_for_today"]!=false)
      {
        this.attendance_updated_earlier=true;
      }
      else
      {
        this.attendance_updated_earlier=false;
       // this.ionViewDidLoad();
        // let now = moment().format('DD-MMM-YYYY ');
        // this.api.get("faculty/student_attendance/get_attendance_by_date.json",{current_date:now,
        //   class_section_id:this.globalVar.current_user.class_section.id }).subscribe(data=>{
        //   });
        let toast = await this.toastCtrl.create({
          message: "Attendance not found",
          duration: 4000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger",
          color:"danger"
        });
        await toast.present();

      }
    });
  }
}
