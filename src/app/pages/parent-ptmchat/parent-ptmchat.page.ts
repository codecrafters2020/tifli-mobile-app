import { Component,OnInit } from '@angular/core';
import { NavController,LoadingController, ToastController } from '@ionic/angular';
import { Api } from '../../providers/api/api.service';
import { ParentParamsService } from 'src/app/providers/parent-params.service';

@Component({
  selector: 'app-parent-ptmchat',
  templateUrl: './parent-ptmchat.page.html',
  styleUrls: ['./parent-ptmchat.page.scss'],
})
export class ParentPtmchatPage implements OnInit{

  meetingcmnts: any;
  cmntShow: any;
  cmntsobject: any;
  meetingid;
  comment: any;
  subject;
  bgparent = 'assets/img/meeting/surveybox-1.png';
  bgteacher = 'assets/img/meeting/surveybox.png';
  bgtypemessage = 'assets/img/meeting/mesagetype.png';
  constructor(
    public navCtrl: NavController,
    public api: Api,
    private loadingController: LoadingController,
    public toastCtrl: ToastController,
    public parentparam : ParentParamsService

  ) {
    this.cmntsobject = [];
    this.cmntShow = [];
    this.meetingcmnts = [];
  }

  async ngOnInit() {
    const loading = await this.loadingController.create({
      // content:"loading data..."
    });
    await loading.present();
    if (this.parentparam.ptm_discussionsubject) {
      this.subject = this.parentparam.ptm_discussionsubject;
    }
    console.log('ionViewDidLoad ParentPtmdiscussionPage');
    this.meetingid = this.parentparam.ptm_discussionid;
    this.api.get("parent/parent_teacher_meeting_comments/show_all_comments.json",
      { ptm_id: this.parentparam.ptm_discussionid }).subscribe(data => {
        console.log(data);
        loading.dismiss();
        this.meetingcmnts=data["parent_teacher_meeting_comments"];
        if(this.meetingcmnts)
        {
        this.subject=this.meetingcmnts[0].parent_teacher_meeting.subject;
          for(var i=0;i<this.meetingcmnts.length;i++)
          {
  
  
            this.cmntsobject={
              comment:this.meetingcmnts[i].message,sentby:this.meetingcmnts[i].sent_by,
              commentTime:this.meetingcmnts[i].created_at
            };
            this.cmntShow.push(this.cmntsobject);
          }
  
        }
      }, async err => {
        loading.dismiss();
        this.navCtrl.pop();
        let toast = await this.toastCtrl.create({
          message: "Error loading discussion",
          duration: 4000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger"
        });
        await toast.present();
      })

  }
  disabled() {
    if (!this.comment) {
      return true;
    }
    else {
      return false;
    }
  }

  async submit() {
    const loading = await this.loadingController.create({
      // content:"loading data..."
    });
    await loading.present();

    this.api.post("parent/parent_teacher_meeting_comments.json", {
      parent_teacher_meeting_id: this.meetingid,
      sent_by: "parent", message: this.comment
    }).subscribe(data => {
      console.log(data);
      this.meetingcmnts = [];
      this.cmntShow = [];
      this.cmntsobject = [];
      this.comment = "";

      this.api.get("parent/parent_teacher_meeting_comments/show_all_comments.json",
        { ptm_id: this.meetingid }).subscribe(data => {
          console.log(data);
          loading.dismiss();
          this.meetingcmnts=data["parent_teacher_meeting_comments"];
          if(this.meetingcmnts)
          {
          this.subject=this.meetingcmnts[0].parent_teacher_meeting.subject;
            for(var i=0;i<this.meetingcmnts.length;i++)
            {
    
    
              this.cmntsobject={
                comment:this.meetingcmnts[i].message,sentby:this.meetingcmnts[i].sent_by,
                commentTime:this.meetingcmnts[i].created_at
              };
              this.cmntShow.push(this.cmntsobject);
            }
    
          }
        }, async err => {
          loading.dismiss();
          this.navCtrl.pop();
          let toast = await this.toastCtrl.create({
            message: "Error loading discussion",
            duration: 4000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-danger"
          });
          await toast.present();
        })

    }, async err => {
      loading.dismiss();
      let toast = await this.toastCtrl.create({
        message: "Something went wrong.",
        duration: 4000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger"
      });
      await toast.present();

    });
  }
  previouspage()
  {
    this.navCtrl.navigateRoot('parent-ptm');
  }
 
}
