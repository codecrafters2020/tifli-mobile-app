import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacultyPtmPage } from './faculty-ptm.page';

const routes: Routes = [
  {
    path: '',
    component: FacultyPtmPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacultyPtmPageRoutingModule {}
