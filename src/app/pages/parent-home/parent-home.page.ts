import { Component, OnInit, ElementRef } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { GlobalVars } from '../../providers/global-vars.service';
import { Api } from '../../providers/api/api.service';
import { ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { AppComponent } from 'src/app/app.component';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-parent-home',
  templateUrl: './parent-home.page.html',
  styleUrls: ['./parent-home.page.scss'],
})
export class ParentHomePage implements OnInit {

  slideOpts = {
    slidesPerView: 1.75,
    speed: 400,
    spaceBetween:-15,
  };
   @ViewChild('slides',{static:true}) slides: IonSlides;
  students: any;
  alert: any;
  item = 'assets/img/bgcover.png';
  constructor(
    public navCtrl: NavController,
    public globalVar: GlobalVars,
    public api: Api,
    public app: AppComponent,
    public storage: Storage,
  ) {
    this.students=[];
  }
  async ngOnInit() {
    this.storage.get('currentuser').then(user => {
      if (user) {
        this.globalVar.current_user = user;
        this.students = this.globalVar.current_user.students;
        this.globalVar.student = this.students[0];
        if(this.globalVar.currentIndexParent)
        {
          this.globalVar.student=this.students[this.globalVar.currentIndexParent];
        }
    
      } else {
         this.openPage('sign-in');
      }
    });
    console.log(await this.slides.getActiveIndex());
}
  ionViewWillEnter() {
    console.log("ionViewWillEnter parent-home");
      this.slides.slideTo(this.globalVar.currentIndexParent,300);  
    //     this.students = this.globalVar.current_user.students;
    // if (this.students) {
    //   this.globalVar.student = this.students[0];
    // }
  }
    slide2items = [
    { img: 'assets/img/album recent.png' },
    { img: 'assets/img/user recent.png' },
    { img: 'assets/img/notice recent.png' },
    { img: 'assets/img/event recent.png' },
    { img: 'assets/img/diary recent.png' },
  ]
  items = [
    { title: 'Album', component: 'parent-album', img: 'assets/img/album.png', icon: '' },
    { title: 'Attendance Report', img: 'assets/img/attendance.png', component: 'attendance-report' },
    { title: 'Child\'s Profile', img: 'assets/img/profile.png', component: 'parent-studentsummary' },
    { title: 'Events', component: 'events', img: 'assets/img/events.png', icon: '' },
    { title: 'Leave', component: 'parent-leave', img: 'assets/img/leave.png', icon: '', attendance: "leave" },
    { title: 'Payment', component: 'parent-payment', img: 'assets/img/payment.png', icon: '' },
    { title: 'Parent Teacher Interaction', component: 'parent-ptm', img: 'assets/img/meeting.png', icon: '' },
    { title: 'Students Diary', component: 'parent-diary', img: 'assets/img/diary.png', icon: '' },
    { title: 'Students Notice', component: 'parent-noticedetail', img: 'assets/img/notice.png', icon: '' },
    { title: 'Survey', component: 'parent-survey', img: 'assets/img/survey-icon.png', icon: '' },
    { title: 'Track Student', component: 'parent-map', img: 'assets/img/tracking.png', icon: '' },
    { title: 'Videos', component: 'parent-allvideo', img: 'assets/img/videos.png', icon: '' },
    { title: 'Resources', component: 'parent-resource', img: 'assets/img/resources.png', icon: '' },
    { title: 'Result', component: 'parent-allresult', img: 'assets/img/result.png', icon: '' },
    { title: 'Time Table', component: 'parent-timetable', img: 'assets/img/timetable.png', icon: '' },
    { title: 'Zoom', component: 'non-login', img: 'assets/img/zoom1.png', icon: '' },
  ]

  nextslide(index) {
    //this.slides.slideTo(index);
  }
  open(page) {
    this.navCtrl.navigateRoot(page);
  }
  openPage(page) {

    this.navCtrl.navigateRoot(page);
  }
  next() {
    this.slides.slideNext();
  }

  next2() {
    // this.slides2.nativeElement.slideNext();
  }
  prev() {
    this.slides.slidePrev();
  }

  selectstudent(i) {
    this.globalVar.student = [];
    this.globalVar.student = this.students[i];
  }
  async slidesload()
  {
    console.log(await this.slides.getActiveIndex());
    
  }
  slideChanged() {
    this.globalVar.student = [];
    this.slides.getActiveIndex().then(currentIndex => {
      this.globalVar.student = this.students[currentIndex];
      this.globalVar.currentIndexParent = currentIndex;
      console.log(this.globalVar.student);
      console.log('Current index is', currentIndex);
    });

  }
  albumpage()
  {
      this.navCtrl.navigateRoot('parent-album');
  }
  diarypage()
  {
    this.navCtrl.navigateRoot('parent-diary');
  }
  profilepage()
  {
    this.navCtrl.navigateRoot('parent-studentsummary');
  }
  noticepage()
  {
    this.navCtrl.navigateRoot('parent-noticedetail');
  }
  eventpage()
  {
    this.navCtrl.navigateRoot('events');
  }
}
