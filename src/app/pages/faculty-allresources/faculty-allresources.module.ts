import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacultyAllresourcesPageRoutingModule } from './faculty-allresources-routing.module';

import { FacultyAllresourcesPage } from './faculty-allresources.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacultyAllresourcesPageRoutingModule
  ],
  declarations: [FacultyAllresourcesPage]
})
export class FacultyAllresourcesPageModule {}
