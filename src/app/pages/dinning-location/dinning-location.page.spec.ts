import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DinningLocationPage } from './dinning-location.page';

describe('DinningLocationPage', () => {
  let component: DinningLocationPage;
  let fixture: ComponentFixture<DinningLocationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DinningLocationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DinningLocationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
