import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacultyNewresourcePage } from './faculty-newresource.page';

const routes: Routes = [
  {
    path: '',
    component: FacultyNewresourcePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacultyNewresourcePageRoutingModule {}
