import { Component,OnInit } from '@angular/core';
import { NavController,LoadingController } from '@ionic/angular';
import { ParentParamsService } from 'src/app/providers/parent-params.service';

@Component({
  selector: 'app-parent-meetingdetails',
  templateUrl: './parent-meetingdetails.page.html',
  styleUrls: ['./parent-meetingdetails.page.scss'],
})
export class ParentMeetingdetailsPage implements OnInit{
  today: any;
  meeting: any;
  comment:any
  meetingcmnts: any;
  cmntsobject: any;
  cmntShow: any;
  constructor(
    public navCtrl: NavController,
    public parentparam : ParentParamsService
    ) {
      this.meeting=this.parentparam.ptm_meetingdetails;
      this.meetingcmnts=[];
      this.cmntShow=[];
      this.cmntsobject=[];
      console.log(this.meeting)
  }

  async ngOnInit() {

    console.log('ionViewDidLoad PtmCommentmodalPage');
    this.today=Date.now();
  }
  // dismiss()
  // {
  //   this.viewCtrl.dismiss("cancel");
  // }

  discussion()
  {
    this.parentparam.ptm_discussionsubject=this.meeting.subject;
    this.parentparam.ptm_discussionid = this.meeting.id;
    this.navCtrl.navigateRoot('parent-ptmchat');
  }
  previouspage()
  {
    this.navCtrl.navigateRoot('parent-ptm');
  }

}
