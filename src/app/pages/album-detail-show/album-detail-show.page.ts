import { Component,OnInit } from '@angular/core';
import { NavController, LoadingController, ToastController, ModalController } from '@ionic/angular';
import { Api } from '../../providers/api/api.service';
import * as moment from 'moment';
import { FacultyParamsService } from '../../providers/faculty-params.service';
import { FacultyNewprogressPage  } from '../faculty-newprogress/faculty-newprogress.page';
@Component({
  selector: 'app-album-detail-show',
  templateUrl: './album-detail-show.page.html',
  styleUrls: ['./album-detail-show.page.scss'],
})
export class AlbumDetailShowPage implements OnInit{

  images: any;
  albumname: any;
  ids: any;
  edit: boolean;
  idtosend: any;

  constructor(
    public navCtrl: NavController,
    public api: Api,
    private loadingController: LoadingController,
    public toastCtrl: ToastController,
    public modalCtrl: ModalController,
    public facultyparams: FacultyParamsService

  ) {
    this.ids = [];
    this.idtosend = [];
    this.images = this.facultyparams.album_array;
    this.albumname = this.images.album_name
    this.edit = false;
  }

  ngOnInit() {
    console.log('ionViewDidLoad AlbumDetailShowPage');
    console.log(this.images);

  }
  previouspage()
  {
    this.navCtrl.navigateRoot('album');
  }
  async openFull(url) {
    if (this.edit == false) {
        this.facultyparams.albummodal_img = this.images;
//        this.navCtrl.navigateRoot('faculty-newprogress')
      let showgrid = await this.modalCtrl.create({
        component: FacultyNewprogressPage,
        componentProps: { player: this.images , index:url}
      });
      await showgrid.present();
    }
  }
  selectPhoto(event, id) {
    if (this.ids.length != 0) {
      var s = 0;
      for (var i = 0; i < this.ids.length; i++) {
        if (id == this.ids[i].id) {
          this.ids.splice(i, 1);
          this.idtosend.splice(i, 1);
          s = 1;
          break;
        }
      }
      if (s == 0) {
        this.ids.push({ id: id });
        this.idtosend.push(id);
      }
    }
    else {
      this.ids.push({ id: id });
      this.idtosend.push(id);
    }
    console.log(event);
  }
  disabled()
  {
    if(this.ids.length==0)
    {
      return true;
    }
    else{
      return false;
    }
  }
  async deletePhoto() {
    const loading = await this.loadingController.create({
    });
    await loading.present();
    let now = moment().format('DD-MMM-YYYY HH:mm:ss');
    this.api.patch("faculty/picture/delete_pictures", { current_time: now, picture_ids: this.idtosend })
      .subscribe(async data => {
        console.log(data);
        let toast = await this.toastCtrl.create({
          message: "Pictures deleted successfully",
          duration: 4000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-success",
          color:"success"
        });
        await toast.present();
        loading.dismiss()
        this.navCtrl.navigateRoot('album');
      }, async err => {
        loading.dismiss()
        let toast = await this.toastCtrl.create({
          message: "Something went wrong",
          duration: 4000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger",
          color:"danger"
        });
        await toast.present();

      })
  }
  editAlbum() {
    this.facultyparams.addmoreimagesalbum=this.images.id;
    this.navCtrl.navigateRoot('album-detail-faculty');
    // this.navCtrl.push("AlbumDetailFacultyPage", { id: this.images.id })
  }
  editTrue() {
    if (this.edit == true) {
      this.edit = false
    }
    else {
      this.edit = true
    }
  }
  ionViewDidLeave()
  {
    this.facultyparams.album_array=[];
  }
}
