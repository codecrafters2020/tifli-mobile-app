import { Component,OnInit } from '@angular/core';
import { NavController} from '@ionic/angular';
import { GlobalVars } from '../../providers/global-vars.service';
import { Api } from '../../providers/api/api.service';
import { ParentParamsService } from 'src/app/providers/parent-params.service';

@Component({
  selector: 'app-parent-leave',
  templateUrl: './parent-leave.page.html',
  styleUrls: ['./parent-leave.page.scss'],
})
export class ParentLeavePage implements OnInit{

  students: any;
  today: number;
  requestedleave: any;
  id: any;

  constructor(
    public navCtrl: NavController,
    public globalVar: GlobalVars,
    public api: Api,
    public facultyparams: ParentParamsService

    ) {
        this.today=Date.now();
  }

  ngOnInit() {
    if(this.facultyparams.notify_leave_parent)
    {
      this.id=this.facultyparams.notify_leave_parent;
      for(var i=0;i<this.globalVar.current_user.students.length;i++)
      {
        if(this.id==this.globalVar.current_user.students[i].id)
        {
          this.students=this.globalVar.current_user.students[i];
        }
      }
    }
    else
    {
      this.students=this.globalVar.student;
      this.id=this.students.id;
    }
  console.log('ionViewDidLoad ParentLeavePage');
    this.api.get("parent/leave_applications/show_all_requested_leaves.json",{student_id:this.id})
    .subscribe(data=>{
      console.log(data);
      this.requestedleave = data["leave_applications"];
    })
  }
  newleaveRequest()
  {
    //  this.navCtrl.navigateForward("ParentNewleavePage",{student:this.students});
     this.navCtrl.navigateRoot('parent-newleave');
  }
  imageShow(img)
  {
    this.facultyparams.doc_img = img;
    this.navCtrl.navigateRoot('document-image');
 //   var server=this.globalVar.server_link+img;
    // this.navCtrl.navigateForward("DocumentImagePage",{img:img});
  }
}
