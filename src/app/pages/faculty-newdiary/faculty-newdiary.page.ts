import { Component, OnInit,NgZone } from '@angular/core';
import { NavController, ToastController, ActionSheetController, LoadingController } from '@ionic/angular';
import { Api } from '../../providers/api/api.service';
import { AwsProvider } from '../../providers/aws.service';
import b64toBlob from 'b64-to-blob';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer/ngx';
import { GlobalVars } from '../../providers/global-vars.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { FacultyParamsService } from '../../providers/faculty-params.service';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { File } from '@ionic-native/file/ngx';
import { FileTransfer,FileUploadOptions,FileTransferObject } from '@ionic-native/file-transfer/ngx';

@Component({
  selector: 'app-faculty-newdiary',
  templateUrl: './faculty-newdiary.page.html',
  styleUrls: ['./faculty-newdiary.page.scss'],
})
export class FacultyNewdiaryPage implements OnInit{
  rate = '0';
  today: any;
  students: any;
  id;
  section: any;
  class: any;
  coursename: any;
  diarytext;
  images: any
  imagelength: boolean;
  avatar: any;
  documentToUpload: any;
  loader: any;
  document: any;
  filename: any;
  classdata: any;
  sectionsfromapi: any;
  course: any;
  courseid;
  sectionid;
  private win: any = window;
  courseidtosend;
  uploadText:any;
  fileTransfer:FileTransferObject
  constructor(
    public navCtrl: NavController,
    public api: Api,
    public toastCtrl: ToastController,
    public camera: Camera,
    private actionSheetController: ActionSheetController,
    public zone: NgZone,
    private awsProvider: AwsProvider,
    private loadingController: LoadingController,
    private viewer : DocumentViewer,
    public globalVar: GlobalVars,
    public facultyParams: FacultyParamsService,
    private transfer:FileTransfer,
    private file:File,
    private FilePath:FilePath,
    private fileChooser:FileChooser  
    ) {
    // this.coursename=this.navParams.get("coursename")
    // this.students=this.navParams.get("students")
    // this.class=this.navParams.get("class")
    // this.section=this.navParams.get("section")
    // this.id=this.navParams.get("courseid")
    this.today = Date.now();
    this.imagelength = false;
    this.classdata = [];
    this.sectionsfromapi = [];
    this.course = [];
    this.uploadText = "";    
  }
  contains = ["Photoshop", "in Desgin", "HTML", "CSS", "Dreamwaver"];

  async ngOnInit() {
    console.log('ionViewDidLoad LessonDetailsPage');
    const loading = await this.loadingController.create({
      // content:"loading data..."
    });
    await loading.present();
    console.log('ionViewDidLoad TeacherNewresourcePage');
    this.api.get("faculty/section_courses/get_faculty_teached_class.json",
      { faculty_id: this.globalVar.current_user.information.id }).subscribe(data => {
        loading.dismiss();
        this.classdata = data["classes"];
        console.log(this.classdata)
        //  this.coursesArray={class:this.coursedata[i].school_class_id,section:this.sections};
      }, err => {
        loading.dismiss();
      });

  }


  async sendCourse(courseid) {
    const loading = await this.loadingController.create({
      // content:"loading data..."
    });
    await loading.present();

    this.api.get("faculty/section_courses/get_faculty_teached_section.json",
      { faculty_id: this.globalVar.current_user.information.id, school_class_id: courseid })
      .subscribe(data => {
        this.sectionsfromapi = data["class_sections"];
        //  for(var i=0;i<this.sectionsfromapi.length;i++)
        //  {
        //    this.alignSections(this.sectionsfromapi[i].section.section_name,this.sectionsfromapi[i].section.id);
        //  }
        loading.dismiss();
      }, err => {
        loading.dismiss();
      });

  }
  async getCourse(sectionid) {
    const loading = await this.loadingController.create({
      // content:"loading data..."
    });
    await loading.present();

    this.api.get("faculty/section_courses/get_section_courses.json", {
      section_id: sectionid,
      faculty_id: this.globalVar.current_user.information.id
    }).
      subscribe(data => {
        this.course = data["section_courses"]
        console.log(data)
        loading.dismiss();
      }, err => {
        loading.dismiss();
      });

  }
  cancel() {
    this.navCtrl.navigateRoot('faculty-home');
  }


  async sendDiary() {
    const loading = await this.loadingController.create({
    });
    await loading.present();
    let that = this;
    if (this.images != null) {
      let ext = 'jpg';
      let type = 'image/jpeg'
      let newName = this.awsProvider.randomString(6) + new Date().getTime() + '.' + ext;

      this.awsProvider.getSignedUploadRequest(newName, ext, type).subscribe(data => {
        console.log("Success in getSignedUploadRequest", data)
        that.avatar = data["public_url"]
        console.log("i am in public url", data["public_url"])

        that.zone.run(() => {
          let request = new XMLHttpRequest();
          request.open('GET', this.images, true);
          request.responseType = 'blob';
          request.timeout = 360000;
          request.onload = function () {
            let reader = new FileReader();
            reader.readAsDataURL(request.response);
            reader.onload = function (e) {
              that.zone.run(() => {
                console.log("error", e);
                var b1 = <string>reader.result
                let blob = b64toBlob(b1 .replace(/^data:image\/\w+;base64,/, ""), 'jpg');
                // let blob = b64toBlob(reader.result.replace(/^data:image\/\w+;base64,/, ""), 'jpg');

                that.awsProvider.uploadFile(data["presigned_url"], blob).subscribe(_result => {

                  console.log("result that i got after uploading to aws", _result)
                  that.api.post("faculty/course_diaries.json", {
                    section_course_id: that.id,
                    diary_info: that.diarytext, image_url: that.avatar, document: null
                  })
                    .subscribe(async data => {
                      let toast = await that.toastCtrl.create({
                        message: "Diary Updated to All",
                        duration: 6000,
                        position: 'top',
                        showCloseButton: true,
                        closeButtonText: "x",
                        cssClass: "toast-success",
                        color:"success"
                      });
                      toast.present();

                      loading.dismiss();
                      that.navCtrl.navigateRoot('faculty-home');
                    }, async err => {
                      loading.dismiss();
                      let toast = await that.toastCtrl.create({
                        message: "Something went wrong",
                        duration: 4000,
                        position: 'top',
                        showCloseButton: true,
                        closeButtonText: "x",
                        cssClass: "toast-danger",
                        color:"danger"

                      });
                      toast.present();


                    });

                });
              })

            }

            reader.onerror = function (e) {
              console.log('We got an error in file reader:::::::: ', e);
              loading.dismiss();
            };

            reader.onabort = function (event) {
              console.log("reader onabort", event)
              loading.dismiss();
            }

          };

          request.onerror = function (e) {
            console.log("** I got an error in XML Http REquest", e);
            loading.dismiss();
          };

          request.ontimeout = function (event) {
            console.log("xmlhttp ontimeout", event)
            loading.dismiss();
          }

          request.onabort = function (event) {
            console.log("xmlhttp onabort", event);
            loading.dismiss();
          }


          request.send();
        });
      }, async (err) => {
        let toast = await this.toastCtrl.create({
          message: "Unable to send request. Check wifi connection",
          duration: 6000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger",
          color:"danger"
        });
        toast.present();
        console.log('getSignedUploadRequest timeout error: ', err);
        loading.dismiss();
      });
    }
    else {
      this.api.post("faculty/course_diaries.json", {
        section_course_id: this.id,
        diary_info: this.diarytext, image_url: null, document: null
      })
        .subscribe(async data => {
          let toast = await that.toastCtrl.create({
            message: "Diary Updated to All",
            duration: 6000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-success",
            color:"success"

          });
          toast.present();
          loading.dismiss();

          this.navCtrl.navigateRoot('faculty-home');

          // this.navCtrl.setRoot("HomePage")
        }, async err => {
          let toast = await this.toastCtrl.create({
            message: "Something went wrong",
            duration: 4000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-danger",
            color:"danger"

          });
          toast.present();

          loading.dismiss();

        })
    }
  }
  diaryToIndividual() {
    this.navCtrl.navigateRoot('faculty-individualdiary');
  }
  takePicture(sourceType) {
    const options: CameraOptions = {
      quality: 100,
      correctOrientation: true,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: sourceType
    }

    this.camera.getPicture(options).then((imageData) => {
      this.images = this.win.Ionic.WebView.convertFileSrc(imageData);
      this.documentToUpload = null;

    });
  }
  disabled() {
    if (this.diarytext == null || this.diarytext == undefined) {
      return true;
    }
    else {
      return false;
    }
  }

  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          //this.selectPhoto();
          //  this.images="../../assets/img/female-logo.jpg"
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.CAMERA);
          //this.takePhoto();
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }
  previousdiary() {
    this.facultyParams.previousdiarystatus="diary"
   this.navCtrl.navigateForward('faculty-previousdiary');
  }
  async uploadDocumentDiary() {
    this.loader = await this.loadingController.create({
    });
    this.loader.present();
    let that = this;
    if (this.documentToUpload != null) {
      var fileName = this.documentToUpload.substr(this.documentToUpload.lastIndexOf('/') + 1);
      var fileExtension = fileName.substr(fileName.lastIndexOf('/') + 1);
      let ext = fileExtension;
      let type = 'application/' + fileExtension;
      let newName = this.awsProvider.randomString(6) + new Date().getTime() + '.' + ext;
      console.log("**new NAme **", newName);

      this.awsProvider.getSignedUploadRequest(newName, ext, type).subscribe(data => {

        console.log("** I got success in getSignedUploadRequest", data);
        this.document = data["public_url"];
        that.zone.run(() => {
          let request = new XMLHttpRequest();
          request.open('GET', this.documentToUpload, true);
          request.responseType = 'blob';
          request.timeout = 360000;
          request.onload = function () {
            that.zone.run(() => {
              let reader = new FileReader();
              reader.readAsDataURL(request.response);

              console.log("Step 1 reader", reader);
              console.log("Step 1 got stuck in request.response", request)

              reader.onload = function () {
                that.zone.run(() => {
                  console.log("Step 2 Loaded", reader.result);
                  var b1 = <string>reader.result
                  let blob = b64toBlob(b1 .replace(/^data:application\/\w+;base64,/, ""), fileExtension);
                  // let blob = b64toBlob(reader.result.replace(/^data:application\/\w+;base64,/, ""), fileExtension);
                  console.log("blob", blob);
                  that.awsProvider.uploadFile(data["presigned_url"], blob).subscribe(_result => {
                    console.log("result that i got after uploading to aws", _result)
                    that.api.post("faculty/course_diaries.json", {
                      section_course_id: that.id,
                      diary_info: that.diarytext, image_url: null, document: that.document
                    })
                      .subscribe(async data => {
                        let toast = await that.toastCtrl.create({
                          message: "Dairy Updated To All Students",
                          duration: 6000,
                          position: 'top',
                          showCloseButton: true,
                          closeButtonText: "x",
                          cssClass: "toast-success",
                          color:"success"
                        });
                        toast.present();
                        that.loader.dismiss();
                        that.navCtrl.navigateRoot('faculty-home');
                      }, async err => {
                        that.loader.dismiss();
                        let toast = await that.toastCtrl.create({
                          message: "Something went wrong",
                          duration: 6000,
                          position: 'top',
                          showCloseButton: true,
                          closeButtonText: "x",
                          cssClass: "toast-danger",
                          color:"danger"
                        });
                        toast.present();
                      })
                  }, (err) => {
                    console.log('err in uploadFile:::::::: ', err);
                  });
                });
              }

              reader.onerror = function (e) {
                console.log('We got an error in file reader:::::::: ', e);
              };

              reader.onabort = function (event) {
                console.log("reader onabort", event)
              }
            })
          };

          request.onerror = function (e) {
            that.loader.dismiss();
            console.log("** I got an error in XML Http REquest", e);
          };

          request.ontimeout = function (event) {
            that.loader.dismiss();
            console.log("xmlhttp ontimeout", event)
          }

          request.onabort = function (event) {
            that.loader.dismiss();
            console.log("xmlhttp onabort", event);
          }


          request.send();
        })
      }, async (err) => {
        that.loader.dismiss();
        console.log('getSignedUploadRequest timeout error: ', err);

        let toast = await this.toastCtrl.create({
          message: "Unable to send request. Check wifi connection",
          duration: 6000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger",
          color:"danger"
        });
        toast.present();

      });


    }
    else {
      let that = this;
      that.api.post("faculty/course_diaries.json", {
        section_course_id: that.id,
        diary_info: that.diarytext, image_url: null, document: null
      })
        .subscribe(async data => {
          let toast = await that.toastCtrl.create({
            message: "Diary Updated To All Students",
            duration: 6000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-success",
            color:"success"
          });
          toast.present();
          that.loader.dismiss();
          that.navCtrl.navigateRoot('faculty-home');
        }, async err => {
          that.loader.dismiss();
          let toast = await that.toastCtrl.create({
            message: "Something went wrong",
            duration: 6000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-danger",
            color:"danger"
          });
          toast.present();
        })

    }

  }
  uploadFile()
  {
    this.fileChooser.open().then(uri => {
      this.FilePath.resolveNativePath(uri).then(nativepath => {
        this.images = null;
        this.documentToUpload = this.win.Ionic.WebView.convertFileSrc(nativepath);
        var fileName = nativepath.substr(this.documentToUpload.lastIndexOf('/') + 1);
        this.filename = fileName;
     }, err => {
        alert(JSON.stringify(err));
      })
    }, err => {
      alert(JSON.stringify(err));
    })
  }
  openPdf() {
    var options: DocumentViewerOptions = {
      title: 'My PDF'
    }
    this.viewer.viewDocument(this.documentToUpload, 'application/pdf', options);
  }

  takeDocuments(sourceType) {

    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
     // encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.ALLMEDIA,
      sourceType: sourceType
    }

    let that = this;
    this.camera.getPicture(options).then((imageData) => {
      this.images = null;
      this.documentToUpload = this.win.Ionic.WebView.convertFileSrc(imageData);
//      this.documentToUpload = imageData;
      var fileName = imageData.substr(this.documentToUpload.lastIndexOf('/') + 1);
      this.filename = fileName;
    }, async (err) => {
      let toast = await this.toastCtrl.create({
        message: "Unable to Access Gallery. Please Try Later",
        duration: 6000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger",
        color:"danger"
      });
      toast.present();
      console.log('err: ', err);
    });
  }

  async presentActionSheetForDocuments() {
    let actionSheet = await this.actionSheetController.create({
      header: 'Select Document Source',
      buttons: [
        {

          text: 'Load from Library',
          handler: () => {
            this.takeDocuments(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        // {
        //   text: 'Use Camera',
        //   handler: () => {
        //     this.takeDocuments(this.camera.PictureSourceType.CAMERA);
        //   }
        // },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  openPage()
  {
      this.navCtrl.navigateRoot('notifications');
  }
}
