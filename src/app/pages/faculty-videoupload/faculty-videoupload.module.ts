import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacultyVideouploadPageRoutingModule } from './faculty-videoupload-routing.module';

import { FacultyVideouploadPage } from './faculty-videoupload.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacultyVideouploadPageRoutingModule
  ],
  declarations: [FacultyVideouploadPage]
})
export class FacultyVideouploadPageModule {}
