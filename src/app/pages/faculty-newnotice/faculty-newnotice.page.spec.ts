import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacultyNewnoticePage } from './faculty-newnotice.page';

describe('FacultyNewnoticePage', () => {
  let component: FacultyNewnoticePage;
  let fixture: ComponentFixture<FacultyNewnoticePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacultyNewnoticePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacultyNewnoticePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
