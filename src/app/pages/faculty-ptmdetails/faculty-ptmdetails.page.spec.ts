import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacultyPtmdetailsPage } from './faculty-ptmdetails.page';

describe('FacultyPtmdetailsPage', () => {
  let component: FacultyPtmdetailsPage;
  let fixture: ComponentFixture<FacultyPtmdetailsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacultyPtmdetailsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacultyPtmdetailsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
