import { Component,OnInit } from '@angular/core';
import { NavController, LoadingController } from '@ionic/angular';
import { Api } from '../../providers/api/api.service';
import { GlobalVars } from '../../providers/global-vars.service';
import { FacultyParamsService } from 'src/app/providers/faculty-params.service';

@Component({
  selector: 'app-faculty-previousdiary',
  templateUrl: './faculty-previousdiary.page.html',
  styleUrls: ['./faculty-previousdiary.page.scss'],
})
export class FacultyPreviousdiaryPage implements OnInit{
  previousdata: any;
  status: any;

  constructor(
    public navCtrl: NavController,
    private loadingController: LoadingController,
    public api: Api,
    public globalVar: GlobalVars,
    public facultyParams: FacultyParamsService

    ) {
      this.previousdata=[];
  }

  async ngOnInit() {
    console.log('ionViewDidLoad PreviousDiaryPage');
    const loading = await this.loadingController.create({
    });
    await loading.present();
    this.status= this.facultyParams.previousdiarystatus;
    if(this.status=="diary")
    {
      this.api.get("faculty/course_diaries/get_previously_issued_diaries.json",
      {faculty_id:this.globalVar.current_user.information.id}).subscribe(data=>{
          console.log(data["course_diaries"]);
          this.previousdata=data["course_diaries"];
          loading.dismiss();
      },err=>{
        loading.dismiss();
      });

    }
    else
    {

      this.api.get("faculty/section_notices/get_previously_issued_notices.json",
      {faculty_id:this.globalVar.current_user.information.id}).subscribe(data=>{
          console.log(data);
          loading.dismiss();
          this.previousdata=data["section_notices"];
      },err=>{
        loading.dismiss();
      });

    }

  }
  imageFullpage(url,diary)
  {
    // this.navCtrl.push("DocumentImagePage",{img:url,diary:diary});
  }
}
