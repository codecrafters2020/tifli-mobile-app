import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacultyNewresourcePageRoutingModule } from './faculty-newresource-routing.module';

import { FacultyNewresourcePage } from './faculty-newresource.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacultyNewresourcePageRoutingModule
  ],
  declarations: [FacultyNewresourcePage]
})
export class FacultyNewresourcePageModule {}
