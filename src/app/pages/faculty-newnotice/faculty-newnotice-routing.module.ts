import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacultyNewnoticePage } from './faculty-newnotice.page';

const routes: Routes = [
  {
    path: '',
    component: FacultyNewnoticePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacultyNewnoticePageRoutingModule {}
