import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacultyAllresultdetailPage } from './faculty-allresultdetail.page';

describe('FacultyAllresultdetailPage', () => {
  let component: FacultyAllresultdetailPage;
  let fixture: ComponentFixture<FacultyAllresultdetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacultyAllresultdetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacultyAllresultdetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
