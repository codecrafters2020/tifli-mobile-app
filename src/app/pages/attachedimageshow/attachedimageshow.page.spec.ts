import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AttachedimageshowPage } from './attachedimageshow.page';

describe('AttachedimageshowPage', () => {
  let component: AttachedimageshowPage;
  let fixture: ComponentFixture<AttachedimageshowPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttachedimageshowPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AttachedimageshowPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
