import { Component,OnInit } from '@angular/core';
import { NavController,ToastController, LoadingController } from '@ionic/angular';
import { Api } from '../../providers/api/api.service';
import { GlobalVars } from '../../providers/global-vars.service';
import * as moment from 'moment';
import { FacultyParamsService } from '../../providers/faculty-params.service';
@Component({
  selector: 'app-faculty-resultuploadmarks',
  templateUrl: './faculty-resultuploadmarks.page.html',
  styleUrls: ['./faculty-resultuploadmarks.page.scss'],
})
export class FacultyResultuploadmarksPage implements OnInit{
  students:any=[];
  studentmarksarray:any;
  id;
  studentarray;
  category;
  coursename;
  totalmarks;
  courseimg='assets/img/course_name.png';
  studentcard='assets/img/student_card.png';
  student_attendance_updated_show: any;
  dateOfResult: any;
  attendancefortoday: any;

  constructor(
    public navCtrl: NavController,
    public api: Api,
    public toastCtrl: ToastController,
    private loadingController: LoadingController,
    public globalVar: GlobalVars,
    public facultyParams: FacultyParamsService
) {
    this.studentmarksarray=[];
    this.studentarray=[];
    this.student_attendance_updated_show=[];
  }

  ngOnInit() {
    let now = moment().format('DD-MMM-YYYY ');
    console.log('ionViewDidLoad TeacherUplaodmarksPage');
    this.students=this.facultyParams.create_result_students_array;
    this.id=this.facultyParams.result_id;
    this.dateOfResult=this.facultyParams.result_date;
    var x = this.dateOfResult.split("T");
    var y = x[0];
    this.category=this.facultyParams.result_category;
    this.totalmarks=this.facultyParams.result_totalmarks;
    this.coursename=this.facultyParams.result_coursename;
    var classsectionid=this.facultyParams.result_classsectionid;
    this.api.get("faculty/student_attendance/get_attendance_by_date.json",{current_date:y,
      class_section_id:classsectionid }).subscribe(data=>{
        this.student_attendance_updated_show=data["student_attendance"];
        this.attendancefortoday=data["attendance_for_today"];
        if(data["attendance_for_today"]!=false)
        {
          for(var i=0;i<this.student_attendance_updated_show.length;i++)
          {
              this.studentarray={student_id:this.student_attendance_updated_show[i].student.id,
                marks_obtained:0,result_id:this.id,
                student_attendance:this.student_attendance_updated_show[i].attendance};
              this.studentmarksarray.push(this.studentarray);
          }

        }
        else{
          for(var i=0;i<this.students.length;i++)
          {
              this.studentarray={student_id:this.students[i].id,marks_obtained:0,result_id:this.id,
                student_attendance:2};
              this.studentmarksarray.push(this.studentarray);
          }

        }
    });
    console.log(this.students);
  }

  previouspage()
  {
    this.navCtrl.navigateRoot('faculty-newresult');
  }
  attendanceKeeper(index,attendance)
  {
      this.studentmarksarray[index].student_attendance=attendance;
  }
  async sendResult()
  {
    const loading = await this.loadingController.create({
      // content:"loading data..."
    });
    await loading.present();

    this.api.post("faculty/result/create_students_result.json",{student_results:this.studentmarksarray})
    .subscribe(data=>{
      console.log(data);
      this.facultyParams.create_result_students_array=[];
      this.facultyParams.result_id=null;
      this.facultyParams.result_date=null;
      this.facultyParams.result_category=null;
      this.facultyParams.result_totalmarks=null;
      this.facultyParams.result_coursename=null;
      this.facultyParams.result_classsectionid=null;
      loading.dismiss();
      this.navCtrl.navigateRoot('faculty-home');
    },async err=>{
      loading.dismiss();
      let toast = await this.toastCtrl.create({
        message: "Something went wrong. Please Try Later",
        duration: 4000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger"
      });
      await toast.present();

    })
  }
}
