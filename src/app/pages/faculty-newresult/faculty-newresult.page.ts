import { Component,OnInit } from '@angular/core';
import { NavController,ToastController, LoadingController } from '@ionic/angular';
import { Api } from '../../providers/api/api.service';
import { GlobalVars } from '../../providers/global-vars.service';
import { FacultyParamsService } from '../../providers/faculty-params.service';
@Component({
  selector: 'app-faculty-newresult',
  templateUrl: './faculty-newresult.page.html',
  styleUrls: ['./faculty-newresult.page.scss'],
})
export class FacultyNewresultPage implements OnInit{
  filtermonthwise;
  category;
  TotalMarks;
  classdata: any;
  courseid;
  sectionsfromapi: any;
  sectionid;
  course: any;
  courseidtosend;
  resultDetails;
  classsectionid: any;
  constructor(
    public navCtrl: NavController,
    public api: Api,
    public toastCtrl: ToastController,
    private loadingController: LoadingController,
    public globalVar: GlobalVars,
    public facultyParams: FacultyParamsService
) {
      this.classdata=[];
      this.sectionsfromapi=[];
      this.course=[];

  }

  async ngOnInit() {
    console.log('ionViewDidLoad TeacherNewresultPage');
    const loading = await this.loadingController.create({
      // content:"loading data..."
  });
  await loading.present();
    this.api.get("faculty/section_courses/get_faculty_teached_class.json",
    {faculty_id:this.globalVar.current_user.information.id}).subscribe(data=>{
      loading.dismiss();
      this.classdata=data["classes"];
      console.log(this.classdata)
              //  this.coursesArray={class:this.coursedata[i].school_class_id,section:this.sections};
    },err=>{
      loading.dismiss();
    });

  }
  previouspage()
  {
    this.navCtrl.navigateRoot('faculty-allresult')
  }
  async sendCourse(courseid)
  {
    const loading = await this.loadingController.create({
      // content:"loading data..."
    });
    await loading.present();

    this.api.get("faculty/section_courses/get_faculty_teached_section.json",
   {faculty_id:this.globalVar.current_user.information.id,school_class_id:courseid})
   .subscribe(data=>{
     this.sectionsfromapi = data["class_sections"];
    //  for(var i=0;i<this.sectionsfromapi.length;i++)
    //  {
    //    this.alignSections(this.sectionsfromapi[i].section.section_name,this.sectionsfromapi[i].section.id);
    //  }
     loading.dismiss();
    },err=>{
    loading.dismiss();
  });

  }
  async getCourse(sectionid)
  {
    const loading = await this.loadingController.create({
      // content:"loading data..."
    });
    await loading.present();

    this.api.get("faculty/section_courses/get_section_courses.json", {
      section_id: sectionid,
      faculty_id: this.globalVar.current_user.information.id
    }).
      subscribe(data => {
        this.course = data["section_courses"];
        this.classsectionid = this.course[0].class_section.id;
        // for(var i=0;i<this.course.length;i++)
        // {
        //   this.Courses(this.course[i].course.course_name,this.course[i].id,this.course[i].students);
        // }
        loading.dismiss();
      }, err => {
        loading.dismiss();
      });

  }
  cancel()
  {
    this.navCtrl.navigateRoot('faculty-home');
  }

  async createResult()
  {
    const loading = await this.loadingController.create({
      // content:"loading data..."
    });
    await loading.present();

    this.api.post("faculty/result.json",{
      result_details:this.resultDetails,
      result_category:this.category,
      total_marks:this.TotalMarks,
      section_course_id:this.courseidtosend,
      faculty_id:this.globalVar.current_user.information.id,
      result_date:this.filtermonthwise
    }).subscribe(data=>{
      console.log(data);
      for(var i=0;i<this.course.length;i++)
      {
        if(this.courseidtosend==this.course[i].id)
        {
          loading.dismiss();
          this.facultyParams.create_result_students_array=this.course[i].students;
          this.facultyParams.result_id = data["result"]["id"];
          this.facultyParams.result_category = data["result"]["result_category"];
          this.facultyParams.result_totalmarks= this.TotalMarks;
          this.facultyParams.result_coursename= this.course[i].course.course_name;
          this.facultyParams.result_date = this.filtermonthwise;
          this.facultyParams.result_classsectionid = this.classsectionid;
          this.navCtrl.navigateRoot('faculty-resultuploadmarks');
          break;
        }
      }
    },async err=>{
      loading.dismiss();
      let toast = await this.toastCtrl.create({
        message: "Something went wrong. Please Try Later",
        duration: 4000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger",
        color:"danger"
      });
      await toast.present();

    })
  }
}
