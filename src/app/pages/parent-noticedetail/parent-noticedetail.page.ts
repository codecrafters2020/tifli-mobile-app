import { Component,OnInit } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import * as moment from 'moment';
import { Api } from '../../providers/api/api.service';
import { GlobalVars } from '../../providers/global-vars.service';
import { DocumentViewer } from '@ionic-native/document-viewer/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import { ParentParamsService } from 'src/app/providers/parent-params.service';

@Component({
  selector: 'app-parent-noticedetail',
  templateUrl: './parent-noticedetail.page.html',
  styleUrls: ['./parent-noticedetail.page.scss'],
})
export class ParentNoticedetailPage implements OnInit {

  student: any;
  name: any;
  section: any;
  class: any;
  today: any;
  diaryapi: any;
  diaryShow: any;
  diaryinfo: any;
  from_date
  id: any;
  day:any;
  yellowdate ='assets/img/notice/yellowdate.png';
  bluedate ='assets/img/notice/bluedate.png';
  pinkdate ='assets/img/notice/pinkdate.png';

  constructor(
    public navCtrl: NavController,
    public api: Api,
    public globalVar: GlobalVars,
    private viewer : DocumentViewer,
    private platform:Platform,
    private file:File,
    private transfer:FileTransfer,
    public parentparam : ParentParamsService

    ) {
      if(this.parentparam.notify_notice_parent)
      {
        this.id=this.parentparam.notify_notice_parent;
        for(var i=0;i<this.globalVar.current_user.students.length;i++)
        {
          if(this.id==this.globalVar.current_user.students[i].id)
          {
            this.student=this.globalVar.current_user.students[i];
            this.name=this.student.first_name;
            this.section = this.student.class_section.section.section_name;
            this.class = this.student.class_section.school_class.class_name;
          }
        }
      }
      else{
        this.student = this.globalVar.student;
        this.name=this.student.first_name;
        this.section = this.student.class_section.section.section_name;
        this.class = this.student.class_section.school_class.class_name;
        this.id=this.student.id;
        }
      this.diaryapi=[];
      this.diaryShow=[];
      this.diaryinfo=[];

  }

  ngOnInit() {
    console.log('ionViewDidLoad ParentNoticeDetailPage');
    this.today = Date.now();
    var date = moment.utc().format('YYYY-MM-DD');
    var stillUtc = moment.utc().format('YYYY-MM-DD');
    var dateend = moment().utc().subtract(7,'d').format('YYYY-MM-DD');
    this.api.get("parent/section_notices/get_student_notices.json",{
      start_date:dateend,end_date:stillUtc,class_section_id:this.student.class_section.id,
      student_id:this.id}).subscribe(data=>{
      console.log(data);
      this.diaryapi=data["section_notices"];
        for(var i=0;i<this.diaryapi.length;i++)
        {
          if(this.diaryapi[i].notice_info!=null)
          {

            this.diaryinfo={diaryinfo:this.diaryapi[i].notice_info,img_url:this.diaryapi[i].image_url,
              subject:this.diaryapi[i].subject,created_at:this.diaryapi[i].created_at,document:this.diaryapi[i].document};
              this.diaryShow.push(this.diaryinfo);
          }
        }


    });

  }
  getNotice()
  {
    this.diaryapi=[];
    this.diaryShow=[];
    this.diaryinfo=[];
  this.api.get("parent/section_notices/get_student_notices.json",{
    start_date:this.from_date,end_date:this.from_date,class_section_id:this.student.class_section.id,
      student_id:this.student.id}).subscribe(data=>{
      console.log(data);
      if(!data["message"])
      {
        this.diaryapi=data["section_notices"];
      for(var i=0;i<this.diaryapi.length;i++)
      {
        if(this.diaryapi[i].notice_info!=null)
        {
         this.day = this.diaryapi[i].created_at;

          this.diaryinfo={diaryinfo:this.diaryapi[i].notice_info,img_url:this.diaryapi[i].image_url,
            subject:this.diaryapi[i].subject,created_at:this.diaryapi[i].created_at,
            document:this.diaryapi[i].document};
          this.diaryShow.push(this.diaryinfo);
        }
          }
       }

      });

  }
  showNotice(i)
  {
    this.parentparam.noticedetail = this.diaryShow[i];
   this.navCtrl.navigateForward('parent-notice');
  }
  imageFullpage(url,diary)
  {
  //  this.navCtrl.push("DocumentImagePage",{img:url,diary:diary});
  }
  downloadAndOpenPdf()
  {
        if(this.platform.is('ios')){
      window.open( this.diaryapi.document , '_system');
    } else {
      let label = encodeURI('My Label');
      window.open( this.diaryapi.document , '_system','location=yes');
    }

    // let path = null;
    //   if(this.platform.is('ios'))
    //   {
    //       path=this.file.documentsDirectory;
    //   }
    //   else
    //   {
    //     path=this.file.dataDirectory;
    //   }
    //   const transfer = this.transfer.create();
    //   transfer.download(this.diaryapi.document,path+'notice.pdf').then(data=>{
    //     console.log(data);
    //     let url = data.nativeURL;
    //       this.viewer.viewDocument(url,'application/pdf',{});
    //     });
  }

}
