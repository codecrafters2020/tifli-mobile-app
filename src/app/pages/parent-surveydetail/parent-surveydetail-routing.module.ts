import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParentSurveydetailPage } from './parent-surveydetail.page';

const routes: Routes = [
  {
    path: '',
    component: ParentSurveydetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParentSurveydetailPageRoutingModule {}
