import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentMapPageRoutingModule } from './parent-map-routing.module';

import { ParentMapPage } from './parent-map.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentMapPageRoutingModule
  ],
  declarations: [ParentMapPage]
})
export class ParentMapPageModule {}
