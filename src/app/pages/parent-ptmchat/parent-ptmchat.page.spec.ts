import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ParentPtmchatPage } from './parent-ptmchat.page';

describe('ParentPtmchatPage', () => {
  let component: ParentPtmchatPage;
  let fixture: ComponentFixture<ParentPtmchatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentPtmchatPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ParentPtmchatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
