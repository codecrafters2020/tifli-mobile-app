import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ParentTimetablePage } from './parent-timetable.page';

describe('ParentTimetablePage', () => {
  let component: ParentTimetablePage;
  let fixture: ComponentFixture<ParentTimetablePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentTimetablePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ParentTimetablePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
