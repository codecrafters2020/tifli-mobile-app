import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ParentResourcePage } from './parent-resource.page';

describe('ParentResourcePage', () => {
  let component: ParentResourcePage;
  let fixture: ComponentFixture<ParentResourcePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentResourcePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ParentResourcePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
