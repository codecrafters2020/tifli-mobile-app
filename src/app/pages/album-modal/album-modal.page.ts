import { Component, OnInit,NgZone } from '@angular/core';
import { NavController, LoadingController, ToastController } from '@ionic/angular';
import { Api } from '../../providers/api/api.service';
import { GlobalVars } from '../../providers/global-vars.service';
import { AwsProvider } from '../../providers/aws.service';
import b64toBlob from "b64-to-blob";
import { FacultyParamsService } from '../../providers/faculty-params.service'

@Component({
  selector: 'app-album-modal',
  templateUrl: './album-modal.page.html',
  styleUrls: ['./album-modal.page.scss'],
})
export class AlbumModalPage implements OnInit{

  image: any;
  privacyisprivate: boolean;
  classesandsection;
  albumname;
  albumdesc
  class;
  classes: any;
  classandsectionshow: any;
  classId: any;
  avatar: any;
  uploadurl: any;
  loading: any;
  constructor(
    public navCtrl: NavController,
    // private viewCtrl: ViewController,
    public api: Api,
    private loadingController: LoadingController,
    public globalVar: GlobalVars,
    public toastCtrl: ToastController,
    public zone: NgZone,
    private awsProvider: AwsProvider,
    public facultyParams: FacultyParamsService
  ) {
    this.uploadurl = [];
    this.image = [];
    this.privacyisprivate = false;
    this.classes = [];
    this.class = [];
    this.classandsectionshow = [];
    this.classId = [];
  }

  async ngOnInit() {
    const loading = await this.loadingController.create({
      // content:"loading data..."
    });
    await loading.present(); console.log('ionViewDidLoad AlbumModalPage');
    this.image = this.facultyParams.album_create_array;
    this.api.get("faculty/class_sections/show_all.json").subscribe(data => {
      console.log(data);
      this.classes = data["class_sections"];
      for (var i = 0; i < this.classes.length; i++) {
        this.class = {
          classname: this.classes[i].school_class.class_name,
          sectionname: this.classes[i].section.section_name, id: this.classes[i].id
        };
        this.classandsectionshow.push(this.class);
      }
      loading.dismiss();
    }, async err => {
      loading.dismiss();
      let toast = await this.toastCtrl.create({
        message: "Something went wrong",
        duration: 4000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger",
        color:"danger"
      });
      await toast.present();
    });
  }
  previouspage()
  {
    this.navCtrl.navigateRoot('album-detail-faculty');
  }
  dismiss() {
    this.navCtrl.navigateRoot('faculty-home');
  }
  delay() {
    return new Promise(resolve => setTimeout(resolve, 300));
  }
  async uploadingImage(im) {
    await this.delay();
    let that = this;

    let ext = 'png';
    let type = 'image/png';
    let newName = this.awsProvider.randomString(6) + new Date().getTime() + '.' + ext;

    this.awsProvider.getSignedUploadRequest(newName, ext, type).subscribe(data => {
      console.log("Success in getSignedUploadRequest", data)
      that.avatar = data["public_url"]
      console.log("i am in public url", data["public_url"])


      that.zone.run(() => {
        let request = new XMLHttpRequest();
        request.open('GET', im, true);
        request.responseType = 'blob';
        request.timeout = 360000;
        request.onload = function () {
          let reader = new FileReader();
          reader.readAsDataURL(request.response);
          reader.onload = function (e) {
            that.zone.run(async () => {
              console.log("error", e);
              var b1 = <string>reader.result
              let blob = b64toBlob(b1 .replace(/^data:image\/\w+;base64,/, ""), 'png');
              //let blob = b64toBlob(reader.result.replace(/^data:image\/\w+;base64,/, ""), 'jpg');
              //await that.delay();
              await that.uploadingImagepart2(data["presigned_url"], blob, data["public_url"]);
            })

          }


          reader.onerror = function (e) {
            console.log('We got an error in file reader:::::::: ', e);
          };

          reader.onabort = function (event) {
            console.log("reader onabort", event)
          }

        };

        request.onerror = function (e) {
          console.log("** I got an error in XML Http REquest", e);
        };

        request.ontimeout = function (event) {
          console.log("xmlhttp ontimeout", event)
        }

        request.onabort = function (event) {
          console.log("xmlhttp onabort", event);
        }


        request.send();
      });
    }, async (err) => {
      let toast = await this.toastCtrl.create({
        message: "Unable to send request. Check wifi connection",
        duration: 6000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger",
        color:"danger"
      });
      await toast.present();
      console.log('getSignedUploadRequest timeout error: ', err);
    });

  }
  async uploadingImagepart2(url, blob, public_url) {
    await this.delay();
    let that = this;
    that.awsProvider.uploadFile(url, blob).subscribe(_result => {
      that.uploadurl.push(public_url);
      console.log("result that i got after uploading to aws", _result)
      // return "success";
      if (that.uploadurl.length == that.image.length) {
        this.loading.dismiss();
        that.uploadimage(that.uploadurl);
      }

    });

  }
  async upload2aws() {
    this.loading = await this.loadingController.create({
      // content:"loading data..."
    });
    await this.loading.present();

    for (const im of this.image) {
      await this.uploadingImage(im);
    }
  }
  async uploadimage(imageurl) {
    const loading = await this.loadingController.create({
      // content:"loading data..."
    });
    await loading.present();
    if (this.classesandsection != null) {
      for (var z = 0; z < this.classesandsection.length; z++) {
        this.classId.push(this.classesandsection[z].id)
        //  console.log(this.classesandsection[z].id)
      }
    }
    if (this.classId.length != 0) {
      this.api.post("faculty/albums.json", {
        album_name: this.albumname, description: this.albumdesc,
        is_public: !this.privacyisprivate, class_sections: this.classId
      }).subscribe(data => {
        console.log(data["album"]["id"]);

        this.api.post("faculty/picture.json", {
          album_id: data["album"]["id"], images: imageurl,
          faculty_id: this.globalVar.current_user.information.id
        }
        ).subscribe(async data => {

          let toast = await this.toastCtrl.create({
            message: "Album Added successfully",
            duration: 6000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-success",
            color:"success"
          });
          //          this.viewCtrl.dismiss();
          await toast.present();
          console.log(data);
          this.facultyParams.album_create_array=[];
          loading.dismiss();
          this.navCtrl.navigateRoot('faculty-home');
        }, err => {
          loading.dismiss();
        });

        loading.dismiss();
      }, async err => {
        loading.dismiss();
        let toast = await this.toastCtrl.create({
          message: "Something went wrong",
          duration: 4000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger",
          color:"danger"
        });
        await toast.present();
      })
      //      this.viewCtrl.dismiss();

    }
    else {
      this.api.post("faculty/albums.json", {
        album_name: this.albumname, description: this.albumdesc,
        is_public: !this.privacyisprivate, class_sections: this.classId
      }).subscribe(data => {
        console.log(data["album"]["id"]);

        this.api.post("faculty/picture.json", {
          album_id: data["album"]["id"], images: imageurl,
          faculty_id: this.globalVar.current_user.information.id
        }
        ).subscribe(async data => {
          let toast = await this.toastCtrl.create({
            message: "Album Added successfully",
            duration: 6000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-success",
            color:"success"
          });
          await toast.present();
          //            this.viewCtrl.dismiss();
          console.log(data);
          this.facultyParams.album_create_array=[];
          this.navCtrl.navigateRoot('faculty-home');
          loading.dismiss();
        }, async err => {
          loading.dismiss();
          let toast = await this.toastCtrl.create({
            message: "Something went wrong",
            duration: 4000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-danger",
            color:"danger"

          });
          await toast.present();
        });

        loading.dismiss();
      }, async err => {
        loading.dismiss();
        let toast = await this.toastCtrl.create({
          message: "Something went wrong",
          duration: 4000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger",
          color:"danger"
        });
        await toast.present();
      })
      //  this.viewCtrl.dismiss();

    }
  }
  disabled() {
    if (this.albumdesc == null || this.albumdesc == undefined) {
      return true;
    }
    else {
      return false;
    }
  }
  setclassbyId(event) {
    console.log(event[0]);
    for (var z = 0; z < event.length; z++) {
      if (event[z] != null) {
        if (event[z].id == this.classandsectionshow[z].id) {
          console.log(this.classandsectionshow[z].id);
        }
      }
    }
  }
  privacy(i) {
    if (i == 1) {
      console.log("private");
      this.privacyisprivate = true;

    }
    else {
      console.log("public");
      this.privacyisprivate = false;
    }
  }
}
