import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacultyPtmPageRoutingModule } from './faculty-ptm-routing.module';

import { FacultyPtmPage } from './faculty-ptm.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacultyPtmPageRoutingModule
  ],
  declarations: [FacultyPtmPage]
})
export class FacultyPtmPageModule {}
