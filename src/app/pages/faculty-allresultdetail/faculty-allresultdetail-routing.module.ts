import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacultyAllresultdetailPage } from './faculty-allresultdetail.page';

const routes: Routes = [
  {
    path: '',
    component: FacultyAllresultdetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacultyAllresultdetailPageRoutingModule {}
