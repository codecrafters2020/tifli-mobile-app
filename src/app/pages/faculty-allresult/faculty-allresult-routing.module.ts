import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacultyAllresultPage } from './faculty-allresult.page';

const routes: Routes = [
  {
    path: '',
    component: FacultyAllresultPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacultyAllresultPageRoutingModule {}
