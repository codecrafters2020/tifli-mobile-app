import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentPaymentPageRoutingModule } from './parent-payment-routing.module';

import { ParentPaymentPage } from './parent-payment.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentPaymentPageRoutingModule
  ],
  declarations: [ParentPaymentPage]
})
export class ParentPaymentPageModule {}
