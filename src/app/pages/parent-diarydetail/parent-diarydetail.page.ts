import { Component,OnInit } from '@angular/core';
import { NavController,Platform } from '@ionic/angular';
import { Api } from '../../providers/api/api.service';
import * as moment from 'moment';
import { GlobalVars } from '../../providers/global-vars.service';
import { DocumentViewer } from '@ionic-native/document-viewer/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import { ParentParamsService } from 'src/app/providers/parent-params.service';

@Component({
  selector: 'app-parent-diarydetail',
  templateUrl: './parent-diarydetail.page.html',
  styleUrls: ['./parent-diarydetail.page.scss'],
})
export class ParentDiarydetailPage implements OnInit{
  name: any;
  today:any;
  student: any;
  section: any;
  class: any;
  diaryinfo: any;
  diaryapi: any;
  diaryShow: any;
  from_date:any;
  dummyimage="assets/img/cources/002.png";
  constructor(
    public navCtrl: NavController,
    public api: Api,
    public globalVar: GlobalVars,
    private viewer : DocumentViewer,
    private platform:Platform,
    private file:File,
    private transfer:FileTransfer,
    public parentparam : ParentParamsService

    ) {
    this.diaryapi=[];
    this.diaryShow=[];
    this.diaryinfo=[];
  }

  ngOnInit() {
    console.log('ionViewDidLoad ParentsDiaryDetailPage');
    this.today = Date.now();
    var date = moment.utc().format('YYYY-MM-DD HH:mm:ss');
    var stillUtc = moment.utc(date).toDate();
    console.log(stillUtc);
    this.diaryapi=this.parentparam.diarydetail;
    console.log(this.diaryapi);
    // this.api.get("parent/course_diaries/get_student_diaries.json",{diary_date:stillUtc,class_section_id:this.student.class_section.id,
    //   student_id:this.student.id}).subscribe(data=>{
    //   console.log(data);
    //   this.diaryapi=data["course_diaries"];
    //   for(var i=0;i<this.diaryapi.length;i++)
    //   {
    //     this.diaryinfo={diaryinfo:this.diaryapi[i].diary_info,
    //       subject:this.diaryapi[i].section_course.course.course_name,url:this.diaryapi[i].image_url};
    //       this.diaryShow.push(this.diaryinfo);
    //   }
    // });
  }
  getDiary()
  {
    this.diaryapi=[];
    this.diaryShow=[];
    this.diaryinfo=[];
    this.api.get("parent/course_diaries/get_student_diaries.json",{diary_date:this.from_date
      ,class_section_id:this.student.class_section.id,
      student_id:this.student.id}).subscribe(data=>{
      console.log(data);
      this.diaryapi=data["course_diaries"];
      for(var i=0;i<this.diaryapi.length;i++)
      {
        this.diaryinfo={url:this.diaryapi[i].image_url,diaryinfo:this.diaryapi[i].diary_info,
          subject:this.diaryapi[i].section_course.course.course_name};
          this.diaryShow.push(this.diaryinfo);
      }
    });
  }
  previouspage()
  {
    this.navCtrl.navigateRoot('parent-diary');
  }

  openPdf()
  {
    this.viewer.viewDocument(this.diaryapi.image,'application/pdf',{});
  }

  imageFullpage(url,diary)
  {
    this.parentparam.doc_img = url;
    this.parentparam.diary_detaildoc = 'diary'
    this.navCtrl.navigateRoot('attachedimageshow');
  }
  downloadAndOpenPdf()
  {
        if(this.platform.is('ios')){
      window.open( this.diaryapi.document , '_system');
    } else {
      let label = encodeURI('My Label');
      window.open( this.diaryapi.document , '_system','location=yes');
    }

  //   let path = null;
  //     if(this.platform.is('ios'))
  //     {
  //         path=this.file.documentsDirectory;
  //     }
  //     else
  //     {
  //       path=this.file.dataDirectory;
  //     }
  //     const transfer = this.transfer.create();
  //     transfer.download(this.diaryapi.document,path+'diary.pdf').then(data=>{
  //         let url = data.nativeURL;
  //         this.viewer.viewDocument(url,'application/pdf',{});
  //       });
   }
}
