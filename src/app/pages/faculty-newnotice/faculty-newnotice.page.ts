import { Component, OnInit, ChangeDetectorRef, NgZone } from '@angular/core';
import { ActionSheetController, Platform, NavController,ToastController, LoadingController } from '@ionic/angular';
import { Api } from '../../providers/api/api.service';
import { GlobalVars } from '../../providers/global-vars.service';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/camera/ngx';
import { AwsProvider } from '../../providers/aws.service';
import b64toBlob from "b64-to-blob";
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer/ngx';
import { FacultyParamsService } from '../../providers/faculty-params.service';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';

@Component({
  selector: 'app-faculty-newnotice',
  templateUrl: './faculty-newnotice.page.html',
  styleUrls: ['./faculty-newnotice.page.scss'],
})
export class FacultyNewnoticePage implements OnInit{

  coursesArray;
  coursesArrayShow;
  today: any;
  diaryText;
  sectionid: any;
  class: any;
  section: any;
  students: any;
  images: any;
  imagelength: boolean;
  subject: any;
  avatar: any;
  documentToUpload: any;
  loader: any;
  document: any;
  filename: any;
  private win: any = window;

  constructor(
    public navCtrl: NavController,
    public api: Api,
    public toastCtrl: ToastController,
    public globalVar: GlobalVars,
    private loadingController: LoadingController,
    public camera: Camera,
    private actionSheetController: ActionSheetController,
    public zone: NgZone,
    private awsProvider: AwsProvider,
    private viewer: DocumentViewer,
    public facultyParams: FacultyParamsService,
    private FilePath:FilePath,
    private fileChooser:FileChooser  

  ) {
    this.coursesArray = [];
    this.coursesArrayShow = [];

  }

  async ngOnInit() {
    const loading = await this.loadingController.create({
    });
    await loading.present();
    console.log('ionViewDidLoad NoticePage');
    this.today = Date.now();
    this.api.get("faculty/section_notices/get_class_section_teacher.json",
      { faculty_id: this.globalVar.current_user.information.id })
      .subscribe((data: any) => {
        loading.dismiss();
        console.log(data.status);
        this.sectionid = data["class_section"].id;
        this.class = data["class_section"].school_class.class_name;
        this.section = data["class_section"].section.section_name;
        this.students = data["class_section"].students;


      }, async err => {
        loading.dismiss();
        console.log(err.status);
        if (err.status == 400) {
          let toast = await this.toastCtrl.create({
            message: "You are not currently assigned class teacher of any section",
            duration: 6000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-success",
            color:"success"
          });
          toast.present();
          this.navCtrl.navigateRoot('faculty-home');

        }
      })
  }
  disabled() {
    if (this.diaryText == null || this.diaryText == undefined) {
      return true;
    }
    else {
      return false;
    }
  }
  diaryToIndividual() {
    this.facultyParams.notice_individual = 1;
    this.facultyParams.notice_individualstudents = this.students;
    this.facultyParams.notice_individualclass = this.class;
    this.facultyParams.notice_individualcourseid = this.sectionid;
    this.facultyParams.notice_individualsection = this.section;
    this.navCtrl.navigateRoot('faculty-individualdiary');

  }
  async sendNotice() {
    const loading = await this.loadingController.create({
    });
    await loading.present();
    let that = this;
    if (this.images != null) {
      let ext = 'jpg';
      let type = 'image/jpeg'
      let newName = this.awsProvider.randomString(6) + new Date().getTime() + '.' + ext;

      this.awsProvider.getSignedUploadRequest(newName, ext, type).subscribe(data => {
        console.log("Success in getSignedUploadRequest", data)
        that.avatar = data["public_url"]
        console.log("i am in public url", data["public_url"])

        that.zone.run(() => {
          let request = new XMLHttpRequest();
          request.open('GET', this.images, true);
          request.responseType = 'blob';
          request.timeout = 360000;
          request.onload = function () {
            let reader = new FileReader();
            reader.readAsDataURL(request.response);
            reader.onload = function (e) {
              that.zone.run(() => {
                console.log("error", e);
                var b1 = <string>reader.result
                let blob = b64toBlob(b1 .replace(/^data:image\/\w+;base64,/, ""), 'jpg');
                // let blob = b64toBlob(reader.result.replace(/^data:image\/\w+;base64,/, ""), 'jpg');

                that.awsProvider.uploadFile(data["presigned_url"], blob).subscribe(_result => {

                  console.log("result that i got after uploading to aws", _result)
                  that.api.post("faculty/section_notices.json", {
                    class_section_id: that.sectionid, notice_info: that.diaryText,
                    subject: that.subject, image_url: that.avatar, document: null
                  }).subscribe(async data => {
                    console.log(data);
                    let toast = await that.toastCtrl.create({
                      message: "Notice send successfully",
                      duration: 4000,
                      position: 'top',
                      showCloseButton: true,
                      closeButtonText: "x",
                      cssClass: "toast-success",
                      color:"success"

                    });
                    toast.present();

                    loading.dismiss();
                    that.navCtrl.navigateRoot('faculty-home');
                  }, async err => {
                    loading.dismiss();
                    let toast = await that.toastCtrl.create({
                      message: "Unable to send request. Check wifi connection",
                      duration: 4000,
                      position: 'top',
                      showCloseButton: true,
                      closeButtonText: "x",
                      cssClass: "toast-danger",
                      color:"danger"

                    });
                    toast.present();
                  });

                });
              })

            }

            reader.onerror = function (e) {
              console.log('We got an error in file reader:::::::: ', e);
            };

            reader.onabort = function (event) {
              console.log("reader onabort", event)
            }

          };

          request.onerror = function (e) {
            console.log("** I got an error in XML Http REquest", e);
          };

          request.ontimeout = function (event) {
            console.log("xmlhttp ontimeout", event)
          }

          request.onabort = function (event) {
            console.log("xmlhttp onabort", event);
          }


          request.send();
        });
      }, async (err) => {
        let toast = await this.toastCtrl.create({
          message: "Unable to send request. Check wifi connection",
          duration: 6000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger",
          color:"danger"

        });
        toast.present();
        console.log('getSignedUploadRequest timeout error: ', err);
      });
    }
    else {
      this.api.post("faculty/section_notices.json", {
        class_section_id: this.sectionid, notice_info: this.diaryText,
        subject: this.subject, image_url: null, document: null
      }).subscribe(async data => {
        console.log(data);
        let toast = await this.toastCtrl.create({
          message: "Notice send successfully",
          duration: 4000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-success",
          color:"success"

        });
        toast.present();

        loading.dismiss();
        this.navCtrl.navigateRoot('faculty-home');
      }, async err => {
        loading.dismiss();
        let toast = await this.toastCtrl.create({
          message: "Unable to send request. Check wifi connection",
          duration: 4000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger",
          color:"danger"

        });
        toast.present();
      });

    }
  }
  takePicture(sourceType) {
    const options: CameraOptions = {
      quality: 100,
      correctOrientation: true,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.PNG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: sourceType
    }

    this.camera.getPicture(options).then((imageData) => {
      this.images = this.win.Ionic.WebView.convertFileSrc(imageData);
      this.imagelength = true;
      this.documentToUpload = null;
    });
  }

  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          //this.selectPhoto();
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.CAMERA);
          //this.takePhoto();
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }
  previousdiary() {
    this.facultyParams.previousdiarystatus = "notice";
    this.navCtrl.navigateForward('facultyy-previousdiary');
  }
  openPdf() {
    var options: DocumentViewerOptions = {
      title: 'My PDF'
    }
    this.viewer.viewDocument(this.documentToUpload, 'application/pdf', options);
  }

  async uploadDocumentNotice() {
    this.loader = await this.loadingController.create({
    });
    await this.loader.present();
    let that = this;
    if (this.documentToUpload != null) {
      var fileName = this.documentToUpload.substr(this.documentToUpload.lastIndexOf('/') + 1);
      var fileExtension = fileName.substr(fileName.lastIndexOf('/') + 1);
      let ext = fileExtension;
      let type = 'application/' + fileExtension;
      let newName = this.awsProvider.randomString(6) + new Date().getTime() + '.' + ext;
      console.log("**new NAme **", newName);

      this.awsProvider.getSignedUploadRequest(newName, ext, type).subscribe(data => {

        console.log("** I got success in getSignedUploadRequest", data);
        this.document = data["public_url"];
        that.zone.run(() => {
          let request = new XMLHttpRequest();
          request.open('GET', this.documentToUpload, true);
          request.responseType = 'blob';
          request.timeout = 360000;
          request.onload = function () {
            that.zone.run(() => {
              let reader = new FileReader();
              reader.readAsDataURL(request.response);

              console.log("Step 1 reader", reader);
              console.log("Step 1 got stuck in request.response", request)

              reader.onload = function () {
                that.zone.run(() => {
                  console.log("Step 2 Loaded", reader.result);
                  var b1 = <string>reader.result
                  let blob = b64toBlob(b1 .replace(/^data:image\/\w+;base64,/, ""), 'jpg');
                  // let blob = b64toBlob(reader.result.replace(/^data:application\/\w+;base64,/, ""), fileExtension);
                  console.log("blob", blob);
                  that.awsProvider.uploadFile(data["presigned_url"], blob).subscribe(_result => {
                    console.log("result that i got after uploading to aws", _result)
                    that.api.post("faculty/section_notices.json", {
                      class_section_id: that.sectionid, notice_info: that.diaryText,
                      subject: that.subject, image_url: null, document: that.document
                    }).subscribe(async data => {
                      let toast = await that.toastCtrl.create({
                        message: "Notice Send To Individual",
                        duration: 6000,
                        position: 'top',
                        showCloseButton: true,
                        closeButtonText: "x",
                        cssClass: "toast-success",
                        color:"success"

                      });
                      toast.present();
                      that.loader.dismiss();
                      that.navCtrl.navigateRoot('faculty-home');
                    }, async err => {
                      that.loader.dismiss();
                      let toast = await that.toastCtrl.create({
                        message: "Something went wrong",
                        duration: 6000,
                        position: 'top',
                        showCloseButton: true,
                        closeButtonText: "x",
                        cssClass: "toast-danger",
                        color:"danger"

                      });
                      toast.present();
                    })
                  }, (err) => {
                    console.log('err in uploadFile:::::::: ', err);
                  });
                });
              }

              reader.onerror = function (e) {
                console.log('We got an error in file reader:::::::: ', e);
              };

              reader.onabort = function (event) {
                console.log("reader onabort", event)
              }
            })
          };

          request.onerror = function (e) {
            that.loader.dismiss();
            console.log("** I got an error in XML Http REquest", e);
          };

          request.ontimeout = function (event) {
            that.loader.dismiss();
            console.log("xmlhttp ontimeout", event)
          }

          request.onabort = function (event) {
            that.loader.dismiss();
            console.log("xmlhttp onabort", event);
          }


          request.send();
        })
      },async (err) => {
        that.loader.dismiss();
        console.log('getSignedUploadRequest timeout error: ', err);

        let toast = await this.toastCtrl.create({
          message: "Unable to send request. Check wifi connection",
          duration: 6000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger",
          color:"danger"

        });
        toast.present();

      });


    }
    else {
      let that = this;
      this.api.post("faculty/section_notices.json", {
        class_section_id: this.sectionid, notice_info: this.diaryText,
        subject: this.subject, image_url: this.document
      }).subscribe(async data => {
        let toast = await that.toastCtrl.create({
          message: "Notice Send To Individual",
          duration: 6000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-success",
          color:"success"

        });
        toast.present();
        that.loader.dismiss();
        this.navCtrl.navigateRoot('faculty-home');
      }, async err => {
        that.loader.dismiss();
        let toast = await that.toastCtrl.create({
          message: "Something went wrong",
          duration: 6000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger",
          color:"danger"

        });
        toast.present();
      })

    }
  }
  takeDocuments()
  {
    this.fileChooser.open().then(uri => {
      this.FilePath.resolveNativePath(uri).then(nativepath => {
        this.images = null;
        this.documentToUpload = this.win.Ionic.WebView.convertFileSrc(nativepath);
        var fileName = nativepath.substr(this.documentToUpload.lastIndexOf('/') + 1);
        this.filename = fileName;
     }, err => {
        alert(JSON.stringify(err));
      })
    }, err => {
      alert(JSON.stringify(err));
    })
  }
//   takeDocuments(sourceType) {

//     const options: CameraOptions = {
//       quality: 100,
//       correctOrientation: true,
//       destinationType: this.camera.DestinationType.FILE_URI,
//       //    encodingType: this.camera.EncodingType.JPEG,
//       mediaType: this.camera.MediaType.ALLMEDIA,
//       sourceType: sourceType
//     }

//     let that = this;
//     this.camera.getPicture(options).then((imageData) => {
//       this.documentToUpload = this.win.Ionic.WebView.convertFileSrc(imageData);
// //      this.documentToUpload = imageData;
//       var fileName = imageData.substr(this.documentToUpload.lastIndexOf('/') + 1);
//       this.filename = fileName;
//       this.images = null;
//     }, async (err) => {
//       let toast = await this.toastCtrl.create({
//         message: "Unable to Access Gallery. Please Try Later",
//         duration: 6000,
//         position: 'top',
//         showCloseButton: true,
//         closeButtonText: "x",
//         cssClass: "toast-danger",
//         color:"danger"

//       });
//       toast.present();
//       console.log('err: ', err);
//     });
//   }

//   async presentActionSheetForDocuments() {
//     let actionSheet = await this.actionSheetController.create({
//       header: 'Select Document Source',
//       buttons: [
//         {
//           text: 'Load from Library',
//           handler: () => {
//             this.takeDocuments(this.camera.PictureSourceType.PHOTOLIBRARY);
//           }
//         },
//         // {
//         //   text: 'Use Camera',
//         //   handler: () => {
//         //     this.takeDocuments(this.camera.PictureSourceType.CAMERA);
//         //   }
//         // },
//         {
//           text: 'Cancel',
//           role: 'cancel'
//         }
//       ]
//     });
//     actionSheet.present();
//   }
  cancel() {
    this.navCtrl.navigateRoot('faculty-home');
  }
  openPage()
  {
      this.navCtrl.navigateRoot('notifications');
  }
}
