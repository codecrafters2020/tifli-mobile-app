import { Component,OnInit } from '@angular/core';
import { NavController, ToastController, LoadingController,Platform } from '@ionic/angular';
import { Api } from '../../providers/api/api.service';
import { GlobalVars } from '../../providers/global-vars.service';
import { DocumentViewer } from '@ionic-native/document-viewer/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';

@Component({
  selector: 'app-faculty-allresources',
  templateUrl: './faculty-allresources.page.html',
  styleUrls: ['./faculty-allresources.page.scss'],
})
export class FacultyAllresourcesPage implements OnInit{

  resourcedata:any;
  bg1image="assets/img/blue1.png";
  bg2image="assets/img/pink1.png";
  bg3image="assets/img/yellow1.png";

  constructor(
    public navCtrl: NavController,
    public api: Api,
    public toastCtrl: ToastController,
    private loadingController: LoadingController,
    public globalVar: GlobalVars,
    private viewer : DocumentViewer,
    private platform:Platform,
    private file:File,
    private transfer:FileTransfer

) {
    this.resourcedata=[];
  }

  async ngOnInit() {
    const loading = await this.loadingController.create({
      // content:"loading data..."
  });
  await loading.present();
    console.log('ionViewDidLoad TeacherAllresourcePage');
    this.api.get("faculty/resources/get_resource_by_faculty.json",
    {faculty_id:this.globalVar.current_user.information.id}).subscribe(data=>{
      console.log(data);
      this.resourcedata=data["resources"]
      loading.dismiss();
    },err=>{
      loading.dismiss();
    })
  }
  newResource()
  {
    this.navCtrl.navigateForward('faculty-newresource');
  }
  downloadAndOpenPdf(url)
  {
        if(this.platform.is('ios')){
      window.open( url , '_system');
    } else {
      let label = encodeURI('My Label');
      window.open( url , '_system','location=yes');
    }

    // let path = null;
    //   if(this.platform.is('ios'))
    //   {
    //       path=this.file.documentsDirectory;
    //   }
    //   else
    //   {
    //     path=this.file.dataDirectory;
    //   }
    //   const transfer = this.transfer.create();
    //   transfer.download(url,path+'resource.pdf').then(data=>{
    //       let url = data.nativeURL;
    //       this.viewer.viewDocument(url,'application/pdf',{});
    //     });
  }

}
