import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacultyAttendancePage } from './faculty-attendance.page';

const routes: Routes = [
  {
    path: '',
    component: FacultyAttendancePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacultyAttendancePageRoutingModule {}
