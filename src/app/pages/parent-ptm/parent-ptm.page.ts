import { Component, OnInit } from '@angular/core';
import { NavController, ModalController, LoadingController } from '@ionic/angular';
import { Api } from '../../providers/api/api.service';
import { GlobalVars } from '../../providers/global-vars.service';
import * as moment from 'moment';
import { ParentParamsService } from '../../providers/parent-params.service';
@Component({
  selector: 'app-parent-ptm',
  templateUrl: './parent-ptm.page.html',
  styleUrls: ['./parent-ptm.page.scss'],
})
export class ParentPtmPage implements OnInit {

  student: any;
  today: any;
  showgrid: any;
  fname
  apigetptm: any;
  objectget;
  ptmShow;
  id: any;
  cid: any;
  doodle = 'assets/img/bgcover.png';
  subjectCover = 'assets/img/meeting/statusapp.png';
  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public api: Api,
    public globalVar: GlobalVars,
    private loadingController: LoadingController,
    public parentparam: ParentParamsService
  ) {
    this.apigetptm = [];
    this.objectget = [];
    this.ptmShow = [];
  }

  async ngOnInit() {
    if(this.parentparam.notify_ptm_parent_student_id)
    {
      this.id=this.parentparam.notify_ptm_parent_student_id;
      this.cid=this.parentparam.notify_ptm_parent_meeting_id;
      for(var i=0;i<this.globalVar.current_user.students.length;i++)
      {
        if(this.id==this.globalVar.current_user.students[i].id)
        {
          this.student=this.globalVar.current_user.students[i];
        }
      }
    }
    else
    {
    this.student = this.globalVar.student;
    this.id = this.student.id;
    }

    console.log('ionViewDidLoad PtmCommunicateteacherPage');
    const loading = await this.loadingController.create({
      // content:"loading data..."
    });
    await loading.present();

    // this.parentname=this.globalVar.current_user.
    // this.student= this.navParams.get("student");
    console.log(this.student)
    this.fname = this.student.first_name + " " + this.student.last_name;
    this.today = Date.now();
    this.api.get("parent/parent_teacher_meetings/show_all_requested_meetings.json",
      { student_id: this.id }).subscribe(data => {
        console.log(data["parent_teacher_meetings"]);
        loading.dismiss();
        if(!data["message"])
        {
        this.apigetptm = data["parent_teacher_meetings"];
        var appointmentdate, appointmenttime
        for (var i = 0; i < this.apigetptm.length; i++) {
          if (this.id != null) {
            if (this.cid == this.apigetptm[i].id) {
              if (this.apigetptm[i].appointment_date == null) {
                appointmentdate = "NA";
                appointmenttime = "NA"
              }
              else {
                appointmentdate = this.apigetptm[i].appointment_date;
                //appointmenttime=this.apigetptm[i].appointment_time;
                var t = this.apigetptm[i].appointment_time.split("T")
                var x = t[1].split(".")
                appointmenttime = x[0];
              }
              this.objectget = {
                subject: this.apigetptm[i].subject, createdat: this.apigetptm[i].created_at,
                description: this.apigetptm[i].description, facultyname: this.apigetptm[i].faculty_name,
                id: this.apigetptm[i].id, appointmentdate: appointmentdate, appointmenttime: appointmenttime
              };
              this.ptmShow.push(this.objectget);
              this.parentparam.ptm_meetingdetails = this.ptmShow[i];
              this.navCtrl.navigateRoot('parent-meetingdetails');
            }
            else {
              if (this.apigetptm[i].appointment_date == null) {
                appointmentdate = "NA";
                appointmenttime = "NA"
              }
              else {
                appointmentdate = this.apigetptm[i].appointment_date;
                //appointmenttime=this.apigetptm[i].appointment_time;
                var t = this.apigetptm[i].appointment_time.split("T")
                var x = t[1].split(".")
                appointmenttime = x[0];
              }
              this.objectget = {
                subject: this.apigetptm[i].subject, createdat: this.apigetptm[i].created_at,
                description: this.apigetptm[i].description, facultyname: this.apigetptm[i].faculty_name,
                id: this.apigetptm[i].id, appointmentdate: appointmentdate, appointmenttime: appointmenttime
              };
              this.ptmShow.push(this.objectget);

            }

          }
          else
          {
            if (this.apigetptm[i].appointment_date == null) {
              appointmentdate = "NA";
              appointmenttime = "NA"
            }
            else {
              appointmentdate = this.apigetptm[i].appointment_date;
              //appointmenttime=this.apigetptm[i].appointment_time;
              var t = this.apigetptm[i].appointment_time.split("T")
              var x = t[1].split(".")
              appointmenttime = x[0];
            }
            this.objectget = {
              subject: this.apigetptm[i].subject, createdat: this.apigetptm[i].created_at,
              description: this.apigetptm[i].description, facultyname: this.apigetptm[i].faculty_name,
              id: this.apigetptm[i].id, appointmentdate: appointmentdate, appointmenttime: appointmenttime
            };
            this.ptmShow.push(this.objectget);
          }


        }
      }
      }, err => {
        loading.dismiss();
      })
  }
  get sortData() {
    return this.ptmShow.sort((a, b) => {
      return <any>new Date(b.createdat) - <any>new Date(a.createdat);
    });
  }
  showModal(i) {
    this.parentparam.ptm_meetingdetails = this.ptmShow[i];
    this.navCtrl.navigateRoot('parent-meetingdetails');
  }
  newMeeting() {
    this.navCtrl.navigateRoot('parent-newptm');
  }
}
