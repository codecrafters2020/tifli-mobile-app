import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacultyTimetablePage } from './faculty-timetable.page';

describe('FacultyTimetablePage', () => {
  let component: FacultyTimetablePage;
  let fixture: ComponentFixture<FacultyTimetablePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacultyTimetablePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacultyTimetablePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
