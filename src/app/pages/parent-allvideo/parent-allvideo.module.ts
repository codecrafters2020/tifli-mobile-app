import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentAllvideoPageRoutingModule } from './parent-allvideo-routing.module';

import { ParentAllvideoPage } from './parent-allvideo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentAllvideoPageRoutingModule
  ],
  declarations: [ParentAllvideoPage]
})
export class ParentAllvideoPageModule {}
