
import { Injectable } from "@angular/core";

// import "rxjs/add/operator/map";
/*
  Generated class for the GlobalVars provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class GlobalVars {
  public current_user;
  public server_link;
  public device_id;
  public os;
  public auth_token;
  public lang;
  public student;
  public currentIndexParent;
  constructor() {
    this.current_user = "";
    this.device_id = "";
    this.os = "";
    this.lang="";
    this.student=[];
   // this.server_link = "http://18.156.17.79:3000";
    //this.server_link = "http://54.254.47.43:3000";
//this.server_link = "http://192.168.10.14:3000";
this.server_link = "https://tifli.io";

  }
}
