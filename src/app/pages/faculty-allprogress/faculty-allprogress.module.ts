import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacultyAllprogressPageRoutingModule } from './faculty-allprogress-routing.module';

import { FacultyAllprogressPage } from './faculty-allprogress.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacultyAllprogressPageRoutingModule
  ],
  declarations: [FacultyAllprogressPage]
})
export class FacultyAllprogressPageModule {}
