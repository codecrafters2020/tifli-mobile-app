import { Component,OnInit } from '@angular/core';
import { NavController,Platform } from '@ionic/angular';
import { GlobalVars } from '../../providers/global-vars.service';
import { Api } from '../../providers/api/api.service';
import * as moment from 'moment';
import { DocumentViewer } from '@ionic-native/document-viewer/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import { ParentParamsService } from 'src/app/providers/parent-params.service';

@Component({
  selector: 'app-parent-diary',
  templateUrl: './parent-diary.page.html',
  styleUrls: ['./parent-diary.page.scss'],
})
export class ParentDiaryPage implements OnInit{
  today: number;
  studentname:any;
  diaryShow;
  student: any;
  diaryapi: any=[];
  yellowdate ='assets/img/notice/yellowdate.png';
  bluedate ='assets/img/notice/bluedate.png';
  pinkdate ='assets/img/notice/pinkdate.png';
  from_date: any;
  constructor(
    public navCtrl: NavController,
    public globalVar: GlobalVars,
    public api: Api,
    private viewer : DocumentViewer,
    private platform:Platform,
    private file:File,
    private transfer:FileTransfer,
    public parentparam : ParentParamsService

) {
      this.diaryapi=[];
    }

  ngOnInit() {
    console.log('ionViewDidLoad ParentsDiaryPage');
    this.today = Date.now();
    this.student = this.globalVar.student;
    var date = moment.utc().format('YYYY-MM-DD');
    var stillUtc = moment.utc().format('YYYY-MM-DD');
    var dateend = moment().utc().subtract(7,'d').format('YYYY-MM-DD');
    console.log( stillUtc);
    console.log(dateend);
    this.api.get("parent/course_diaries/get_student_diaries.json",{
      start_date:dateend,end_date:stillUtc,
      class_section_id:this.student.class_section.id,student_id:this.student.id}).subscribe(data=>{
      console.log(data);
      this.diaryapi=data["course_diaries"];
      // for(var i=0;i<this.diaryapi.length;i++)
      // {
      //   this.diaryinfo={diaryinfo:this.diaryapi[i].diary_info,
      //     subject:this.diaryapi[i].section_course.course.course_name,url:this.diaryapi[i].image_url};
      //     this.diaryShow.push(this.diaryinfo);
      // }
    });

  }

  getDiary()
  {
    this.diaryapi=[];
    this.api.get("parent/course_diaries/get_student_diaries.json",{start_date:this.from_date
      ,end_date:this.from_date,class_section_id:this.student.class_section.id,
      student_id:this.student.id}).subscribe(data=>{
      console.log(data);
      if(!data["message"])
      {
        this.diaryapi=data["course_diaries"];
      }
    });
  }

  showDiary(i)
  {
    this.parentparam.diarydetail = this.diaryapi[i];
    this.navCtrl.navigateForward('parent-diarydetail');
  }
  downloadAndOpenPdf()
  {
    let path = null;
      if(this.platform.is('ios'))
      {
          path=this.file.documentsDirectory;
      }
      else
      {
        path=this.file.dataDirectory;
      }
      const transfer = this.transfer.create();
      transfer.download(this.diaryapi.document,path+'diary.pdf').then(data=>{
          let url = data.nativeURL;
          this.viewer.viewDocument(url,'application/pdf',{});
        });
  }

}
