import { Component , OnInit} from '@angular/core';
import { NavController } from '@ionic/angular';
import { GlobalVars } from '../../providers/global-vars.service';
import { Api } from '../../providers/api/api.service';
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media/ngx';
@Component({
  selector: 'app-faculty-allvideo',
  templateUrl: './faculty-allvideo.page.html',
  styleUrls: ['./faculty-allvideo.page.scss'],
})
export class FacultyAllvideoPage implements OnInit{
  allvideo:any;

  constructor(
    public navCtrl: NavController,
    public globalVar: GlobalVars,
    public api: Api,
    private streamingMedia: StreamingMedia
) {
      this.allvideo=[];
  }

  ngOnInit() {
    console.log('ionViewDidLoad TeacherAllvideoshowPage');
    this.api.get("faculty/videos/show_all_videos.json").subscribe(data=>{
      console.log(data);
      this.allvideo=data;
    })
  }
  uploadvideo()
  {
    this.navCtrl.navigateRoot('faculty-videoupload');
  }
  closeVideo()
  {
//    this.videoPlayer.close();1
  }
  playvideo(videourl)
  {
    let options: StreamingVideoOptions = {
      successCallback: () => { console.log('Video played') },
      errorCallback: (e) => { console.log('Error streaming') },
      orientation: 'landscape',
      shouldAutoClose: true,
      controls: true
    };

    this.streamingMedia.playVideo(videourl, options);

    // let options:VideoOptions={
    //   volume:0.7,
    // }
    // this.videoPlayer.play(videourl,{
    //   volume:0.7,
    // });
    // setTimeout(data=>{
    //     this.videoPlayer.close();
    // },3000);

  }

}
