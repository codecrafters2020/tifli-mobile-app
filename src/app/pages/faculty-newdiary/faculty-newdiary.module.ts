import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacultyNewdiaryPageRoutingModule } from './faculty-newdiary-routing.module';

import { FacultyNewdiaryPage } from './faculty-newdiary.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacultyNewdiaryPageRoutingModule
  ],
  declarations: [FacultyNewdiaryPage]
})
export class FacultyNewdiaryPageModule {}
