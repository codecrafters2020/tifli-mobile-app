import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacultyPreviousdiaryPage } from './faculty-previousdiary.page';

describe('FacultyPreviousdiaryPage', () => {
  let component: FacultyPreviousdiaryPage;
  let fixture: ComponentFixture<FacultyPreviousdiaryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacultyPreviousdiaryPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacultyPreviousdiaryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
