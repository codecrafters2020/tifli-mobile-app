import { Component,OnInit, ViewChild } from '@angular/core';
import { NavController,Platform,ModalController,NavParams } from '@ionic/angular';
import { Api } from '../../providers/api/api.service';
import { GlobalVars } from '../../providers/global-vars.service';
import { FacultyParamsService } from '../../providers/faculty-params.service'
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-faculty-newprogress',
  templateUrl: './faculty-newprogress.page.html',
  styleUrls: ['./faculty-newprogress.page.scss'],
})
export class FacultyNewprogressPage implements OnInit{

  @ViewChild('slides1',{static:true}) slides: IonSlides;
  images: any;

  constructor(
    public navCtrl: NavController,
    public api: Api,
    public globalVar: GlobalVars,
    // private viewCtrl: ViewController,
    // private ionicApp: IonicApp,
    public modalctrl: ModalController,
    public platform: Platform,
    // public app: App,
    public facultyParam: FacultyParamsService,
    public navParams: NavParams
) {
  }

  ngOnInit() {
    console.log('ionViewDidLoad FacultyNewprogressPage');
    var i= this.navParams.get('index');
    this.images=this.navParams.get('player');
    this.slideJump(i)
    // this.platform.registerBackButtonAction(() => {
    //   let nav = this.app.getActiveNavs()[0];
    //   let activeModal=this.ionicApp._modalPortal.getActive();
    //   if(activeModal){
    //        activeModal.dismiss();
    //        if (nav.canGoBack()) {
    //         //Can we go back?
    //         nav.pop();
    //       }
    //     return;
    //       // if(this.globalVar.current_user.role=='faculty')
    //       //  {
    //       //    this.navCtrl.setRoot("AlbumPage")
    //       //    return;
    //       //   }
    //       //  else
    //       //  {
    //       //    this.navCtrl.setRoot("ParentAlbumPage")
    //       //    return;
    //       //   }
    //      }
    // });
  }
  async slideJump(i)
  {
    console.log(await this.slides.getActiveIndex());
    this.slides.slideTo(i,300);  
  }
  dismiss()
  {
    this.modalctrl.dismiss();
  }
  previouspage()
  {
      if(this.globalVar.current_user.role=='parent')
      {
        this.navCtrl.navigateRoot('parent-album');
      } 
      else
      {
        this.navCtrl.navigateRoot('album');
      }
  }
}

