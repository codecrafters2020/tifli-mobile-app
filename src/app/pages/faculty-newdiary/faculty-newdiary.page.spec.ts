import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacultyNewdiaryPage } from './faculty-newdiary.page';

describe('FacultyNewdiaryPage', () => {
  let component: FacultyNewdiaryPage;
  let fixture: ComponentFixture<FacultyNewdiaryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacultyNewdiaryPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacultyNewdiaryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
