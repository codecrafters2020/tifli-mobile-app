import { Component, OnInit,TestabilityRegistry, ViewChild, ElementRef } from '@angular/core';
import { NavController, ModalController, ToastController, Platform } from '@ionic/angular';
import { GlobalVars } from '../../providers/global-vars.service';
import { Api } from '../../providers/api/api.service';
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';
import {  NgZone } from '@angular/core';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';

declare var google;
var directionsService = new google.maps.DirectionsService();
var directionsRenderer = new google.maps.DirectionsRenderer();

@Component({
  selector: 'app-driver-home',
  templateUrl: './driver-home.page.html',
  styleUrls: ['./driver-home.page.scss'],
})
export class DriverHomePage implements OnInit{

  @ViewChild('map') mapElement: ElementRef;
  map: any;

  public myDate:any;
  status: any;
  global: any;
  showstartbutton: boolean;
  showendbutton: boolean;
  statusTosend
  lat:any;
  lng: any;
  watch: any;
  current_position: any;
  interval: any;
  markers: any;
  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public globalVar: GlobalVars,
    public api: Api,
    public geolocation: Geolocation,
    public zone: NgZone,
    public toastCtrl: ToastController,
    private diagnostic: Diagnostic,
    private locationAccuracy: LocationAccuracy,
    public platform: Platform,
    ) {
    this.myDate= new Date().getTime();
    this.global = this.globalVar.current_user
    this.statusTosend=[];
    this.markers=[];
  }


  // go to another Page
  openPage(page){
    this.navCtrl.navigateRoot(page);
  }
  ngOnInit() {

    this.getStatus();
    if (this.platform.is('cordova')) {
      this.diagnostic.isLocationEnabled().then((enabled) => {
        if (enabled) {
          this.askForHighAccuracy();
        } else {
          this.locationAccuracy.canRequest().then((canRequest: boolean) => {
            this.askForHighAccuracy();
          });
        }
      })
    }
    // else {
    //   this.loadMap();
    // }

    this.getCurrentLocation()

    let self=this;
    this.interval = setInterval(function () {
      self.getlatlng();

    }, 10000);


  }
    getCurrentLocation()
    {
      var i1 = 0.00150
      var i2 = 0.00160
      var i3 = 0.00140

      this.geolocation.getCurrentPosition().then((position) => {
        this.current_position = position;
        this.lat = position.coords.latitude;
        this.lng= position.coords.longitude;
        var  latLng = new google.maps.LatLng(this.lat, this.lng);
        let mapOptions = {
          center: latLng,
          zoom:19,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          mapTypeControl: false,
          zoomControl: false,
          streetViewControl: false
        }
         if (!this.map) {
          this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
          this.loadMarkers();
        }

        let location = new google.maps.LatLng(this.lat+i1,this.lng-i1);
        let location1 = new google.maps.LatLng(this.lat+i2,this.lng-i2);
        let location2 = new google.maps.LatLng(this.lat+i3,this.lng-i3);
        this.addDummyMarker(location);
        this.addDummyMarker(location1);
        this.addDummyMarker(location2);

      });


    }
    loadMarkers(){
      this.deleteMarkers();

      let updatelocation = new google.maps.LatLng(this.lat,this.lng);
          this.addMarker(updatelocation);
       this.setMapOnAll(this.map);


    }
    setMapOnAll(map) {
      for (var i = 0; i < this.markers.length; i++) {
        this.markers[i].setMap(map);
      }
        // this.dropmarkers[0].setMap(map);

    }

    deleteMarkers() {
      this.clearMarkers();
      this.markers = [];
   //   this.dropmarkers=
    }
    clearMarkers() {
      this.setMapOnAll(null);
    }
    addDummyMarker(location)
    {
      let marker = new google.maps.Marker({
        position: location,
        map: this.map,
        animation: google.maps.Animation.DROP,
        icon: 'assets/img/truck_pin.png'
      });
    }
    addMarker(location) {
      let marker = new google.maps.Marker({
        position: location,
        map: this.map,
        animation: google.maps.Animation.DROP,
      });
      this.markers.push(marker);
    }
     askForHighAccuracy() {
    this.locationAccuracy
      .request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY);
  }
  getStatus()
  {
    this.api.get("van_driver/vans/"+this.globalVar.current_user.van.id).subscribe((data)=>
    {
      this.statusTosend=data["van"]
      this.status=data["van"]["status"]
      if(data["van"]["status"]=="end_to_school")
      {
        this.status="end to school"
      }
      else if(data["van"]["status"]=="end_from_school")
      {
        this.status = "end from school"
      }
      else if(data["van"]["status"]=="start_from_school")
      {
        this.status = "start from school"
      }
      else{
        this.status = "end to school"
      }
     if(this.status=="free" || this.status=="end to school" || this.status=="end from school")
     {
         this.showstartbutton = true;
         this.showendbutton=false;
     }
     else
     {
       this.showendbutton = true;
       this.showstartbutton = false;
     }
    });

  }
  async StartRide() {
    // let modal = await this.modalCtrl.create('DiningLocationPage',{id:this.globalVar.current_user.van.id,
    //   status:this.status});
    // modal.onDidDismiss(item => {
    //   this.getStatus()
    // })
    // modal.present();

  }

  endRide()
  {
      if(this.status=="start to school")
      {
//        this.statusTosend="end-to-school";
        this.api.post("van_driver/vans/update_status.json",{id:this.globalVar.current_user.van.id,
          status:"end_to_school"})
        .subscribe(async data=>
          {
            console.log(data);
            this.getStatus();
            let toast = await this.toastCtrl.create({
              message: "Status updated successfully",
              duration: 6000,
              position: 'top',
              showCloseButton: true,
              closeButtonText: "x",
              cssClass: "toast-success"
            });
            await toast.present();
          })
      }
      else if(this.status=="start from school")
      {
  //      this.statusTosend="end-from-school"
        this.api.post("van_driver/vans/update_status.json",{id:this.globalVar.current_user.van.id,
          status:"end_from_school"})
        .subscribe(async data=>
          {
            console.log(data);
            this.getStatus();
            let toast = await this.toastCtrl.create({
              message: "Status updated successfully",
              duration: 6000,
              position: 'top',
              showCloseButton: true,
              closeButtonText: "x",
              cssClass: "toast-success"
            });
            await toast.present();
          });

      }
  }
  ngOnDestroy() {
    if (this.interval) {
      clearInterval(this.interval);
    }
    // this.locationTracker.stopTracking();


  }

  getlatlng()
  {

    if(this.status=="start_to_school" || this.status=="start_from_school")
    {
      this.geolocation.getCurrentPosition().then((position) => {
        this.current_position = position;
        console.log(this.current_position);
        this.lat = position.coords.latitude;
        this.lng= position.coords.longitude;
        console.log(this.statusTosend);
        this.map.setCenter({lat:this.lat,lng:this.lng})

        this.updatelatlng();
      });

    }
  }
  updatelatlng()
  {

     this.api.post("van_driver/vans/update_van_location.json",{id:this.globalVar.current_user.van.id,
      latitude:this.lat,longitude:this.lng}).subscribe(data=>{
      console.log(data)
    })

  }

}
