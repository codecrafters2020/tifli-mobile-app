import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacultyPtmchatPage } from './faculty-ptmchat.page';

describe('FacultyPtmchatPage', () => {
  let component: FacultyPtmchatPage;
  let fixture: ComponentFixture<FacultyPtmchatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacultyPtmchatPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacultyPtmchatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
