import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ParentNoticedetailPage } from './parent-noticedetail.page';

describe('ParentNoticedetailPage', () => {
  let component: ParentNoticedetailPage;
  let fixture: ComponentFixture<ParentNoticedetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentNoticedetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ParentNoticedetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
