import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentNewptmPageRoutingModule } from './parent-newptm-routing.module';

import { ParentNewptmPage } from './parent-newptm.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentNewptmPageRoutingModule
  ],
  declarations: [ParentNewptmPage]
})
export class ParentNewptmPageModule {}
