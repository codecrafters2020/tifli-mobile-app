import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacultyNewnoticePageRoutingModule } from './faculty-newnotice-routing.module';

import { FacultyNewnoticePage } from './faculty-newnotice.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacultyNewnoticePageRoutingModule
  ],
  declarations: [FacultyNewnoticePage]
})
export class FacultyNewnoticePageModule {}
