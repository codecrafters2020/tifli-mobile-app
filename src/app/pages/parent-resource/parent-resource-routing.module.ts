import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParentResourcePage } from './parent-resource.page';

const routes: Routes = [
  {
    path: '',
    component: ParentResourcePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParentResourcePageRoutingModule {}
