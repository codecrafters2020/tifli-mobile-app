import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParentTimetablePage } from './parent-timetable.page';

const routes: Routes = [
  {
    path: '',
    component: ParentTimetablePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParentTimetablePageRoutingModule {}
