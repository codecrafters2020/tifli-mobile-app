import { Component,OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { GlobalVars } from '../../providers/global-vars.service';
import { Api } from '../../providers/api/api.service';
import * as moment from 'moment';

@Component({
  selector: 'app-parent-studentsummary',
  templateUrl: './parent-studentsummary.page.html',
  styleUrls: ['./parent-studentsummary.page.scss'],
})
export class ParentStudentsummaryPage implements OnInit{

  click_mark=false;
  show_comment=false;
  presents:any;
  absents:any;
  leaves:any;
  total:any;
  student_name:any;
  class:any;
  section:any;
  dob:any;
  enrol:any;
  gen:any;
  age:any;
  address:any;
  schoolname:any;
  imageStd:any;
  absentbox='assets/img/profile/absents.png';
  presentbox='assets/img/profile/Presents.png';
  leavebox='assets/img/profile/leaves.png';
  totalbox='assets/img/profile/total.png';
  biobox ='assets/img/profile/bio box.png';
  gender='assets/img/profile/Gender.png';
  enroll='assets/img/profile/enroll.png';
  classa='assets/img/profile/Class.png';
  sectiona ='assets/img/profile/Section.png';
  filtermonthwise;
  student_progress;
  constructor(
    public navCtrl: NavController,
    public globalVar: GlobalVars,
    public api: Api,
    ) {
    this.student_name = this.globalVar.student;
    this.class = this.student_name.class_section.school_class.class_name;
    this.section = this.student_name.class_section.section.section_name;
    this.dob = this.student_name.date_of_birth;
    this.enrol = this.student_name.enrollment_number;
    this.gen = this.student_name.gender;
    this.age = moment().diff(this.dob,'years');
    this.address = this.student_name.address;
    this.imageStd = this.student_name.image_url;
    this.schoolname = this.student_name.school_name.school_name;
    if(this.imageStd==null ){
      this.imageStd = "assets/img/profile/picture.png";
    }
    this.student_progress=[];
  }

  ngOnInit() {
    console.log('ionViewDidLoad NewsDetailsPage');
        var date=new Date();
      var d=moment(date);
      this.loadResource(d.month()+1);

    this.api.get('parent/student/get_student_attendance_details.json',
    {student_id: this.globalVar.student.id}).subscribe((res: any) => {
      if (res != null) {
        this.presents=res.student_attendance_details[0];
        this.absents=res.student_attendance_details[1];
        this.leaves=res.student_attendance_details[2];
        this.total = this.presents+this.absents+this.leaves;

      }
      else{
        this.presents=0
        this.absents=0
        this.leaves=0
        this.total = 0
      }
    }, err => {
      this.presents=0
      this.absents=0
      this.leaves=0
      this.total = 0
        console.error('ERROR', err);
    });

  }
  loadResource(filtermonthwise)
  {
    this.student_progress=[];
    this.api.get("parent/progress/get_student_progress_by_month.json",
    {student_id: this.globalVar.student.id,month:filtermonthwise}).subscribe(data=>{
      console.log(data);
      this.student_progress=data["student_progresses"]["0"]["progress_ratings"];
    })
  }
  add_comment(){
    this.show_comment=(this.show_comment==true)?false:true;
  }
  // click mark book function
  marked(){
    this.click_mark=(this.click_mark==true)?false:true;
    console.log(this.click_mark);
  }

  // click heart btn
  clicked_heart=false;
  num_clicked=50;
  heart_color="color3";
  click_like(){
    if(this.clicked_heart==false){
      this.clicked_heart =true;
      this.num_clicked=this.num_clicked+1;
      this.heart_color="danger";
    }
    else{
      this.clicked_heart =false;
      this.num_clicked=this.num_clicked-1;
      this.heart_color="color3";
    }
  }

}
