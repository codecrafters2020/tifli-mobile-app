import { Component, OnInit, NgZone } from '@angular/core';
import { NavController, ToastController, LoadingController, ActionSheetController } from '@ionic/angular';
import { Api } from '../../providers/api/api.service';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/camera/ngx';
import { AwsProvider } from '../../providers/aws.service';
import b64toBlob from 'b64-to-blob';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer/ngx';
import { File } from '@ionic-native/file/ngx';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { GlobalVars } from '../../providers/global-vars.service';
import { FacultyParamsService } from '../../providers/faculty-params.service';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';

@Component({
  selector: 'app-faculty-individualdiary',
  templateUrl: './faculty-individualdiary.page.html',
  styleUrls: ['./faculty-individualdiary.page.scss'],
})
export class FacultyIndividualdiaryPage implements OnInit {
  today: any;
  diaryPage: any;
  students: any;
  class: any;
  section: any;
  course: any;
  diarytext;
  courseid: any;
  studentid;
  notice: any;
  images: any;
  subject: any;
  avatar: any;
  loading: any;
  loader: any;
  document: any;
  documentToUpload: any;
  filename: any;
  classdata: any;
  sectionsfromapi: any;
  sectionid
  id;
  private win: any = window;
  fileTransfer:FileTransferObject

  constructor(
    public navCtrl: NavController,
    public api: Api,
    public toastCtrl: ToastController,
    private loadingController: LoadingController,
    public camera: Camera,
    private actionSheetController: ActionSheetController,
    public zone: NgZone,
    private awsProvider: AwsProvider,
    private viewer: DocumentViewer,
    private file: File,
    private transfer: FileTransfer,
    public globalVar: GlobalVars,
    public facultyparams: FacultyParamsService,
    private FilePath:FilePath,
    private fileChooser:FileChooser  
  ) {
    if (this.facultyparams.notice_individual == 1) {
      this.students = this.facultyparams.notice_individualstudents;
      this.class = this.facultyparams.notice_individualclass;
      this.section = this.facultyparams.notice_individualsection;
     // this.course = this.navParams.get("coursename");
      this.courseid = this.facultyparams.notice_individualcourseid;
      this.notice = this.facultyparams.notice_individual;
    }
    else {
      this.classdata = [];
      this.sectionsfromapi = [];
      this.course = [];

    }
  }

  async ngOnInit() {
    const loading = await this.loadingController.create({
      // content:"loading data..."
    });
    await loading.present();

    console.log('ionViewDidLoad IndividualDiaryPage');
    console.log(this.students);
    this.api.get("faculty/section_courses/get_faculty_teached_class.json",
      { faculty_id: this.globalVar.current_user.information.id }).subscribe(data => {
        loading.dismiss();
        this.classdata = data["classes"];
        console.log(this.classdata)
        //  this.coursesArray={class:this.coursedata[i].school_class_id,section:this.sections};
      }, err => {
        loading.dismiss();
      });

  }
  async sendCourse(courseid) {
    const loading = await this.loadingController.create({
      // content:"loading data..."
    });
    await loading.present();

    this.api.get("faculty/section_courses/get_faculty_teached_section.json",
      { faculty_id: this.globalVar.current_user.information.id, school_class_id: courseid })
      .subscribe(data => {
        this.sectionsfromapi = data["class_sections"];
        loading.dismiss();
      }, err => {
        loading.dismiss();
      });

  }
  async getCourse(sectionid) {
    const loading = await this.loadingController.create({
      // content:"loading data..."
    });
    await loading.present();

    this.api.get("faculty/section_courses/get_section_courses.json", {
      section_id: sectionid,
      faculty_id: this.globalVar.current_user.information.id
    }).
      subscribe(data => {
        this.course = data["section_courses"]
        console.log(data)
        loading.dismiss();
      }, err => {
        loading.dismiss();
      });

  }
  changeStudents(id) {
    for (var i = 0; i < this.course.length; i++) {
      if (id == this.course[i].course.id) {
        this.students = this.course[i].students;
      }
    }
  }
  async sendDiary() {
    const loading = await this.loadingController.create({
    });
    await loading.present();
    let that = this;
    if (this.images != null) {
      let ext = 'jpg';
      let type = 'image/jpeg'
      let newName = this.awsProvider.randomString(6) + new Date().getTime() + '.' + ext;

      this.awsProvider.getSignedUploadRequest(newName, ext, type).subscribe(data => {
        console.log("Success in getSignedUploadRequest", data)
        that.avatar = data["public_url"]
        console.log("i am in public url", data["public_url"])

        that.zone.run(() => {
          let request = new XMLHttpRequest();
          request.open('GET', this.images, true);
          request.responseType = 'blob';
          request.timeout = 360000;
          request.onload = function () {
            let reader = new FileReader();
            reader.readAsDataURL(request.response);
            reader.onload = function (e) {
              that.zone.run(() => {
                console.log("error", e);
                var b1 = <string>reader.result
                let blob = b64toBlob(b1 .replace(/^data:image\/\w+;base64,/, ""), 'jpg');
                // let blob = b64toBlob(reader.result.replace(/^data:image\/\w+;base64,/, ""), 'jpg');

                that.awsProvider.uploadFile(data["presigned_url"], blob).subscribe(_result => {

                  console.log("result that i got after uploading to aws", _result)
                  that.api.post("faculty/course_diaries.json", {
                    student_id: that.studentid, section_course_id: that.id, diary_info: that.diarytext,
                    image_url: that.avatar, document: null
                  })
                    .subscribe(async data => {
                      loading.dismiss();
                      let toast = await that.toastCtrl.create({
                        message: "Diary Send To Individual",
                        duration: 4000,
                        position: 'top',
                        showCloseButton: true,
                        closeButtonText: "x",
                        cssClass: "toast-success",
                        color:"success"
                      });
                      await toast.present();

                      that.navCtrl.navigateRoot('faculty-home');
                    }, async err => {
                      loading.dismiss();
                      let toast = await that.toastCtrl.create({
                        message: "Something went wrong.",
                        duration: 4000,
                        position: 'top',
                        showCloseButton: true,
                        closeButtonText: "x",
                        cssClass: "toast-danger",
                        color:"danger"
                      });
                      await toast.present();
                    })

                });
              })

            }

            reader.onerror = function (e) {
              console.log('We got an error in file reader:::::::: ', e);
            };

            reader.onabort = function (event) {
              console.log("reader onabort", event)
            }

          };

          request.onerror = function (e) {
            console.log("** I got an error in XML Http REquest", e);
          };

          request.ontimeout = function (event) {
            console.log("xmlhttp ontimeout", event)
          }

          request.onabort = function (event) {
            console.log("xmlhttp onabort", event);
          }


          request.send();
        });
      }, async (err) => {
        let toast = await this.toastCtrl.create({
          message: "Unable to send request. Check wifi connection",
          duration: 6000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger",
          color:"danger"
        });
        await toast.present();
        console.log('getSignedUploadRequest timeout error: ', err);
      });
    }
    else {
      this.api.post("faculty/course_diaries.json", {
        student_id: this.studentid, section_course_id: this.id, diary_info: this.diarytext,
        image_url: null, document: null
      })
        .subscribe(async data => {
          loading.dismiss();
          let toast = await this.toastCtrl.create({
            message: "Diary Send To Individual",
            duration: 4000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-success",
            color:"success"
         });
          await toast.present();

          this.navCtrl.navigateRoot('faculty-home');
        }, async err => {
          loading.dismiss();
          let toast = await this.toastCtrl.create({
            message: "Something went wrong.",
            duration: 4000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-danger",
            color:"danger"
          });
          await toast.present();
        })

    }

  }
  openPdf() {
    var options: DocumentViewerOptions = {
      title: 'My PDF'
    }
    this.viewer.canViewDocument(this.documentToUpload, 'application/pdf', options, this.openPdf2, this.onmissing)

  }
  onmissing() {
    alert("Sorry! Cannot show document.");
  }
  openPdf2() {
    this.viewer.viewDocument(this.documentToUpload, 'application/pdf', {})
  }
  disabled() {
    if (this.studentid == null || this.studentid == undefined) {
      return true;
    }
    else {
      return false;
    }
  }
  async  sendNotice() {
    const loading = await this.loadingController.create({
    });
    await loading.present();
    if (this.images != null) {
      let that = this;

      let ext = 'jpg';
      let type = 'image/jpeg'
      let newName = this.awsProvider.randomString(6) + new Date().getTime() + '.' + ext;

      this.awsProvider.getSignedUploadRequest(newName, ext, type).subscribe(data => {
        console.log("Success in getSignedUploadRequest", data)
        that.avatar = data["public_url"]
        console.log("i am in public url", data["public_url"])

        that.zone.run(() => {
          let request = new XMLHttpRequest();
          request.open('GET', this.images, true);
          request.responseType = 'blob';
          request.timeout = 360000;
          request.onload = function () {
            let reader = new FileReader();
            reader.readAsDataURL(request.response);
            reader.onload = function (e) {
              that.zone.run(() => {
                console.log("error", e);
                var b1 = <string>reader.result
                let blob = b64toBlob(b1 .replace(/^data:image\/\w+;base64,/, ""), 'jpg');
                // let blob = b64toBlob(reader.result.replace(/^data:image\/\w+;base64,/, ""), 'jpg');

                that.awsProvider.uploadFile(data["presigned_url"], blob).subscribe(_result => {

                  console.log("result that i got after uploading to aws", _result)
                  that.api.post("faculty/section_notices.json", {
                    student_id: that.studentid,
                    class_section_id: that.courseid, notice_info: that.diarytext, subject: that.subject,
                    image_url: that.avatar, document: null
                  })
                    .subscribe(async data => {
                      let toast = await that.toastCtrl.create({
                        message: "Notice Send To Individual",
                        duration: 6000,
                        position: 'top',
                        showCloseButton: true,
                        closeButtonText: "x",
                        cssClass: "toast-success",
                        color:"success"
                      });
                      await toast.present();
                      loading.dismiss();
                      that.navCtrl.navigateRoot('faculty-home');
                    }, async err => {
                      loading.dismiss();
                      let toast = await that.toastCtrl.create({
                        message: "Something went wrong",
                        duration: 6000,
                        position: 'top',
                        showCloseButton: true,
                        closeButtonText: "x",
                        cssClass: "toast-danger",
                        color:"danger"

                      });
                      await toast.present();
                    })
                });
              })

            }

            reader.onerror = function (e) {
              console.log('We got an error in file reader:::::::: ', e);
            };

            reader.onabort = function (event) {
              console.log("reader onabort", event)
            }

          };

          request.onerror = function (e) {
            console.log("** I got an error in XML Http REquest", e);
          };

          request.ontimeout = function (event) {
            console.log("xmlhttp ontimeout", event)
          }

          request.onabort = function (event) {
            console.log("xmlhttp onabort", event);
          }


          request.send();
        });
      }, async (err) => {
        let toast = await this.toastCtrl.create({
          message: "Unable to send request. Check wifi connection",
          duration: 6000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger",
          color:"danger"
        });
        await toast.present();
        console.log('getSignedUploadRequest timeout error: ', err);
      });

    }
    else {
      let that = this;
      that.api.post("faculty/section_notices.json", {
        student_id: that.studentid,
        class_section_id: that.courseid, notice_info: that.diarytext, subject: that.subject,
        image_url: null, document: null
      })
        .subscribe(async data => {
          let toast = await that.toastCtrl.create({
            message: "Notice Send To Individual",
            duration: 6000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-success",
            color:"success"

          });
          await toast.present();
          loading.dismiss();
          this.navCtrl.navigateRoot('faculty-home');
        }, async err => {
          loading.dismiss();
          let toast = await that.toastCtrl.create({
            message: "Something went wrong",
            duration: 6000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-danger",
            color:"danger"

          });
          await toast.present();
        })

    }

  }
  async uploadDocumentNotice() {
    this.loader = await this.loadingController.create({
    });
    await this.loader.present();
    let that = this;
    if (this.documentToUpload != null) {
      var fileName = this.documentToUpload.substr(this.documentToUpload.lastIndexOf('/') + 1);
      this.filename = fileName;
      var fileExtension = fileName.substr(fileName.lastIndexOf('/') + 1);
      let ext = fileExtension;
      let type = 'application/' + fileExtension;
      let newName = this.awsProvider.randomString(6) + new Date().getTime() + '.' + ext;
      console.log("**new NAme **", newName);

      this.awsProvider.getSignedUploadRequest(newName, ext, type).subscribe(data => {

        console.log("** I got success in getSignedUploadRequest", data);
        this.document = data["public_url"];
        that.zone.run(() => {
          let request = new XMLHttpRequest();
          request.open('GET', this.documentToUpload, true);
          request.responseType = 'blob';
          request.timeout = 360000;
          request.onload = function () {
            that.zone.run(() => {
              let reader = new FileReader();
              reader.readAsDataURL(request.response);

              console.log("Step 1 reader", reader);
              console.log("Step 1 got stuck in request.response", request)

              reader.onload = function () {
                that.zone.run(() => {
                  console.log("Step 2 Loaded", reader.result);
                  var b1 = <string>reader.result
                  let blob = b64toBlob(b1 .replace(/^data:application\/\w+;base64,/, ""), fileExtension);
                  // let blob = b64toBlob(reader.result.replace(/^data:application\/\w+;base64,/, ""), fileExtension);
                  console.log("blob", blob);
                  that.awsProvider.uploadFile(data["presigned_url"], blob).subscribe(_result => {
                    console.log("result that i got after uploading to aws", _result)
                    that.api.post("faculty/section_notices.json", {
                      student_id: that.studentid,
                      class_section_id: that.courseid, notice_info: that.diarytext, subject: that.subject,
                      image_url: null, document: that.document
                    })
                      .subscribe(async data => {
                        let toast = await that.toastCtrl.create({
                          message: "Notice Send To Individual",
                          duration: 6000,
                          position: 'top',
                          showCloseButton: true,
                          closeButtonText: "x",
                          cssClass: "toast-success",
                          color:"success"

                        });
                        await toast.present();
                        that.loader.dismiss();
                        that.navCtrl.navigateRoot('faculty-home');
                      }, async err => {
                        that.loader.dismiss();
                        let toast = await that.toastCtrl.create({
                          message: "Something went wrong",
                          duration: 6000,
                          position: 'top',
                          showCloseButton: true,
                          closeButtonText: "x",
                          cssClass: "toast-danger",
                          color:"danger"

                        });
                        await toast.present();
                      })
                  }, (err) => {
                    console.log('err in uploadFile:::::::: ', err);
                  });
                });
              }

              reader.onerror = function (e) {
                console.log('We got an error in file reader:::::::: ', e);
              };

              reader.onabort = function (event) {
                console.log("reader onabort", event)
              }
            })
          };

          request.onerror = function (e) {
            that.loader.dismiss();
            console.log("** I got an error in XML Http REquest", e);
          };

          request.ontimeout = function (event) {
            that.loader.dismiss();
            console.log("xmlhttp ontimeout", event)
          }

          request.onabort = function (event) {
            that.loader.dismiss();
            console.log("xmlhttp onabort", event);
          }


          request.send();
        })
      }, async (err) => {
        that.loader.dismiss();
        console.log('getSignedUploadRequest timeout error: ', err);

        let toast = await this.toastCtrl.create({
          message: "Unable to send request. Check wifi connection",
          duration: 6000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger",
          color:"danger"

        });
        await toast.present();

      });


    }
    else {
      let that = this;
      that.api.post("faculty/section_notices.json", {
        student_id: that.studentid,
        class_section_id: that.courseid, notice_info: that.diarytext, subject: that.subject,
        image_url: null, document: null
      })
        .subscribe(async data => {
          let toast = await that.toastCtrl.create({
            message: "Notice Send To Individual",
            duration: 6000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-success",
            color:"success"

          });
          await toast.present();
          that.loader.dismiss();
          this.navCtrl.navigateRoot('faculty-home');
        }, async err => {
          that.loader.dismiss();
          let toast = await that.toastCtrl.create({
            message: "Something went wrong",
            duration: 6000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-danger",
            color:"danger"

          });
          await toast.present();
        })

    }
  }
  async uploadDocumentDiary() {
    this.loader = await this.loadingController.create({
    });
    await this.loader.present();
    let that = this;
    if (this.documentToUpload != null) {
      var fileName = this.documentToUpload.substr(this.documentToUpload.lastIndexOf('/') + 1);
      var fileExtension = fileName.substr(fileName.lastIndexOf('/') + 1);
      let ext = fileExtension;
      let type = 'application/' + fileExtension;
      let newName = this.awsProvider.randomString(6) + new Date().getTime() + '.' + ext;
      console.log("**new NAme **", newName);

      this.awsProvider.getSignedUploadRequest(newName, ext, type).subscribe(data => {

        console.log("** I got success in getSignedUploadRequest", data);
        this.document = data["public_url"];
        that.zone.run(() => {
          let request = new XMLHttpRequest();
          request.open('GET', this.documentToUpload, true);
          request.responseType = 'blob';
          request.timeout = 360000;
          request.onload = function () {
            that.zone.run(() => {
              let reader = new FileReader();
              reader.readAsDataURL(request.response);

              console.log("Step 1 reader", reader);
              console.log("Step 1 got stuck in request.response", request)

              reader.onload = function () {
                that.zone.run(() => {
                  console.log("Step 2 Loaded", reader.result);
                  var b1 = <string>reader.result
                  let blob = b64toBlob(b1 .replace(/^data:application\/\w+;base64,/, ""), fileExtension);
                  // let blob = b64toBlob(reader.result.replace(/^data:application\/\w+;base64,/, ""), fileExtension);
                  console.log("blob", blob);
                  that.awsProvider.uploadFile(data["presigned_url"], blob).subscribe(_result => {
                    console.log("result that i got after uploading to aws", _result)
                    that.api.post("faculty/course_diaries.json", {
                      student_id: that.studentid, section_course_id: that.id, diary_info: that.diarytext,
                      image_url: null, document: that.document
                    })
                      .subscribe(async data => {
                        let toast = await that.toastCtrl.create({
                          message: "Diary Send To Individual",
                          duration: 6000,
                          position: 'top',
                          showCloseButton: true,
                          closeButtonText: "x",
                          cssClass: "toast-success",
                          color:"success"

                        });
                        await toast.present();
                        that.loader.dismiss();
                        that.navCtrl.navigateRoot('faculty-home');
                      }, async err => {
                        that.loader.dismiss();
                        let toast = await that.toastCtrl.create({
                          message: "Something went wrong",
                          duration: 6000,
                          position: 'top',
                          showCloseButton: true,
                          closeButtonText: "x",
                          cssClass: "toast-danger",
                          color:"danger"

                        });
                        await toast.present();
                      })
                  }, (err) => {
                    console.log('err in uploadFile:::::::: ', err);
                  });
                });
              }

              reader.onerror = function (e) {
                console.log('We got an error in file reader:::::::: ', e);
              };

              reader.onabort = function (event) {
                console.log("reader onabort", event)
              }
            })
          };

          request.onerror = function (e) {
            that.loader.dismiss();
            console.log("** I got an error in XML Http REquest", e);
          };

          request.ontimeout = function (event) {
            that.loader.dismiss();
            console.log("xmlhttp ontimeout", event)
          }

          request.onabort = function (event) {
            that.loader.dismiss();
            console.log("xmlhttp onabort", event);
          }


          request.send();
        })
      }, async (err) => {
        that.loader.dismiss();
        console.log('getSignedUploadRequest timeout error: ', err);

        let toast = await this.toastCtrl.create({
          message: "Unable to send request. Check wifi connection",
          duration: 6000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger",
          color:"danger"

        });
        await toast.present();

      });


    }
    else {
      let that = this;
      that.api.post("faculty/course_diaries.json", {
        student_id: that.studentid, section_course_id: that.id, diary_info: that.diarytext,
        image_url: null, document: null
      })
        .subscribe(async data => {
          let toast = await that.toastCtrl.create({
            message: "Diary Send To Individual",
            duration: 6000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-success",
             color:"success"

          });
          await toast.present();
          that.loader.dismiss();
          this.navCtrl.navigateRoot('faculty-home');
        }, async err => {
          that.loader.dismiss();
          let toast = await that.toastCtrl.create({
            message: "Something went wrong",
            duration: 6000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-danger",
             color:"danger"

          });
          await toast.present();
        })

    }

  }
  cancel() {
    this.navCtrl.navigateRoot('faculty-home');
  }
  takeDocuments()
  {
    this.fileChooser.open().then(uri => {
      this.FilePath.resolveNativePath(uri).then(nativepath => {
        this.images = null;
        this.documentToUpload = this.win.Ionic.WebView.convertFileSrc(nativepath);
        var fileName = nativepath.substr(this.documentToUpload.lastIndexOf('/') + 1);
        this.filename = fileName;
     }, err => {
        alert(JSON.stringify(err));
      })
    }, err => {
      alert(JSON.stringify(err));
    })
  }
//   takeDocuments(sourceType) {

//     const options: CameraOptions = {
//       quality: 100,
//       correctOrientation: true,
//       destinationType: this.camera.DestinationType.FILE_URI,
//       //    encodingType: this.camera.EncodingType.JPEG,
//       mediaType: this.camera.MediaType.ALLMEDIA,
//       sourceType: sourceType
//     }

//     let that = this;
//     this.camera.getPicture(options).then((imageData) => {
//       this.images = null;
//       this.documentToUpload = this.win.Ionic.WebView.convertFileSrc(imageData);
// //      this.documentToUpload = imageData;
//       var fileName = imageData.substr(this.documentToUpload.lastIndexOf('/') + 1);
//       this.filename = fileName;
//     }, async (err) => {
//       let toast = await this.toastCtrl.create({
//         message: "Unable to Access Gallery. Please Try Later",
//         duration: 6000,
//         position: 'top',
//         showCloseButton: true,
//         closeButtonText: "x",
//         cssClass: "toast-danger",
//         color:"danger"
//       });
//       await toast.present();
//       console.log('err: ', err);
//     });
//   }

  // async presentActionSheetForDocuments() {
  //   let actionSheet = await this.actionSheetController.create({
  //     header: 'Select Document Source',
  //     buttons: [
  //       {
  //         text: 'Load from Library',
  //         handler: () => {
  //           this.takeDocuments(this.camera.PictureSourceType.PHOTOLIBRARY);
  //         }
  //       },
  //       // {
  //       //   text: 'Use Camera',
  //       //   handler: () => {
  //       //     this.takeDocuments(this.camera.PictureSourceType.CAMERA);
  //       //   }
  //       // },
  //       {
  //         text: 'Cancel',
  //         role: 'cancel'
  //       }
  //     ]
  //   });
  //   actionSheet.present();
  // }
  takePicture(sourceType) {
    const options: CameraOptions = {
      quality: 100,
      correctOrientation: true,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.PNG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: sourceType
    }

    this.camera.getPicture(options).then((imageData) => {
      this.images = this.win.Ionic.WebView.convertFileSrc(imageData);
      this.documentToUpload = null;

    });
  }

  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }
  openPage()
  {
      this.navCtrl.navigateRoot('notifications');
  }
}
