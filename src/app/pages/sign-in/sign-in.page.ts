import { Component } from '@angular/core';
import {  NavController,ModalController, Platform, LoadingController, ToastController } from '@ionic/angular';
import { Api } from '../../providers/api/api.service';
import { Storage } from '@ionic/storage';
import { GlobalVars } from '../../providers/global-vars.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.page.html',
  styleUrls: ['./sign-in.page.scss'],
})
export class SignInPage {
  public _email: string;
  public _password: string;
  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public api: Api,
    public storage: Storage,
    public globalVar: GlobalVars,
    public platform: Platform,
    private loadingController: LoadingController,
    public toastCtrl: ToastController,

) {

  }

  passwordModal() {
    // let modal = this.modalCtrl.create('PasswordPage');
    // modal.present();
  }

  // go to another page
  openPage(page) {
    this.navCtrl.navigateRoot(page);
  }

  async doLogin(){
    const loading = await this.loadingController.create({
    });
    await loading.present();
   let accountInfo = {};
   let userInfo={};
   userInfo = {device_id:this.globalVar.device_id,os:this.globalVar.os,sms_user_id:""};

    accountInfo['sms_user'] = {email: this._email, password: this._password}
    if(this._password!=null && this._email!=null)
    {
    this.api.post('sms_users/sign_in.json', accountInfo).subscribe(async (res: any) => {
      console.log(res);
      // If the API returned a successful response, mark the user as logged in
      if (res != null) {
        let user = res["sms_user"]
        userInfo = {device_id:this.globalVar.device_id,os:this.globalVar.os,
          sms_user_id:res["sms_user"]["information"]["sms_user_id"]};

        if(user.role=="driver"){
          this.openPage('driver-home');
          this.platform.ready().then(() => {
            this.storage.ready().then(() => {
                this.storage.set("currentuser",res["sms_user"]);
                this.globalVar.current_user = res["sms_user"];
                this.api.post("sms_users/set_login_device.json",{login_device:userInfo}).subscribe(data=>{
                  console.log(data);
              })

                loading.dismiss();
            });
          });
        }
        else if(user.role=="faculty")
        {
              this.storage.set("currentuser",res["sms_user"]);
                this.globalVar.current_user = res["sms_user"];
                this.openPage('faculty-home');
                this.api.post("sms_users/set_login_device.json",{login_device:userInfo}).subscribe(data=>{
                  console.log(data);
              });

                loading.dismiss();
         }
        else if(user.role=="parent")
        {
          this.platform.ready().then(() => {
            this.storage.ready().then(() => {
                this.storage.set("currentuser",res["sms_user"]);
                this.globalVar.current_user = res["sms_user"];
                this.openPage('parent-home');
                this.api.post("sms_users/set_login_device.json",{login_device:userInfo}).subscribe(data=>{
                  console.log(data);
              });

                loading.dismiss();
              });
          });
        }
      } else {
        console.log("Not logged in");
        loading.dismiss();
        let toast = await this.toastCtrl.create({
          message:"Something went wrong",
          duration: 4000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger",
          color:"danger"
        });
        await toast.present();


      }
    }, async err => {
      console.error('ERROR', err);
      loading.dismiss();
      let toast = await this.toastCtrl.create({
        message:"Something went wrong",
        duration: 4000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger",
        color:"danger"
      });
      await toast.present();

    });

    }
    else{
      loading.dismiss();
      let toast = await this.toastCtrl.create({
        message:"Enter email and password",
        duration: 4000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger",
        color:"danger"
      });
      await toast.present();

    }

  }
}
