import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacultyNewprogressPageRoutingModule } from './faculty-newprogress-routing.module';

import { FacultyNewprogressPage } from './faculty-newprogress.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacultyNewprogressPageRoutingModule
  ],
  declarations: [FacultyNewprogressPage]
})
export class FacultyNewprogressPageModule {}
