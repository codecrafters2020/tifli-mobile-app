import { Component,OnInit } from '@angular/core';
import { NavController,Platform } from '@ionic/angular';
import { GlobalVars } from '../../providers/global-vars.service';
import { DocumentViewer } from '@ionic-native/document-viewer/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import { ParentParamsService } from 'src/app/providers/parent-params.service';

@Component({
  selector: 'app-parent-notice',
  templateUrl: './parent-notice.page.html',
  styleUrls: ['./parent-notice.page.scss'],
})
export class ParentNoticePage implements OnInit{

  today: any;
  studentname: any;
  dummyimage="assets/img/cources/002.png";
  diaryapi: any;

  constructor(
    public navCtrl: NavController,
    public globalVar: GlobalVars,
    private viewer : DocumentViewer,
    private platform:Platform,
    private file:File,
    private transfer:FileTransfer,
    public parentparam : ParentParamsService

    ) {
      this.diaryapi=this.parentparam.noticedetail;
      console.log(this.diaryapi)
  }

  ngOnInit() {
    console.log('ionViewDidLoad ParentNoticePage');
    this.today = Date.now();
  }

  showNotice(name)
  {
  //  this.navCtrl.push("ParentNoticeDetailPage",{student:name});
  }
  previouspage()
  {
    this.navCtrl.navigateRoot('parent-noticedetail');
  }

  imageFullpage(url,diary)
  {
    this.parentparam.doc_img = diary.img_url;
    this.parentparam.diary_detaildoc = "notice";
   this.navCtrl.navigateRoot('attachedimageshow');
  }

  downloadAndOpenPdf()
  {
        if(this.platform.is('ios')){
      window.open( this.diaryapi.document , '_system');
    } else {
      let label = encodeURI('My Label');
      window.open( this.diaryapi.document , '_system','location=yes');
    }

    // let path = null;
    //   if(this.platform.is('ios'))
    //   {
    //       path=this.file.documentsDirectory;
    //   }
    //   else
    //   {
    //     path=this.file.dataDirectory;
    //   }
    //   const transfer = this.transfer.create();
    //   transfer.download(this.diaryapi.document,path+'notice.pdf').then(data=>{
    //     console.log(data);
    //     let url = data.nativeURL;
    //       this.viewer.viewDocument(url,'application/pdf',{});
    //     });
  }

}
