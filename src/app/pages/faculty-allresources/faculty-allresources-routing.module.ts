import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacultyAllresourcesPage } from './faculty-allresources.page';

const routes: Routes = [
  {
    path: '',
    component: FacultyAllresourcesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacultyAllresourcesPageRoutingModule {}
