import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ParentNewleavePage } from './parent-newleave.page';

describe('ParentNewleavePage', () => {
  let component: ParentNewleavePage;
  let fixture: ComponentFixture<ParentNewleavePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentNewleavePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ParentNewleavePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
