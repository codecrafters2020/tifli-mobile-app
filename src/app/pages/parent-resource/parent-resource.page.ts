import { Component,OnInit } from '@angular/core';
import { NavController, ToastController, LoadingController, Platform } from '@ionic/angular';
import { Api } from '../../providers/api/api.service';
import { GlobalVars } from '../../providers/global-vars.service';
import { DocumentViewer } from '@ionic-native/document-viewer/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
@Component({
  selector: 'app-parent-resource',
  templateUrl: './parent-resource.page.html',
  styleUrls: ['./parent-resource.page.scss'],
})
export class ParentResourcePage implements OnInit{
  filtermonthwise;
  subject;
  bg1image = "assets/img/blue1.png";
  bg2image = "assets/img/pink1.png";
  bg3image = "assets/img/yellow1.png";
  resourcedata: any;
  studentcourses: any;
  constructor(
    public navCtrl: NavController,
    public api: Api,
    public toastCtrl: ToastController,
    private loadingController: LoadingController,
    public globalVar: GlobalVars,
    private viewer: DocumentViewer,
    private platform: Platform,
    private file: File,
    private transfer: FileTransfer

  ) {
    this.resourcedata = [];
    this.studentcourses = [];
  }

  async ngOnInit() {
    console.log('ionViewDidLoad ParentResourcePage');
    const loading = await this.loadingController.create({
      // content:"loading data..."
    });
    await loading.present();
    this.api.get("parent/section_courses/get_student_courses.json", { class_section_id: this.globalVar.student.class_section.id }).subscribe(data => {
      console.log(data);
      this.studentcourses = data["section_courses"];
    })
    this.api.get("parent/resources/get_resource_by_student.json", { student_id: this.globalVar.student.id }).subscribe(data => {
      loading.dismiss();
      this.resourcedata = data["resources"];
    }, err => {
      loading.dismiss();
    })
  }
  async loadResource(filtermonthwise, subject) {
    console.log('1-' + filtermonthwise + '-2020');
    console.log(subject);
    // if(!subject)
    // {
    //   this.subject='nil'
    // }
    const loading = await this.loadingController.create({
      // content:"loading data..."
    });
    await loading.present();
    this.resourcedata = [];
    if (filtermonthwise && subject == undefined) {
      this.api.get("parent/resources/get_filtered_resources.json", {
        month: this.filtermonthwise,
        class_section_id: this.studentcourses[0].class_section.id
      }).subscribe(data => {
        loading.dismiss();
        if (!data["message"]) {
          this.resourcedata = data["resources"];
        }
      }, err => {
        loading.dismiss();
      })

    }
    else if (subject && filtermonthwise == undefined) {
      this.api.get("parent/resources/get_filtered_resources.json", {
        section_course_id: this.subject, class_section_id: this.studentcourses[0].class_section.id
      }).subscribe(data => {
        loading.dismiss();
        if (!data["message"]) {
          this.resourcedata = data["resources"];
        }
      }, err => {
        loading.dismiss();
      })

    }
    else {
      this.api.get("parent/resources/get_filtered_resources.json", {
        month: this.filtermonthwise,
        section_course_id: this.subject, class_section_id: this.studentcourses[0].class_section.id
      }).subscribe(data => {
        loading.dismiss();
        if (!data["message"]) {
          this.resourcedata = data["resources"];
        }
      }, err => {
        loading.dismiss();
      })

    }

  }
  downloadAndOpenPdf(url) {
        if(this.platform.is('ios')){
      window.open( url , '_system');
    } else {
      let label = encodeURI('My Label');
      window.open( url , '_system','location=yes');
    }

    // let path = null;
    // if (this.platform.is('ios')) {
    //   path = this.file.documentsDirectory;
    // }
    // else {
    //   path = this.file.dataDirectory;
    // }
    // const transfer = this.transfer.create();
    // transfer.download(url, path + 'resource.pdf').then(data => {
    //   let url = data.nativeURL;
    //   this.viewer.viewDocument(url, 'application/pdf', {});
    // });
  }

}
