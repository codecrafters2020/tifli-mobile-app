import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacultyAllprogressPage } from './faculty-allprogress.page';

const routes: Routes = [
  {
    path: '',
    component: FacultyAllprogressPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacultyAllprogressPageRoutingModule {}
