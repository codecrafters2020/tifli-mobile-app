import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParentStudentsummaryPage } from './parent-studentsummary.page';

const routes: Routes = [
  {
    path: '',
    component: ParentStudentsummaryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParentStudentsummaryPageRoutingModule {}
