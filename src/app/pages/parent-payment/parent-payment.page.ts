import { Component,OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Api } from '../../providers/api/api.service';
import { GlobalVars } from '../../providers/global-vars.service';

@Component({
  selector: 'app-parent-payment',
  templateUrl: './parent-payment.page.html',
  styleUrls: ['./parent-payment.page.scss'],
})
export class ParentPaymentPage implements OnInit{
  payment_box = 'assets/img/PaymentBox.png';
  pending_item = 'assets/img/PendPayment.png';
  approved_item = 'assets/img/TotalPay.png';
  students: any;
  classsectiontosend: any;
  payment: any;
  paymentto: any;
  paymenttoshow: any;
  totalPayment: any
  totalPaymentpayed: any;
  month: string;
  constructor(
    public navCtrl: NavController,
    public api: Api,
    public globalVar: GlobalVars,
  ) {
    this.students = [];
    this.classsectiontosend = [];
    this.payment = [];
    this.paymentto = [];
    this.paymenttoshow = [];
    this.totalPayment = 0;
    this.totalPaymentpayed = 0;
  }

  ngOnInit() {
    console.log('ionViewDidLoad ParentPaymentPage');
    this.students = this.globalVar.current_user.students;
    for (var i = 0; i < this.students.length; i++) {
      this.classsectiontosend.push(this.students[i].id);
    }
    this.getPayment(this.classsectiontosend);
  }
  getPayment(classsectionid) {
    this.api.get("parent/student_fees/show_all_fee_details.json", { student_id: classsectionid }).subscribe(data => {
      console.log(data["student_fees"]);
      this.payment = data["student_fees"];
      for (var i = 0; i < this.payment.length; i++) {
        if (this.payment[i].student.first_name == this.globalVar.student.first_name && this.payment[i].student.last_name == this.globalVar.student.last_name) {
          // var month = this.payment[i].fees_month.split("-")
          // this.getMonth(month[1]);
          this.getMonth(this.payment[i].fees_month);
          this.paymentto = {
            amount: this.payment[i].amount, duedate: this.payment[i].due_date,
            feesmonth: this.month, status: this.payment[i].payment_status,
            name: this.payment[i].student.first_name, last_name: this.payment[i].student.last_name
          };
          this.paymenttoshow.push(this.paymentto);
          if (this.payment[i].payment_status == 'pending') {
            this.totalPayment = this.payment[i].amount + this.totalPayment;
          }
          this.totalPaymentpayed = this.payment[i].amount + this.totalPaymentpayed;
          // document.getElementById("totalamountpayed").innerHTML = 'PKR '+this.totalPaymentpayed
        }
      }

    });

  }
  getMonth(monthnumber) {
    if (monthnumber == '01') {
      this.month = "JAN";
    }
    else if (monthnumber == '02') {
      this.month = "FEB"
    }
    else if (monthnumber == '03') {
      this.month = "MAR"
    }
    else if (monthnumber == '04') {
      this.month = "APR"
    }
    else if (monthnumber == '05') {
      this.month = "MAY"
    }
    else if (monthnumber == '06') {
      this.month = "JUNE"
    }
    else if (monthnumber == '07') {
      this.month = "JULY"
    }
    else if (monthnumber == '08') {
      this.month = "AUG"
    }
    else if (monthnumber == '09') {
      this.month = "SEP"
    }
    else if (monthnumber == '10') {
      this.month = "OCT"
    }
    else if (monthnumber == '11') {
      this.month = "NOV"
    }
    else if (monthnumber == '12') {
      this.month = "DEC"
    }

  }
}
