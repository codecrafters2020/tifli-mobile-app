import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentMeetingdetailsPageRoutingModule } from './parent-meetingdetails-routing.module';

import { ParentMeetingdetailsPage } from './parent-meetingdetails.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentMeetingdetailsPageRoutingModule
  ],
  declarations: [ParentMeetingdetailsPage]
})
export class ParentMeetingdetailsPageModule {}
