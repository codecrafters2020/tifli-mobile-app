import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacultyAttendancePage } from './faculty-attendance.page';

describe('FacultyAttendancePage', () => {
  let component: FacultyAttendancePage;
  let fixture: ComponentFixture<FacultyAttendancePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacultyAttendancePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacultyAttendancePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
