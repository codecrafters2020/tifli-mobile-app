import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  // {
  //   path: '',
  //   redirectTo: 'folder/Inbox',
  //   pathMatch: 'full'
  // },
  // {
  //   path: 'folder/:id',
  //   loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  // },
  {
    path: 'album',
    loadChildren: () => import('./pages/album/album.module').then( m => m.AlbumPageModule)
  },
  {
    path: 'album-detail-faculty',
    loadChildren: () => import('./pages/album-detail-faculty/album-detail-faculty.module').then( m => m.AlbumDetailFacultyPageModule)
  },
  {
    path: 'album-detail-show',
    loadChildren: () => import('./pages/album-detail-show/album-detail-show.module').then( m => m.AlbumDetailShowPageModule)
  },
  {
    path: 'album-modal',
    loadChildren: () => import('./pages/album-modal/album-modal.module').then( m => m.AlbumModalPageModule)
  },
  {
    path: 'attendance-report',
    loadChildren: () => import('./pages/attendance-report/attendance-report.module').then( m => m.AttendanceReportPageModule)
  },
  {
    path: 'courses',
    loadChildren: () => import('./pages/courses/courses.module').then( m => m.CoursesPageModule)
  },
  {
    path: 'dinning-location',
    loadChildren: () => import('./pages/dinning-location/dinning-location.module').then( m => m.DinningLocationPageModule)
  },
  {
    path: 'document-image',
    loadChildren: () => import('./pages/document-image/document-image.module').then( m => m.DocumentImagePageModule)
  },
  {
    path: 'driver-home',
    loadChildren: () => import('./pages/driver-home/driver-home.module').then( m => m.DriverHomePageModule)
  },
  {
    path: 'event-details',
    loadChildren: () => import('./pages/event-details/event-details.module').then( m => m.EventDetailsPageModule)
  },
  {
    path: 'faculty-allprogress',
    loadChildren: () => import('./pages/faculty-allprogress/faculty-allprogress.module').then( m => m.FacultyAllprogressPageModule)
  },
  {
    path: 'faculty-newprogress',
    loadChildren: () => import('./pages/faculty-newprogress/faculty-newprogress.module').then( m => m.FacultyNewprogressPageModule)
  },
  {
    path: 'faculty-home',
    loadChildren: () => import('./pages/faculty-home/faculty-home.module').then( m => m.FacultyHomePageModule)
  },
  {
    path: 'faculty-individualdiary',
    loadChildren: () => import('./pages/faculty-individualdiary/faculty-individualdiary.module').then( m => m.FacultyIndividualdiaryPageModule)
  },
  {
    path: 'faculty-newdiary',
    loadChildren: () => import('./pages/faculty-newdiary/faculty-newdiary.module').then( m => m.FacultyNewdiaryPageModule)
  },
  {
    path: 'parent-trackingmap',
    loadChildren: () => import('./pages/parent-trackingmap/parent-trackingmap.module').then( m => m.ParentTrackingmapPageModule)
  },
  {
    path: 'parent-studentsummary',
    loadChildren: () => import('./pages/parent-studentsummary/parent-studentsummary.module').then( m => m.ParentStudentsummaryPageModule)
  },
  {
    path: 'faculty-newnotice',
    loadChildren: () => import('./pages/faculty-newnotice/faculty-newnotice.module').then( m => m.FacultyNewnoticePageModule)
  },
  {
    path: 'notifications',
    loadChildren: () => import('./pages/notifications/notifications.module').then( m => m.NotificationsPageModule)
  },
  {
    path: 'parent-album',
    loadChildren: () => import('./pages/parent-album/parent-album.module').then( m => m.ParentAlbumPageModule)
  },
  {
    path: 'parent-albumdetail',
    loadChildren: () => import('./pages/parent-albumdetail/parent-albumdetail.module').then( m => m.ParentAlbumdetailPageModule)
  },
  {
    path: 'parent-allresult',
    loadChildren: () => import('./pages/parent-allresult/parent-allresult.module').then( m => m.ParentAllresultPageModule)
  },
  {
    path: 'parent-allvideo',
    loadChildren: () => import('./pages/parent-allvideo/parent-allvideo.module').then( m => m.ParentAllvideoPageModule)
  },
  {
    path: 'events',
    loadChildren: () => import('./pages/events/events.module').then( m => m.EventsPageModule)
  },
  {
    path: 'parent-leave',
    loadChildren: () => import('./pages/parent-leave/parent-leave.module').then( m => m.ParentLeavePageModule)
  },
  {
    path: 'parent-newleave',
    loadChildren: () => import('./pages/parent-newleave/parent-newleave.module').then( m => m.ParentNewleavePageModule)
  },
  {
    path: 'parent-notice',
    loadChildren: () => import('./pages/parent-notice/parent-notice.module').then( m => m.ParentNoticePageModule)
  },
  {
    path: 'parent-noticedetail',
    loadChildren: () => import('./pages/parent-noticedetail/parent-noticedetail.module').then( m => m.ParentNoticedetailPageModule)
  },
  {
    path: 'parent-payment',
    loadChildren: () => import('./pages/parent-payment/parent-payment.module').then( m => m.ParentPaymentPageModule)
  },
  {
    path: 'parent-ptm',
    loadChildren: () => import('./pages/parent-ptm/parent-ptm.module').then( m => m.ParentPtmPageModule)
  },
  {
    path: 'parent-ptmchat',
    loadChildren: () => import('./pages/parent-ptmchat/parent-ptmchat.module').then( m => m.ParentPtmchatPageModule)
  },
  {
    path: 'parent-resource',
    loadChildren: () => import('./pages/parent-resource/parent-resource.module').then( m => m.ParentResourcePageModule)
  },
  {
    path: 'parent-survey',
    loadChildren: () => import('./pages/parent-survey/parent-survey.module').then( m => m.ParentSurveyPageModule)
  },
  {
    path: 'parent-surveydetail',
    loadChildren: () => import('./pages/parent-surveydetail/parent-surveydetail.module').then( m => m.ParentSurveydetailPageModule)
  },
  {
    path: 'parent-diary',
    loadChildren: () => import('./pages/parent-diary/parent-diary.module').then( m => m.ParentDiaryPageModule)
  },
  {
    path: 'parent-diarydetail',
    loadChildren: () => import('./pages/parent-diarydetail/parent-diarydetail.module').then( m => m.ParentDiarydetailPageModule)
  },
  {
    path: 'parent-home',
    loadChildren: () => import('./pages/parent-home/parent-home.module').then( m => m.ParentHomePageModule)
  },
  {
    path: 'faculty-attendance',
    loadChildren: () => import('./pages/faculty-attendance/faculty-attendance.module').then( m => m.FacultyAttendancePageModule)
  },
  {
    path: 'faculty-previousdiary',
    loadChildren: () => import('./pages/faculty-previousdiary/faculty-previousdiary.module').then( m => m.FacultyPreviousdiaryPageModule)
  },
  {
    path: 'parent-meetingdetails',
    loadChildren: () => import('./pages/parent-meetingdetails/parent-meetingdetails.module').then( m => m.ParentMeetingdetailsPageModule)
  },
  {
    path: 'faculty-ptm',
    loadChildren: () => import('./pages/faculty-ptm/faculty-ptm.module').then( m => m.FacultyPtmPageModule)
  },
  {
    path: 'faculty-ptmdetails',
    loadChildren: () => import('./pages/faculty-ptmdetails/faculty-ptmdetails.module').then( m => m.FacultyPtmdetailsPageModule)
  },
  {
    path: 'faculty-ptmchat',
    loadChildren: () => import('./pages/faculty-ptmchat/faculty-ptmchat.module').then( m => m.FacultyPtmchatPageModule)
  },
  {
    path: 'faculty-newptm',
    loadChildren: () => import('./pages/faculty-newptm/faculty-newptm.module').then( m => m.FacultyNewptmPageModule)
  },
  {
    path: 'parent-newptm',
    loadChildren: () => import('./pages/parent-newptm/parent-newptm.module').then( m => m.ParentNewptmPageModule)
  },
  {
    path: 'sign-in',
    loadChildren: () => import('./pages/sign-in/sign-in.module').then( m => m.SignInPageModule)
  },
  {
    path: 'sign-up',
    loadChildren: () => import('./pages/sign-up/sign-up.module').then( m => m.SignUpPageModule)
  },
  {
    path: 'splash',
    loadChildren: () => import('./pages/splash/splash.module').then( m => m.SplashPageModule)
  },
  {
    path: 'faculty-allresources',
    loadChildren: () => import('./pages/faculty-allresources/faculty-allresources.module').then( m => m.FacultyAllresourcesPageModule)
  },
  {
    path: 'faculty-allresult',
    loadChildren: () => import('./pages/faculty-allresult/faculty-allresult.module').then( m => m.FacultyAllresultPageModule)
  },
  {
    path: 'faculty-allresultdetail',
    loadChildren: () => import('./pages/faculty-allresultdetail/faculty-allresultdetail.module').then( m => m.FacultyAllresultdetailPageModule)
  },
  {
    path: 'faculty-allvideo',
    loadChildren: () => import('./pages/faculty-allvideo/faculty-allvideo.module').then( m => m.FacultyAllvideoPageModule)
  },
  {
    path: 'faculty-leave',
    loadChildren: () => import('./pages/faculty-leave/faculty-leave.module').then( m => m.FacultyLeavePageModule)
  },
  {
    path: 'faculty-newresource',
    loadChildren: () => import('./pages/faculty-newresource/faculty-newresource.module').then( m => m.FacultyNewresourcePageModule)
  },
  {
    path: 'faculty-newresult',
    loadChildren: () => import('./pages/faculty-newresult/faculty-newresult.module').then( m => m.FacultyNewresultPageModule)
  },
  {
    path: 'faculty-resultuploadmarks',
    loadChildren: () => import('./pages/faculty-resultuploadmarks/faculty-resultuploadmarks.module').then( m => m.FacultyResultuploadmarksPageModule)
  },
  {
    path: 'faculty-videoupload',
    loadChildren: () => import('./pages/faculty-videoupload/faculty-videoupload.module').then( m => m.FacultyVideouploadPageModule)
  },
  {
    path: 'parent-map',
    loadChildren: () => import('./pages/parent-map/parent-map.module').then( m => m.ParentMapPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'non-login',
    loadChildren: () => import('./pages/non-login/non-login.module').then( m => m.NonLoginPageModule)
  },
  {
    path: 'faculty-timetable',
    loadChildren: () => import('./pages/faculty-timetable/faculty-timetable.module').then( m => m.FacultyTimetablePageModule)
  },
  {
    path: 'parent-timetable',
    loadChildren: () => import('./pages/parent-timetable/parent-timetable.module').then( m => m.ParentTimetablePageModule)
  },
  {
    path: 'reset-password',
    loadChildren: () => import('./pages/reset-password/reset-password.module').then( m => m.ResetPasswordPageModule)
  },
  {
    path: 'attachedimageshow',
    loadChildren: () => import('./pages/attachedimageshow/attachedimageshow.module').then( m => m.AttachedimageshowPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
