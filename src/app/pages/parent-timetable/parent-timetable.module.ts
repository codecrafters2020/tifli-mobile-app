import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentTimetablePageRoutingModule } from './parent-timetable-routing.module';

import { ParentTimetablePage } from './parent-timetable.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentTimetablePageRoutingModule
  ],
  declarations: [ParentTimetablePage]
})
export class ParentTimetablePageModule {}
