import { Component, OnInit } from '@angular/core';
import { NavController, ModalController, LoadingController, ToastController } from '@ionic/angular';
import { Api } from '../../providers/api/api.service';
import { GlobalVars } from '../../providers/global-vars.service';
import * as moment from 'moment';
import { ParentParamsService } from '../../providers/parent-params.service';
@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.page.html',
  styleUrls: ['./reset-password.page.scss'],
})
export class ResetPasswordPage implements OnInit {

  password;
  confirm_password;
  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public api: Api,
    public globalVar: GlobalVars,
    private loadingController: LoadingController,
    public parentparam: ParentParamsService,
    public toastCtrl: ToastController,

  ) { }

  ngOnInit() {
  }
  async change_password()
  {
    if(this.password==this.confirm_password)
    {
    this.api.patch("sms_users/change_password.json",{password:this.password,
      confirm_password:this.confirm_password}).subscribe(data=>{
        console.log(data);
        if(this.globalVar.current_user.role=="faculty")
        {
          this.navCtrl.navigateRoot("faculty-home");
        }
        else{
          this.navCtrl.navigateRoot("parent-home");
        }
    });
    }
    else{
      let toast = await this.toastCtrl.create({
        message: "Password Misatch.",
        duration: 4000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-success",
        color:"danger"
      });
      toast.present();
    }
  }
  disabled()
  {
      if(this.confirm_password)
      {
        return false
      }
      else{
        return true
      }

  }
}
