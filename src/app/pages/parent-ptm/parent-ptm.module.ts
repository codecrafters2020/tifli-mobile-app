import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentPtmPageRoutingModule } from './parent-ptm-routing.module';

import { ParentPtmPage } from './parent-ptm.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentPtmPageRoutingModule
  ],
  declarations: [ParentPtmPage]
})
export class ParentPtmPageModule {}
