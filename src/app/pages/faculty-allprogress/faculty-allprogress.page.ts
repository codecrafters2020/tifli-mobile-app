import { Component,OnInit } from '@angular/core';
import { NavController, ToastController, LoadingController } from '@ionic/angular';
import { Api } from '../../providers/api/api.service';
import { GlobalVars } from '../../providers/global-vars.service';
import * as moment from 'moment';

@Component({
  selector: 'app-faculty-allprogress',
  templateUrl: './faculty-allprogress.page.html',
  styleUrls: ['./faculty-allprogress.page.scss'],
})
export class FacultyAllprogressPage implements OnInit{

  students: any = [];
  categories: any;
  student_progress: any;
  student_rating: any;
  array1;
  filtermonthwise;
  progressispresent = false;
  studentmonthprogress: any;
  edit = false;
  studentratingedit: any;
  student_progress_edit: any;
  constructor(
    public navCtrl: NavController,
    public api: Api,
    public globalVar: GlobalVars,
    public toastCtrl: ToastController,
    private loadingController: LoadingController,

  ) {
    this.categories = [];
    this.student_progress = [];
    this.student_rating = [];
    this.array1 = [];
    this.studentratingedit = [];
    this.studentmonthprogress = [];
    this.student_progress_edit = [];
  }

  async ngOnInit() {
    const loading = await this.loadingController.create({
      // content:"loading data..."
    });
    await loading.present();

    console.log('ionViewDidLoad FacultyAllprogressPage');
    this.students = this.globalVar.current_user.section_students;
    var studentlength = this.students.length;
    var sL = 0;
    var date = new Date();
    var d = moment(date);
    this.api.get("faculty/progress/get_progress_by_month.json",
      { month: d.month() + 1, faculty_id: this.globalVar.current_user.information.id }).subscribe(data => {
        console.log(data);
          loading.dismiss();
        if (!data["message"]) {
          console.log(data["student_progresses"])
          this.studentmonthprogress = data["student_progresses"];
          this.progressispresent = true;
          var s = 0;
          var c = this.studentmonthprogress[0].progress_ratings.length;
          for (var i = 0; i < this.students.length;) {
            var student_rating = {
              rating: this.studentmonthprogress[i].progress_ratings[s].rating,
              progress_rating_id: this.studentmonthprogress[i].progress_ratings[s].id
            };
            this.studentratingedit.push(student_rating);
            s++;
            if (s == c) {
              var studentrating = { student_rating: this.studentratingedit }
              this.student_progress_edit.push(studentrating);
              i++;
              s = 0;
              this.studentratingedit = [];

            }
          }

        }
        else {
          console.log(data["message"]);
          this.edit = false;
          this.progressispresent = false;
        }
      });
    if (!this.progressispresent) {
      this.api.get("faculty/progress/get_progress_category.json").subscribe(data => {
        console.log(data);
        this.categories = data;
        var s = 0;
        var c = this.categories.length;
        for (var i = 0; i < this.students.length;) {
          this.array1 = [];
          var student_rating = { rating: 0, progress_category_id: this.categories[s].id }
          this.student_rating.push(student_rating);
          s++;
          sL++;
          if (s == c) {
            var student_progress = { student_id: this.students[i].id, student_rating: this.student_rating };
            //  this.array1.push(student_progress);
            s = 0;
            i++;
            this.student_progress.push(student_progress);
            this.student_rating = [];
          }
        }
        loading.dismiss();

      })

    }
  }
  disabled() {
    if (!this.progressispresent) {
      return true;
    }
    else {
      return false;
    }
  }
  loadResource(filtermonthwise) {
    this.student_progress_edit = []
    this.studentratingedit = [];
    this.api.get("faculty/progress/get_progress_by_month.json",
      { faculty_id: this.globalVar.current_user.information.id, month: filtermonthwise }).subscribe(async data => {
        if (!data["message"]) {
          console.log(data["student_progresses"])
          this.studentmonthprogress = data["student_progresses"];
          this.progressispresent = true;
          var s = 0;
          var c = this.studentmonthprogress[0].progress_ratings.length;
          for (var i = 0; i < this.students.length;) {
            var student_rating = {
              rating: this.studentmonthprogress[i].progress_ratings[s].rating,
              progress_rating_id: this.studentmonthprogress[i].progress_ratings[s].id
            };
            this.studentratingedit.push(student_rating);
            s++;
            if (s == c) {
              var studentrating = { student_rating: this.studentratingedit }
              this.student_progress_edit.push(studentrating);
              i++;
              s = 0;
              this.studentratingedit = [];

            }
          }
        }
        else {
          console.log(data["message"]);
          this.progressispresent = false;
          this.edit = false;
          let toast = await this.toastCtrl.create({
            message: "Progres for month not found.",
            duration: 4000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-danger",
            color:"danger"
          });
          toast.present();
        }
      })
  }

  async postdata() {
    var thisMonth
    if (this.filtermonthwise) {
      thisMonth = this.filtermonthwise;
    }
    else {
      thisMonth = (new Date()).getMonth() + 1;
    }

    const loading = await this.loadingController.create({
      // content:"loading data..."
    });
    await loading.present();

    var progress = { month: thisMonth, faculty_id: this.globalVar.current_user.information.id }
    this.api.post("faculty/progress.json", { progress: progress, student_progress: this.student_progress }).subscribe(async data => {
      console.log(data);
      loading.dismiss();
      this.navCtrl.navigateRoot('faculty-home');
      let toast = await this.toastCtrl.create({
        message: "Progress Uploaded successfully.",
        duration: 4000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-success",
        color:"success"
      });
      toast.present();
    }, async err => {
      let toast = await this.toastCtrl.create({
        message: "Something Went Wrong. Please try later.",
        duration: 4000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger",
        color:"danger"
      });
      toast.present();

    })
  }
  editData() {
    if (this.edit) {
      this.edit = false;
    }
    else {
      this.edit = true;
    }
  }
  async  progress_update() {
    const loading = await this.loadingController.create({
      // content:"loading data..."
    });
    await loading.present();
    this.api.patch("faculty/progress.json", { student_progress_edit: this.student_progress_edit }).subscribe(
      async data => {
        console.log(data);
        loading.dismiss();
        this.navCtrl.navigateRoot('faculty-home');
        let toast = await this.toastCtrl.create({
          message: "Progress Uploaded successfully.",
          duration: 4000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-success",
          color:"success"
        });
        toast.present();
      }, async err => {
        loading.dismiss();
        let toast = await this.toastCtrl.create({
          message: "Something Went Wrong",
          duration: 4000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger",
          color:"danger"
        });
        toast.present();
      })
  }
}
