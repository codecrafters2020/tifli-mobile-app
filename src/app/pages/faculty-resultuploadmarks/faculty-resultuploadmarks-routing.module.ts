import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacultyResultuploadmarksPage } from './faculty-resultuploadmarks.page';

const routes: Routes = [
  {
    path: '',
    component: FacultyResultuploadmarksPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacultyResultuploadmarksPageRoutingModule {}
