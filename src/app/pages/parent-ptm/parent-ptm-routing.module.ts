import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParentPtmPage } from './parent-ptm.page';

const routes: Routes = [
  {
    path: '',
    component: ParentPtmPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParentPtmPageRoutingModule {}
