import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacultyAllvideoPageRoutingModule } from './faculty-allvideo-routing.module';

import { FacultyAllvideoPage } from './faculty-allvideo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacultyAllvideoPageRoutingModule
  ],
  declarations: [FacultyAllvideoPage]
})
export class FacultyAllvideoPageModule {}
