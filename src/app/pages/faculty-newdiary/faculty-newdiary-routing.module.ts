import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacultyNewdiaryPage } from './faculty-newdiary.page';

const routes: Routes = [
  {
    path: '',
    component: FacultyNewdiaryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacultyNewdiaryPageRoutingModule {}
