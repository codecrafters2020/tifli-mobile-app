import { Component,OnInit } from '@angular/core';
import { NavController, NavParams } from '@ionic/angular';
import { GlobalVars } from '../../providers/global-vars.service';
import { Api } from '../../providers/api/api.service';


@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage implements OnInit{

  notification_item1='assets/img/1stnotif.png';
  notification_item2='assets/img/2ndnotif.png';
  notification_item3='assets/img/3rdnotif.png';
  notification_item4='assets/img/4thnotif.png';
  notifications:any=[];
  page=0;
  constructor(
    public navCtrl: NavController,
    public globalVar: GlobalVars,
    public api: Api,) {
//      this.page=0;
  }
  ngOnInit() {
    console.log('ionViewDidLoad NotificationsPage');
    this.getNotification();
  }
  getNotification(infiniteScroll=null)
  {
    this.api.get('sms_users/'+this.globalVar.current_user.information.sms_user_id+'/notifications.json?page='+this.page).subscribe(data=>{
      console.log(data);
      if (infiniteScroll) {
        for(let i=0; i < Object.keys(data).length; i++) {
           this.notifications.push(data[i]);
         }
       }else{
        this.notifications=data;
       }
       this.page=this.page+1;
    },(err) => {
      this.infiniteScrollComplete(infiniteScroll)
    })
  }
  removeItem(item){
  let index = this.notifications.indexOf(item);
  if(index > -1){
    this.notifications.splice(index, 1);
      }
  }
  infiniteScrollComplete(infiniteScroll){
    if (infiniteScroll) {
      infiniteScroll.complete();
    }
  }

  doInfinite(infiniteScroll) {
    this.getNotification(infiniteScroll);
  }

}
