import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacultyAllresultPageRoutingModule } from './faculty-allresult-routing.module';

import { FacultyAllresultPage } from './faculty-allresult.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacultyAllresultPageRoutingModule
  ],
  declarations: [FacultyAllresultPage]
})
export class FacultyAllresultPageModule {}
