import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ParentParamsService {

  album_images;
  ptm_meetingdetails;
  ptm_discussionid;
  ptm_discussionsubject;
  diarydetail;
  doc_img;
  diary_detaildoc;
  noticedetail;
  survey;
  notify_notice_parent;
  notify_ptm_parent_student_id;
  notify_ptm_parent_meeting_id;
  notify_leave_parent;
  notify_parent_ptm_chat;
  notify_faculty_ptm_chat;
  notify_attendance_studentid;
  constructor() {
    this.album_images = [];
    this.ptm_meetingdetails = [];
    // this.ptm_discussionid="";
    this.ptm_discussionsubject="";
    this.diarydetail=[];
    this.doc_img = "";
    this.diary_detaildoc=[];
    this.noticedetail=[];
    this.survey = [];
    this.notify_notice_parent="";
    this.notify_ptm_parent_student_id="";
    this.notify_ptm_parent_meeting_id="";
    this.notify_leave_parent="";
    this.notify_parent_ptm_chat="";
    this.notify_faculty_ptm_chat="";
    this.notify_attendance_studentid="";  
   }
}
