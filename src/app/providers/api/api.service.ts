import { HttpClient, HttpParams,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
// import 'rxjs/add/operator/toPromise';

import { GlobalVars } from '../global-vars.service';
// import { Storage } from '@ionic/storage';
/**
 * Api is a generic REST Api handler. Set your API url first.
 */
@Injectable()
export class Api {

  url: string = '';


  public headers : any

  constructor(public http: HttpClient,public globalVar: GlobalVars) {
    this.url = this.globalVar.server_link + "/api/v1";
  }

  get(endpoint: string, params?: any, reqOpts?: any) {
    this.setHeader()
    if (!reqOpts) {
      reqOpts = {
        headers: this.headers,
        params: new HttpParams()

      };
    }else{
      reqOpts = {
        headers: this.headers
      };
    }

    // Support easy query params for GET requests
    if (params) {
      reqOpts.params = new HttpParams();
      for (let k in params) {
        reqOpts.params = reqOpts.params.set(k, params[k]);
      }
    }
    console.log("this.globalVar.current_user",this.globalVar.current_user.auth_token)
    return this.http.get(this.url + '/' + endpoint, reqOpts);
  }

  post(endpoint: string, body: any, reqOpts?: any) {
    this.setHeader();
    // console.log("dsaadsf",this.headers)
    return this.http.post(this.url + '/' + endpoint, body, {headers: this.headers});
  }

  put(endpoint: string, body: any, reqOpts?: any) {
    this.setHeader()
    return this.http.put(this.url + '/' + endpoint, body, {headers: this.headers});
  }

  delete(endpoint: string, reqOpts?: any) {
    this.setHeader()
    return this.http.delete(this.url + '/' + endpoint, {headers: this.headers});
  }

  patch(endpoint: string, body: any, reqOpts?: any) {
    this.setHeader()
    return this.http.patch(this.url + '/' + endpoint, body, {headers: this.headers});
  }

  setHeader(){
    this.headers = new HttpHeaders({
       Authorization: this.globalVar.current_user.auth_token|| '',
       "Access-Control-Allow-Origin": "*"
    })
  }
}
