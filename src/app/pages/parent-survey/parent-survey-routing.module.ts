import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParentSurveyPage } from './parent-survey.page';

const routes: Routes = [
  {
    path: '',
    component: ParentSurveyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParentSurveyPageRoutingModule {}
