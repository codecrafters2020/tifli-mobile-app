import { Component,OnInit } from '@angular/core';
import {  NavController } from '@ionic/angular';
import { GlobalVars } from '../../providers/global-vars.service';
import { Api } from '../../providers/api/api.service';
import { ParentParamsService } from 'src/app/providers/parent-params.service';

@Component({
  selector: 'app-attendance-report',
  templateUrl: './attendance-report.page.html',
  styleUrls: ['./attendance-report.page.scss'],
})
export class AttendanceReportPage implements OnInit{

  thisMonth:any;
  filtermonthwise:any;
  studentDetails:any;
  studentid: any;
  absentbox='assets/img/absentbox.png';
  presentbox='assets/img/presentbox.png';
  leavebox='assets/img/leavebox.png';
  totalbox='assets/img/totalbox.png';
  totalpresent: any;
  totalabsent:any;
  totalleave:any;
  total:any
  constructor(
    public navCtrl: NavController,
    public globalVar: GlobalVars,
    public api: Api,
    public parentParam: ParentParamsService
    ) {
    this.studentDetails=[];
    this.thisMonth = (new Date()).getMonth()+1;
    this.filtermonthwise = this.thisMonth;
    if(this.parentParam.notify_attendance_studentid)
    {
      this.studentid=this.parentParam.notify_attendance_studentid;
    }
    else
    {
      this.studentid = this.globalVar.student.id;
    }
  }

  ngOnInit() {
    console.log('ionViewDidLoad AttendanceReportPage');
    this.loadAttendanceReport(this.filtermonthwise);
  }

  loadAttendanceReport(filtermonthwise){
    this.totalabsent=0,this.totalpresent=0,this.total=0,this.totalleave=0;
    this.studentDetails=[];
    var date = new Date();
    var y = date.getFullYear();
    var m = filtermonthwise-1;
    var m2 =filtermonthwise ;
    var firstday = new Date(y,m,1);
    var lastday = new Date(y,m2,0);
    console.log(firstday);
    console.log(lastday);
    this.api.get('parent/student/get_detailed_student_attendance.json', {
      student_id: this.studentid,from_date:firstday,to_date:lastday}).subscribe((res: any) => {
      console.log(res);
       this.studentDetails = res["student_attendance_details"];
      for(var i=0;i<this.studentDetails.length;i++)
      {
          if(this.studentDetails[i].attendance=='present')
          {
            this.totalpresent=this.totalpresent+1;
          }
          else if(this.studentDetails[i].attendance=='absent')
         {
            this.totalabsent=this.totalabsent+1;
         }
         else if(this.studentDetails[i].attendance=='leave')
         {
            this.totalleave=this.totalleave+1;
         }
      }
      this.total=this.totalabsent+this.totalleave+this.totalpresent;

    }, err => {
      console.error('ERROR', err);
    });
  }
  get sortData() {
    return this.studentDetails.sort((a, b) => {
      return <any>new Date(b.created_at) - <any>new Date(a.created_at);
    });
  }
}
