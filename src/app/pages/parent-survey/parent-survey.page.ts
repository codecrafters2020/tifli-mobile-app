import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, ToastController } from '@ionic/angular';
import { Api } from '../../providers/api/api.service';
import { GlobalVars } from '../../providers/global-vars.service';
import { ParentParamsService } from '../../providers/parent-params.service';
@Component({
  selector: 'app-parent-survey',
  templateUrl: './parent-survey.page.html',
  styleUrls: ['./parent-survey.page.scss'],
})
export class ParentSurveyPage implements OnInit {

  studentname
  surveyquestions: any;
  surveypostanswer;
  surveypostanswersend: any;
  surveyquestionsshow: any;
  yellowdate ='assets/img/notice/yellowdate.png';
  bluedate ='assets/img/notice/bluedate.png';
  pinkdate ='assets/img/notice/pinkdate.png';

  constructor(
    public navCtrl: NavController,
    public globalVar: GlobalVars,
    public api: Api,
    private loadingController: LoadingController,
    public toastCtrl: ToastController,
    public parentParam: ParentParamsService
  ) { 
    this.studentname=this.globalVar.student;
    this.surveyquestions=[];
    this.surveyquestionsshow=[];
    this.surveypostanswer=[];
    this.surveypostanswersend=[];

  }

  async ngOnInit() {
    console.log('ionViewDidLoad ParentSurveryPage');
    const loading = await this.loadingController.create({
    });
    await loading.present();

    console.log('ionViewDidLoad ParentSurverydetailPage');
    this.api.get("parent/survey/get_survey_by_student.json",{
      student_id:this.globalVar.student.id}).subscribe(data=>{
        console.log(data);
        this.surveyquestions=data["surveys"];
        loading.dismiss();
      },async err=>{
        loading.dismiss();
        this.navCtrl.navigateRoot("parent-home");
        let toast = await this.toastCtrl.create({
          message: "Error Loading Survey.",
          duration: 4000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger",
          color:"danger"
        });
        await toast.present();

      });

  }
  detailsurvey(index)
  {
    this.parentParam.survey = this.surveyquestions[index];
   this.navCtrl.navigateRoot('parent-surveydetail');
  }
  open(page)
  {
      this.navCtrl.navigateRoot(page);
  }
}
