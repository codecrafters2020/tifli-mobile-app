import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacultyNewresourcePage } from './faculty-newresource.page';

describe('FacultyNewresourcePage', () => {
  let component: FacultyNewresourcePage;
  let fixture: ComponentFixture<FacultyNewresourcePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacultyNewresourcePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacultyNewresourcePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
