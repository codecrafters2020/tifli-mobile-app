import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
// import { MyApp } from './app.component';
import { IonicStorageModule, Storage } from '@ionic/storage';
import { GlobalVars } from './providers/global-vars.service';
import { Api } from './providers/api/api.service';
import { HttpClientModule } from '@angular/common/http';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { Push, PushObject, PushOptions } from '@ionic-native/push/ngx';
import { Dialogs } from '@ionic-native/dialogs/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { AwsProvider } from './providers/aws.service';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { NetworkProvider } from './providers/network.service';
import { Network } from '@ionic-native/network/ngx';
import { DocumentViewer } from '@ionic-native/document-viewer/ngx';
import { FacultyNewprogressPageModule } from './pages/faculty-newprogress/faculty-newprogress.module';
import { SplashPageModule } from './pages/splash/splash.module';
import { StreamingMedia } from '@ionic-native/streaming-media/ngx';
import { Zoom } from '@ionic-native/zoom/ngx';
import { FacultyParamsService }  from './providers/faculty-params.service';
import { ParentParamsService } from './providers/parent-params.service';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { File } from '@ionic-native/file/ngx';
@NgModule({
  declarations: [
    AppComponent,
    // SplashPage,
    // FacultyNewprogressPage
    ],
    entryComponents: [
      // SplashPage,
      // FacultyNewprogressPage
    ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    SplashPageModule,
    FacultyNewprogressPageModule,
  ],
  providers: [
     Diagnostic,
   LocationAccuracy,
    StatusBar,
    SplashScreen,
    Camera,
     StreamingMedia,
    Zoom,
    File,
     FileTransfer,
     FileChooser,
     FilePath,
    ImagePicker,
     CallNumber,
    AwsProvider,
    NetworkProvider,
     DocumentViewer,
     Network,
     FacultyParamsService,
     ParentParamsService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    Push,
    Dialogs,
    GlobalVars,
    Api,
    Geolocation,
    BackgroundMode,
    ],
  bootstrap: [AppComponent]
})
export class AppModule {}



