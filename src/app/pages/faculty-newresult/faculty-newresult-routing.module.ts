import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacultyNewresultPage } from './faculty-newresult.page';

const routes: Routes = [
  {
    path: '',
    component: FacultyNewresultPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacultyNewresultPageRoutingModule {}
