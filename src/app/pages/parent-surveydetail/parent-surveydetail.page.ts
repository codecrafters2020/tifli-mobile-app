import { Component,OnInit } from '@angular/core';
import { NavController, LoadingController, ToastController } from '@ionic/angular';
import { GlobalVars } from '../../providers/global-vars.service';
import { Api } from '../../providers/api/api.service';
import { ParentParamsService } from '../../providers/parent-params.service';

@Component({
  selector: 'app-parent-surveydetail',
  templateUrl: './parent-surveydetail.page.html',
  styleUrls: ['./parent-surveydetail.page.scss'],
})
export class ParentSurveydetailPage implements OnInit{
  student: any;
  fname: any;
  surveyquestions:any;
  surveyquestionsshow:any;
  surveypostanswer:any;
  surveypostanswersend:any


  constructor(
    public navCtrl: NavController,
    public globalVar: GlobalVars,
    public api: Api,
    private loadingController: LoadingController,
    public toastCtrl: ToastController,
    public parentParam: ParentParamsService

  ) {
    this.surveyquestions=[];
    this.surveyquestionsshow=[];
    this.surveypostanswer=[];
    this.surveypostanswersend=[];

  }

  async ngOnInit() {
   
    console.log('ionViewDidLoad ParentSurverydetailPage');
        this.surveyquestions=this.parentParam.survey;
          for(var i=0;i<this.surveyquestions.survey_questions.length;i++)
          {
            this.surveypostanswer={answer:3,survey_question_id:this.surveyquestions.survey_questions[i].id,
            parent_id:this.globalVar.current_user.information.id,student_id:this.globalVar.student.id};
            this.surveypostanswersend.push(this.surveypostanswer);
          }
          this.surveyquestionsshow = this.surveyquestions.survey_questions;

  }
  setanswer(index,answer)
  {
      this.surveypostanswersend[index].answer=answer
      console.log(this.surveypostanswersend);
      if(answer==1)
      {
        document.getElementById("col1-"+[index]).style.borderBottom="3px solid #000";
        document.getElementById("col2-"+[index]).style.borderBottom="none";
        document.getElementById("col3-"+[index]).style.borderBottom="none";
        document.getElementById("col4-"+[index]).style.borderBottom="none";
        document.getElementById("col5-"+[index]).style.borderBottom="none";
      }
      else if(answer==2)
      {
        document.getElementById("col1-"+[index]).style.borderBottom="none";
        document.getElementById("col2-"+[index]).style.borderBottom="3px solid #e9427b";
        document.getElementById("col3-"+[index]).style.borderBottom="none";
        document.getElementById("col4-"+[index]).style.borderBottom="none";
        document.getElementById("col5-"+[index]).style.borderBottom="none";
      }
      else if(answer==3)
      {
        document.getElementById("col1-"+[index]).style.borderBottom="none";
        document.getElementById("col2-"+[index]).style.borderBottom="none";
        document.getElementById("col3-"+[index]).style.borderBottom="3px solid #f9a825";
        document.getElementById("col4-"+[index]).style.borderBottom="none";
        document.getElementById("col5-"+[index]).style.borderBottom="none";
      }
      else if(answer==4)
      {
        document.getElementById("col1-"+[index]).style.borderBottom="none";
        document.getElementById("col2-"+[index]).style.borderBottom="none";
        document.getElementById("col3-"+[index]).style.borderBottom="none";
        document.getElementById("col4-"+[index]).style.borderBottom="3px solid #488aff";
        document.getElementById("col5-"+[index]).style.borderBottom="none";
      }
      else if(answer==5)
      {
        document.getElementById("col1-"+[index]).style.borderBottom="none";
        document.getElementById("col2-"+[index]).style.borderBottom="none";
        document.getElementById("col3-"+[index]).style.borderBottom="none";
        document.getElementById("col4-"+[index]).style.borderBottom="none";
        document.getElementById("col5-"+[index]).style.borderBottom="3px solid #5aea01";
      }

  }
  async postAnswer()
  {
    const loading = await this.loadingController.create({
    });
    await loading.present();

    this.api.post("parent/survey/create_survey_answers.json",
    {survey_answer:this.surveypostanswersend}).subscribe(async data=>{
      console.log(data);
      this.navCtrl.navigateRoot('parent-home');
      let toast = await this.toastCtrl.create({
        message: "Thanks for submitting feedback.",
        duration: 4000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-success",
        color:"success"
      });
      await toast.present();
      loading.dismiss();
    },async err=>{
      loading.dismiss();
      let toast = await this.toastCtrl.create({
        message: "Something Went Wrong.",
        duration: 4000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger",
        color:"danger"
      });
      await toast.present();

    })
  }
  previouspage()
  {
    this.navCtrl.navigateRoot('parent-survey');
  }
}
