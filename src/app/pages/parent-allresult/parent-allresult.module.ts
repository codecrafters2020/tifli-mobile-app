import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentAllresultPageRoutingModule } from './parent-allresult-routing.module';

import { ParentAllresultPage } from './parent-allresult.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentAllresultPageRoutingModule
  ],
  declarations: [ParentAllresultPage]
})
export class ParentAllresultPageModule {}
