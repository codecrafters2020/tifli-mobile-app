import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AlbumDetailShowPage } from './album-detail-show.page';

const routes: Routes = [
  {
    path: '',
    component: AlbumDetailShowPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AlbumDetailShowPageRoutingModule {}
