import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParentNewptmPage } from './parent-newptm.page';

const routes: Routes = [
  {
    path: '',
    component: ParentNewptmPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParentNewptmPageRoutingModule {}
