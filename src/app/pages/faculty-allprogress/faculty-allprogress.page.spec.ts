import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacultyAllprogressPage } from './faculty-allprogress.page';

describe('FacultyAllprogressPage', () => {
  let component: FacultyAllprogressPage;
  let fixture: ComponentFixture<FacultyAllprogressPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacultyAllprogressPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacultyAllprogressPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
