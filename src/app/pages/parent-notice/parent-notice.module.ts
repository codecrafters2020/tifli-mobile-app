import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentNoticePageRoutingModule } from './parent-notice-routing.module';

import { ParentNoticePage } from './parent-notice.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentNoticePageRoutingModule
  ],
  declarations: [ParentNoticePage]
})
export class ParentNoticePageModule {}
