import { Component,OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Api } from '../../providers/api/api.service';
import { FacultyParamsService } from '../../providers/faculty-params.service';
@Component({
  selector: 'app-events',
  templateUrl: './events.page.html',
  styleUrls: ['./events.page.scss'],
})
export class EventsPage implements OnInit {

  events:any;
  eventsobject: any;
  eventShow: any;
  item='assets/img/event.png';
  constructor(
    public navCtrl: NavController,
    public api: Api,
    public facultyparams: FacultyParamsService
    ) {
      this.events=[];
      this.eventsobject=[];
      this.eventShow=[];
  }

  ngOnInit() {
    console.log('ionViewDidLoad ParentCalendersPage');
    this.api.get("events/:event_id/get_all_events").subscribe(data=>{
      console.log(data);
      this.events=data;
      for(var i=0;i<this.events.length;i++)
      {
        this.eventsobject={image:this.events[i].image_url,description:this.events[i].description,
        createdat:this.events[i].created_at,eventdate:this.events[i].event_date,
        eventname:this.events[i].event_name,location:this.events[i].location};
        this.eventShow.push(this.eventsobject);
      }
    })
  }
  open(page)
  {
      this.navCtrl.navigateRoot(page);
  }
  eventFull(i)
  {
    this.facultyparams.event_detail = this.eventShow[i];
      this.navCtrl.navigateRoot('event-details');
  }
}
