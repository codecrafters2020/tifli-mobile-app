import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacultyPreviousdiaryPage } from './faculty-previousdiary.page';

const routes: Routes = [
  {
    path: '',
    component: FacultyPreviousdiaryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacultyPreviousdiaryPageRoutingModule {}
