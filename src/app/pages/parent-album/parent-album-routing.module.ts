import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParentAlbumPage } from './parent-album.page';

const routes: Routes = [
  {
    path: '',
    component: ParentAlbumPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParentAlbumPageRoutingModule {}
