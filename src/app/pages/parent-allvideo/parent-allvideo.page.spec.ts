import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ParentAllvideoPage } from './parent-allvideo.page';

describe('ParentAllvideoPage', () => {
  let component: ParentAllvideoPage;
  let fixture: ComponentFixture<ParentAllvideoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentAllvideoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ParentAllvideoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
