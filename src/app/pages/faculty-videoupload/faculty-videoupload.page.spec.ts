import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacultyVideouploadPage } from './faculty-videoupload.page';

describe('FacultyVideouploadPage', () => {
  let component: FacultyVideouploadPage;
  let fixture: ComponentFixture<FacultyVideouploadPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacultyVideouploadPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacultyVideouploadPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
