import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AlbumDetailShowPageRoutingModule } from './album-detail-show-routing.module';

import { AlbumDetailShowPage } from './album-detail-show.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AlbumDetailShowPageRoutingModule
  ],
  declarations: [AlbumDetailShowPage]
})
export class AlbumDetailShowPageModule {}
