import { Component,OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { NavController,Platform } from '@ionic/angular';
import { Api } from '../../providers/api/api.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { GlobalVars } from '../../providers/global-vars.service';
declare var google;
var directionsService = new google.maps.DirectionsService();
var directionsRenderer = new google.maps.DirectionsRenderer();

@Component({
  selector: 'app-parent-trackingmap',
  templateUrl: './parent-trackingmap.page.html',
  styleUrls: ['./parent-trackingmap.page.scss'],
})
export class ParentTrackingmapPage implements OnInit{

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  student: any;
  van_lat: any;
  van_lng: any;
  lat: any;
  lng: any;
  today: any;
  status: any;
  van_color: any;
  driver_mobile_number: any;
  van_registration_number: any;
  driver_nic: any;
  interval: any;
  statusred: boolean;
  markers: any;
  driver_name: any;
  item = 'assets/img/bluebox.png';
  constructor(
    public navCtrl: NavController,
    public api: Api,
    public geolocation: Geolocation,
    public zone: NgZone,
    private diagnostic: Diagnostic,
    private locationAccuracy: LocationAccuracy,
    public platform: Platform,
    public callnumber: CallNumber,
    public globalVar: GlobalVars,
  ) {

    this.student = this.globalVar.student;
    this.today = Date.now();
    this.markers = [];
  }

  ngOnInit() {
    console.log('ionViewDidLoad PeoplePage');
    let mapOptions = {
      zoom: 19,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
      zoomControl: false,
      streetViewControl: false
    }

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    if (this.platform.is('cordova')) {
      this.diagnostic.isLocationEnabled().then((enabled) => {
        if (enabled) {
          this.askForHighAccuracy();
        } else {
          this.locationAccuracy.canRequest().then((canRequest: boolean) => {
            this.askForHighAccuracy();
          });
        }
      })
    }
    this.api.get("parent/student/get_student_van.json", { van_id: this.student.van_id }).subscribe(data => {
      console.log(data);
      this.status = data["van"]["status"];
      if (data["van"]["status"] == "end_to_school") {
        this.status = "end to school";
      }
      else if (data["van"]["status"] == "end_from_school") {
        this.status = "end from school"
      }
      else if (data["van"]["status"] == "start_to_school") {
        this.status = "start to school"
      }
      else if (data["van"]["status"] == "start_from_school") {
        this.status = "start from school"
      }
      this.van_color = data["van"]["van_color"];
      this.driver_name = data["van"]["van_driver"]["van_driver_name"]
      this.driver_mobile_number = data["van"]["van_driver"]["van_driver_number"];
      this.van_registration_number = data["van"]["van_registration_number"];
      this.driver_nic = data["van"]["van_driver"]["nic"];
      if (this.status == "end to school" || this.status == "end from school") {
        this.statusred = true
      }
      let self = this;
      if (this.status == "start to school" || this.status == "start from school") {

        this.interval = setInterval(function () {
          self.initMap();

        }, 10000);
      }

    });
    let self = this;
    self.initMap();


  }
  askForHighAccuracy() {
    this.locationAccuracy
      .request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY);
  }


  initMap() {
    this.geolocation.getCurrentPosition().then((position) => {
      this.lat = position.coords.latitude;
      this.lng = position.coords.longitude;
      this.currentline(position.coords.latitude, position.coords.longitude);
    });

  }
  currentline(lat, lng) {
    this.api.get("parent/student/get_van_current_location", { van_id: this.student.van_id }).subscribe(data => {
      console.log(data);
      this.van_lat = data["latitude"];
      this.van_lng = data["longitude"];
      this.map.setCenter({ lat: this.van_lat, lng: this.van_lng })
      var haight = new google.maps.LatLng(this.van_lat, this.van_lng);
      var oceanBeach = new google.maps.LatLng(lat, lng);
      this.loadMarkers();

      directionsRenderer.setMap(this.map);
      var request = {
        origin: haight,
        destination: oceanBeach,

        travelMode: 'DRIVING'
      };
      directionsService.route(request, function (response, status) {
        if (status == 'OK') {
          // directionsRenderer.setDirections(response);
        }
        //        console.log(response);
        this.distance = response.routes[0].legs[0].duration.text;
        document.getElementById("fleet_distance").innerText = this.distance;
        //   console.log(this.distance)

      });

      // directionsService.route(
      //   {
      //     origin:haight,
      //     destination: oceanBeach,
      //     travelMode: 'DRIVING'
      //   },
      //   function(response, status) {
      //     if (status === 'OK') {
      //             directionsRenderer.setDirections(response);
      //     } else {
      //       // window.alert('Directions request failed due to ' + status);
      //     }
      //   })

    })
  }
  loadMarkers() {
    this.deleteMarkers();

    let updatelocation = new google.maps.LatLng(this.van_lat, this.van_lng);
    this.addMarker(updatelocation);
    this.setMapOnAll(this.map);


  }
  setMapOnAll(map) {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(map);
    }
    // this.dropmarkers[0].setMap(map);

  }

  deleteMarkers() {
    this.clearMarkers();
    this.markers = [];
    //   this.dropmarkers=
  }
  clearMarkers() {
    this.setMapOnAll(null);
  }
  addMarker(location) {
    let marker = new google.maps.Marker({
      position: location,
      map: this.map,
      animation: google.maps.Animation.DROP,
    });
    this.markers.push(marker);
  }
  callDriver() {
    this.callnumber.callNumber(this.driver_mobile_number, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
    console.log("individual" + this.driver_mobile_number);

  }
  ngOnDestroy() {
    let self = this
    console.log("view leave");
    if (this.interval) {
      clearInterval(this.interval);
    }


  }

}
