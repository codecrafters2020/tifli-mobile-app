import { HttpClient, HttpParams,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { GlobalVars } from './global-vars.service';
// import { Storage } from '@ionic/storage';
//import { Observable } from 'rxjs/Observable';
@Injectable()
export class AwsProvider {


  apiUrl = '';


   public headers : any
  constructor(public http: HttpClient,public globalVar: GlobalVars) {
    this.apiUrl = this.globalVar.server_link + "/api/v1";
  }

  getSignedUploadRequest(name,ext, type) {
    this.setHeader()
    // return this.http.get(`${this.apiUrl}/companies/presign_upload?filename=${name}&filetype=${type}&contentType=jpeg`, {headers: this.headers});

    return this.http.get(`${this.apiUrl}/signed_urls.json?filename=${name}&filetype=${type}&contentType=jpg`,
    {headers: this.headers});


  }

  // getFileList(): Observable<Array<any>> {
  //   return this.http.get(`${this.apiUrl}aws/files`)
  //     .map(res => res.json())
  //     .map(res => {
  //       return res['Contents'].map(val => val.Key);
  //     });
  // }

  // getSignedFileRequest(name) {
  //   return this.http.get(`${this.apiUrl}aws/files/${name}`).map(res => res.json());
  // }

  // deleteFile(name) {
  //   return this.http.delete(`${this.apiUrl}aws/files/${name}`).map(res => res.json());
  // }

  // https://www.thepolyglotdeveloper.com/2015/03/create-a-random-nonce-string-using-javascript/
  randomString = function (length) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < length; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  }

  uploadFile(url, file) {
    return this.http.put(url, file);
  }

  uploadFileToServer(file) {
    this.setHeader()

    let data = {
                individual_informations: {
                  name: 'test',
                  address: 'test',
                  landline_no: 'asdf',
                  website_address: 'asdf',
                  license_number: 'asd',
                  license_expiry_date: null,
                  secondary_contact_name: 'asdf',
                  secondary_mobile_no: '2332',
                  news_update: true,
                  current_password: 'asdfasdf',
                  avatar: file
                }
              }

    return this.http.post(`${this.apiUrl}/companies/add_individual_information.json`,data, {headers: this.headers});
  }
  setHeader(){
    this.headers = new HttpHeaders({
       Authorization: this.globalVar.current_user.auth_token|| '',
       "Access-Control-Allow-Origin": "*"

    })
  }
}
