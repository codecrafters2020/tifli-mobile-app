import { Component,OnInit } from '@angular/core';
import { NavController, LoadingController, ToastController } from '@ionic/angular';
import { GlobalVars } from '../../providers/global-vars.service';
import { Api } from '../../providers/api/api.service';
import { ParentParamsService } from '../../providers/parent-params.service';
import * as moment from 'moment';

@Component({
  selector: 'app-parent-album',
  templateUrl: './parent-album.page.html',
  styleUrls: ['./parent-album.page.scss'],
})
export class ParentAlbumPage implements OnInit{
  showalbumcover: any;
  albumcover: any;
  albumcovershowonpage: any;
  server: any;
  students: any;
//  classsectionid:any;
  classsectiontosend;
  filtermonthwise

  constructor(
    public navCtrl: NavController,
    public api: Api,
    private loadingController: LoadingController,
    public globalVar: GlobalVars,
    public parentparam: ParentParamsService,
    public toastCtrl: ToastController,
    ) {
      this.showalbumcover=[];
      this.albumcover = [];
      this.albumcovershowonpage=[];
      this.students=[];
      //this.classsectionid=[];
      this.classsectiontosend=[];
  }

  ngOnInit() {
    console.log('ionViewDidLoad ParentAlbumPage');
    var date=new Date();
    var d=moment(date);
  //  this.getAlbums(d.month()+1);
    this.server=this.globalVar.server_link;
    this.students = this.globalVar.current_user.students;
    for(var i=0; i<this.students.length;i++)
    {
    //  this.classsectionid= {classsectionid:this.students[i].class_section.id}
      this.classsectiontosend.push(this.students[i].class_section.id);
      }
      this.filtermonthwise=d.month()+1;
      this.getAlbums(this.classsectiontosend);

  }
  getAlbums(classsectionid)
  {
    this.showalbumcover=[];
    this.albumcovershowonpage=[];
    this.api.get("parent/albums/show_all_albums.json",{class_section_ids:classsectionid,month:this.filtermonthwise}).subscribe(async data=>{
      console.log(data);
      if(!data["message"])
      {
        this.showalbumcover = data["albums"];
        for(var i=0;i<this.showalbumcover.length;i++)
        {
          if(this.showalbumcover[i].pictures[0]!=undefined)
          {
            this.albumcover = {image:this.showalbumcover[i].pictures[0].image_url,
              albumname:this.showalbumcover[i].album_name,images:
              this.showalbumcover[i].pictures};
              this.albumcovershowonpage.push(this.albumcover);
  
          }
        }  
      }
      else{
        let toast = await this.toastCtrl.create({
          message:"No Albums Found.",
          duration: 4000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger",
          color:"danger"
        });
        await toast.present();

      }
    });

  }
  albumShow(i)
  {
    this.parentparam.album_images = this.albumcovershowonpage[i];
     this.navCtrl.navigateForward('parent-albumdetail');
  }

}
