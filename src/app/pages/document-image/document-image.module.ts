import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DocumentImagePageRoutingModule } from './document-image-routing.module';

import { DocumentImagePage } from './document-image.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DocumentImagePageRoutingModule
  ],
  declarations: [DocumentImagePage]
})
export class DocumentImagePageModule {}
