import { Component, NgZone,OnInit } from '@angular/core';
import { NavController,ToastController, LoadingController, ActionSheetController } from '@ionic/angular';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/camera/ngx';
import { AwsProvider } from '../../providers/aws.service';
import b64toBlob from "b64-to-blob";
import { Api } from '../../providers/api/api.service';
import { GlobalVars } from '../../providers/global-vars.service';
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media/ngx';

@Component({
  selector: 'app-faculty-videoupload',
  templateUrl: './faculty-videoupload.page.html',
  styleUrls: ['./faculty-videoupload.page.scss'],
})
export class FacultyVideouploadPage implements OnInit {

  images: any;
  avatar: any;
  description;
  classes: any;
  privacyisprivate: boolean;
  classesandsection;
  video_name;
  classId: any;
  private win: any = window;
  constructor(
    public navCtrl: NavController,
    public api: Api,
    public toastCtrl: ToastController,
    private loadingController: LoadingController,
    public camera: Camera,
    private actionSheetController: ActionSheetController,
    public zone: NgZone,
    private awsProvider: AwsProvider,
    public globalVar: GlobalVars,
    private streamingMedia: StreamingMedia

  ) {
    this.classes = [];
    this.classId = [];
    this.privacyisprivate = false;
    this.classesandsection = [];
  }

  async ngOnInit() {
    const loading = await this.loadingController.create({
      // content:"loading data..."
    });
    await loading.present();
    console.log('ionViewDidLoad TeacherVideouploadPage');
    this.api.get("faculty/class_sections/show_all.json").subscribe(data => {
      console.log(data);
      this.classes = data["class_sections"];
      loading.dismiss();
    }, async err => {
      loading.dismiss();
      let toast = await this.toastCtrl.create({
        message: "Something went wrong",
        duration: 4000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger",
        color:"danger"
      });
      toast.present();
    });
  }

  previouspage()
  {
    this.navCtrl.navigateRoot('faculty-allvideo');
  }
  privacy(i) {
    if (i == 1) {
      console.log("private");
      this.privacyisprivate = true;

    }
    else {
      console.log("public");
      this.classesandsection = [];
      this.privacyisprivate = false;
    }
  }

  takePicture(sourceType) {
    const options: CameraOptions = {
      quality: 100,
      correctOrientation: true,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.VIDEO,
      sourceType: sourceType
    }

    this.camera.getPicture(options).then((imageData) => {
      this.images = this.win.Ionic.WebView.convertFileSrc(imageData);


    });
  }
  playvideo() {
    let options: StreamingVideoOptions = {
      successCallback: () => { console.log('Video played') },
      errorCallback: (e) => { console.log('Error streaming') },
      //  orientation: 'landscape',
      shouldAutoClose: true,
      controls: true
    };

    this.streamingMedia.playVideo(this.images, options);

  }
  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
      header: "Select Video source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }
  async uploadVideo() {
    if(this.images)
    {
      const loading = await this.loadingController.create({
      });
      await loading.present();
      let that = this;
      let ext = '.mp4';
      let type = 'video/mp4'
      let newName = this.awsProvider.randomString(6) + new Date().getTime() + '.' + ext;
  
      this.awsProvider.getSignedUploadRequest(newName, ext, type).subscribe(data => {
        console.log("Success in getSignedUploadRequest", data)
        that.avatar = data["public_url"]
        console.log("i am in public url", data["public_url"])
  
        that.zone.run(() => {
          let request = new XMLHttpRequest();
          request.open('GET', this.images, true);
          request.responseType = 'blob';
          request.timeout = 360000;
          request.onload = async function () {
            let sizeInMB:any = (request.response.size / (1024*1024)).toFixed(2);
            if(sizeInMB > 50)
            {
               loading.dismiss();
               let toast = await that.toastCtrl.create({
                 message: "Upload video of size less than 50mb.",
                 duration: 4000,
                 position: 'top',
                 showCloseButton: true,
                 closeButtonText: "x",
                 cssClass: "toast-danger",
                 color:"danger"
                });
               await toast.present();
               return;
            }
            let reader = new FileReader();
            reader.readAsDataURL(request.response);
            reader.onload = function (e) {
                debugger
                that.zone.run(() => {
                console.log("error", e);
                debugger
                var b1 = <string>reader.result
                let blob = b64toBlob(b1 .replace(/^data:video\/\w+;base64,/, ""), 'mp4');
                // let blob = b64toBlob(reader.result.replace(/^data:video\/\w+;base64,/, ""), 'mp4');
  
                that.awsProvider.uploadFile(data["presigned_url"], blob).subscribe(_result => {
  
                  console.log("result that i got after uploading to aws", _result)
                  loading.dismiss();
                  that.uploadVideo2(that.avatar)
  
                });
              })
  
            }
  
            reader.onerror = function (e) {
              console.log('We got an error in file reader:::::::: ', e);
            };
  
            reader.onabort = function (event) {
              console.log("reader onabort", event)
            }
  
          };
  
          request.onerror = function (e) {
            console.log("** I got an error in XML Http REquest", e);
          };
  
          request.ontimeout = function (event) {
            console.log("xmlhttp ontimeout", event)
          }
  
          request.onabort = function (event) {
            console.log("xmlhttp onabort", event);
          }
  
  
          request.send();
        });
      }, async (err) => {
        let toast = await this.toastCtrl.create({
          message: "Unable to send request. Check wifi connection",
          duration: 6000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger",
          color:"danger"
        });
        toast.present();
        console.log('getSignedUploadRequest timeout error: ', err);
      });
    }
    else
    {
      let toast = await this.toastCtrl.create({
        message: "No video attached",
        duration: 4000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger",
        color:"danger"
      });
      toast.present();
    }


  }

  async uploadVideo2(public_url) {
    const loading = await this.loadingController.create({
      // content:"loading data..."
    });
    await loading.present();
    if (this.classesandsection != null) {
      for (var z = 0; z < this.classesandsection.length; z++) {
        this.classId.push(this.classesandsection[z].id)
        //  console.log(this.classesandsection[z].id)
      }
    }

    let that = this;
    if (this.classId.length != 0) {

      that.api.post("faculty/videos.json",{video_name:that.video_name,
        description:that.description,is_public:!that.privacyisprivate,
       video_url:public_url,faculty_id:that.globalVar.current_user.information.id,class_sections:this.classId})
      .subscribe(async data=>{
          let toast = await that.toastCtrl.create({
            message: "Video Uploaded Successfully",
            duration: 6000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-success",
            color:"success"
          });
          toast.present();
          loading.dismiss();
          this.navCtrl.navigateForward('faculty-home');
        }, async err => {
          loading.dismiss();
          let toast = await that.toastCtrl.create({
            message: "Something went wrong",
            duration: 6000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-danger",
            color:"danger"
          });
          toast.present();
        })
    }
    else {
      that.api.post("faculty/videos.json",{video_name:that.video_name,
        description:that.description,is_public:that.privacyisprivate,
       video_url:public_url,faculty_id:that.globalVar.current_user.information.id,class_sections:this.classId})
      .subscribe(async data=>{
          let toast = await that.toastCtrl.create({
            message: "Video Uploaded Successfully",
            duration: 6000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-success",
            color:"success"
          });
          toast.present();
          loading.dismiss();
          this.navCtrl.navigateForward('faculty-home');
        }, async err => {
          loading.dismiss();
          let toast = await that.toastCtrl.create({
            message: "Something went wrong",
            duration: 6000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-danger",
            color:"danger"
          });
          toast.present();
        })
    }
  }
  cancel() {
    this.navCtrl.navigateForward('faculty-home');
  }
  base64toBlob(base64Data, contentType) {
    contentType = contentType || '';
    var sliceSize = 1024;
    var byteCharacters = atob(base64Data);
    var bytesLength = byteCharacters.length;
    var slicesCount = Math.ceil(bytesLength / sliceSize);
    var byteArrays = new Array(slicesCount);

    for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
      var begin = sliceIndex * sliceSize;
      var end = Math.min(begin + sliceSize, bytesLength);

      var bytes = new Array(end - begin);
      for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
        bytes[i] = byteCharacters[offset].charCodeAt(0);
      }
      byteArrays[sliceIndex] = new Uint8Array(bytes);
    }
    return new Blob(byteArrays, { type: contentType });
  }
}
