import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacultyAllvideoPage } from './faculty-allvideo.page';

describe('FacultyAllvideoPage', () => {
  let component: FacultyAllvideoPage;
  let fixture: ComponentFixture<FacultyAllvideoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacultyAllvideoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacultyAllvideoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
