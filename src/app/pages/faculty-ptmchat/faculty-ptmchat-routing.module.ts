import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacultyPtmchatPage } from './faculty-ptmchat.page';

const routes: Routes = [
  {
    path: '',
    component: FacultyPtmchatPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacultyPtmchatPageRoutingModule {}
