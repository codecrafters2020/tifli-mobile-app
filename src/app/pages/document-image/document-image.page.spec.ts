import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DocumentImagePage } from './document-image.page';

describe('DocumentImagePage', () => {
  let component: DocumentImagePage;
  let fixture: ComponentFixture<DocumentImagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentImagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DocumentImagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
