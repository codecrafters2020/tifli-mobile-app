import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParentAllvideoPage } from './parent-allvideo.page';

const routes: Routes = [
  {
    path: '',
    component: ParentAllvideoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParentAllvideoPageRoutingModule {}
