import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ParentAlbumPage } from './parent-album.page';

describe('ParentAlbumPage', () => {
  let component: ParentAlbumPage;
  let fixture: ComponentFixture<ParentAlbumPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentAlbumPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ParentAlbumPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
