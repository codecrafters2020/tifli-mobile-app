import { Component,OnInit } from '@angular/core';
import { NavController, ToastController, LoadingController } from '@ionic/angular';
import { Api } from '../../providers/api/api.service';
import { GlobalVars } from '../../providers/global-vars.service';
import { FacultyParamsService } from '../../providers/faculty-params.service';
@Component({
  selector: 'app-faculty-allresult',
  templateUrl: './faculty-allresult.page.html',
  styleUrls: ['./faculty-allresult.page.scss'],
})
export class FacultyAllresultPage implements OnInit{


  show_result: any;
  classdata: any;
  courseid;
  sectionsfromapi;
  sectionid;
  id;
  course;
  courseimg = 'assets/img/course_name.png';
  studentcard = 'assets/img/student_card.png';
  constructor(
    public navCtrl: NavController,
    public api: Api,
    public toastCtrl: ToastController,
    private loadingController: LoadingController,
    public globalVar: GlobalVars,
    public facultyparam: FacultyParamsService
  ) {
    this.show_result = [];
    this.classdata = [];
    this.sectionsfromapi = [];
    this.course = [];
  }

  async ngOnInit() {
    const loading = await this.loadingController.create({
      // content:"loading data..."
    });
    await loading.present();

    this.api.get("faculty/section_courses/get_course_by_faculty.json",
      { faculty_id: this.globalVar.current_user.information.id }).subscribe(data => {

        console.log(data);
        this.course = data;

      });

    this.api.get("faculty/section_courses/get_faculty_teached_class.json",
      { faculty_id: this.globalVar.current_user.information.id }).subscribe(data => {
        loading.dismiss();
        this.classdata = data["classes"];
        console.log(this.classdata)
        //  this.coursesArray={class:this.coursedata[i].school_class_id,section:this.sections};
      }, err => {
        loading.dismiss();
      });


    console.log('ionViewDidLoad TeacherAllresultPage');
    this.api.get("faculty/result/get_filtered_results.json",
      { faculty_id: this.globalVar.current_user.information.id }).subscribe(data => {
        console.log(data);
        this.show_result = data["results"];
        loading.dismiss();
      }, async err => {
        loading.dismiss();
        let toast = await this.toastCtrl.create({
          message: "Something went wrong. Please Try Later",
          duration: 4000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger"
        });
        toast.present();

      })
  }

  async sendCourse(classid) {
    const loading = await this.loadingController.create({
      // content:"loading data..."
    });
    await loading.present();

    this.api.get("faculty/section_courses/get_faculty_teached_section.json",
      { faculty_id: this.globalVar.current_user.information.id, school_class_id: classid })
      .subscribe(data => {
        this.sectionsfromapi = data["class_sections"];
        loading.dismiss();
      }, err => {
        loading.dismiss();
      });
    this.filterresults(classid, 'class');
  }
  async filterresults(id, type) {
    this.show_result = [];
    const loading = await this.loadingController.create({
      // content:"loading data..."
    });
    await loading.present();
    if (this.id != undefined && this.sectionid != undefined) {
      this.api.get("faculty/result/get_filtered_results.json",
        {
          faculty_id: this.globalVar.current_user.information.id, section_course_id: this.id,
          section_id: this.sectionid
        }).subscribe(data => {
          console.log(data);
          this.show_result = data["results"];
          loading.dismiss();
        }, async err => {
          loading.dismiss();
          let toast = await this.toastCtrl.create({
            message: "Something went wrong. Please Try Later",
            duration: 4000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-danger"
          });
          toast.present();

        })

    }
    else {
      if (this.id != undefined) {
        this.api.get("faculty/result/get_filtered_results.json",
          { faculty_id: this.globalVar.current_user.information.id, section_course_id: this.id }).subscribe(data => {
            console.log(data);
            this.show_result = data["results"];
            loading.dismiss();
          }, async err => {
            loading.dismiss();
            let toast = await this.toastCtrl.create({
              message: "Something went wrong. Please Try Later",
              duration: 4000,
              position: 'top',
              showCloseButton: true,
              closeButtonText: "x",
              cssClass: "toast-danger"
            });
            toast.present();

          })
      }
      if (this.sectionid != undefined) {
        this.api.get("faculty/result/get_filtered_results.json",
          { faculty_id: this.globalVar.current_user.information.id, section_id: this.sectionid }).subscribe(data => {
            console.log(data);
            this.show_result = data["results"];
            loading.dismiss();
          }, async err => {
            loading.dismiss();
            let toast = await this.toastCtrl.create({
              message: "Something went wrong. Please Try Later",
              duration: 4000,
              position: 'top',
              showCloseButton: true,
              closeButtonText: "x",
              cssClass: "toast-danger"
            });
            toast.present();

          })

      }
      else if (this.courseid != undefined) {
        this.api.get("faculty/result/get_filtered_results.json",
          { faculty_id: this.globalVar.current_user.information.id, class_id: this.courseid }).subscribe(data => {
            console.log(data);
            this.show_result = data["results"];
            loading.dismiss();
          }, async err => {
            loading.dismiss();
            let toast = await this.toastCtrl.create({
              message: "Something went wrong. Please Try Later",
              duration: 4000,
              position: 'top',
              showCloseButton: true,
              closeButtonText: "x",
              cssClass: "toast-danger"
            });
            toast.present();

          })

      }
    }

  }
  newResult() {
    this.navCtrl.navigateRoot('faculty-newresult');
  }
  result_details(index) {
    this.facultyparam.result_detail = this.show_result[index];
    this.navCtrl.navigateRoot('faculty-allresultdetail');
  }
}
