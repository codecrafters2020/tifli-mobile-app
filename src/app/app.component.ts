import { Component, ViewChild } from '@angular/core';
import { NavController, Platform, AlertController, Events, ToastController, ModalController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Storage } from '@ionic/storage';
import { GlobalVars } from './providers/global-vars.service';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { Push, PushObject, PushOptions } from '@ionic-native/push/ngx';
import { Dialogs } from '@ionic-native/dialogs/ngx';
import { interval } from 'rxjs';
import { Api } from './providers/api/api.service';
import { NetworkProvider } from './providers/network.service';
import { Network } from '@ionic-native/network/ngx';
import * as moment from 'moment';
import { SplashPage } from './pages/splash/splash.page';
import { Zoom } from '@ionic-native/zoom/ngx';
import { ParentParamsService } from './providers/parent-params.service';
import { FacultyParamsService }  from './providers/faculty-params.service';
import { Router } from '@angular/router';

declare var cordova;
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  // @ViewChild(Nav) nav: Nav;
  public animateVarible: boolean = true;
  rootPage: any;
  // rootPage: any = 'SignInPage';
  pages: Array<{ title: string, component: any, img: any, icon: any }>;
  user: any;
  userrole;
  alert: any;
  notificationTypes: any = ['New leave application',
    'New meeting request', 'New comment', 'Leave Application', 'Your child', 'Appointment date set',
    'A new notice by the teacher', 'New album has been added','New resource for','Result of '];
  faculty_leave_app_page = false;
  ptm_teacher_page = false;
  ptm_comment_new = false;
  ptm_leave_parent = false;
  student_attendance = false;
  appointment_date_parent = false;
  notice_parent = false;
  album_parent = false;
  item = 'assets/img/doodle.png';
  API_KEY = "mWHCgDbKXhsvUUMkuUSyV5fHFFQA7bauFzf5"
  API_SECRET = "SCjHRp69hVKrjaGRiyV0ryda2IJdPy4Vsvzh"
  resource_parent = false;
  result_parent=false;

  constructor(
    private router: Router,
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public storage: Storage,
    public globalVar: GlobalVars,
    public backgroundMode: BackgroundMode,
    private push: Push,
    private dialogs: Dialogs,
    public api: Api,
    public alertCtrl: AlertController,
    public networkProvider: NetworkProvider,
    public network: Network,
//    public app: App,
    public events: Events,
    public toastCtrl: ToastController,
    public modalCtrl: ModalController,
    public zoomService: Zoom,
    public navCtrl: NavController,
    public parentParam: ParentParamsService,
    public facultyParam: FacultyParamsService
  ) {
    this.user = [];
    let self = this
    this.platform.ready().then(async () => {
      this.statusBar.styleDefault();
               //this.splashScreen.hide();
            //  this.navCtrl.navigateRoot('splash');
      let splash = await this.modalCtrl.create({
        component:SplashPage
      });
      await splash.present();
      if (!this.platform.is("cordova")) {
        console.log("no cordova available");
      }
      else {
        this.backgroundMode.enable();
        this.backgroundMode.on('activate').subscribe(() => {
          this.backgroundMode.disableWebViewOptimizations();
          cordova.plugins.backgroundMode.disableBatteryOptimizations();


          this.backgroundMode.setDefaults({
            title: "New College App running in background",
            text: " ",
          });

        });

      }
      this.initializeApp();
      this.initPushNotification();
      this.checkNetwork();
    });
    // used for an example of ngFor and navigation
    // this.pages = [
    //   { title: 'Home', component: 'HomePage',img:'assets/img/menu/001.png',icon:'' },
    //   { title: 'Profile', component: 'ProfilePage',img:'assets/img/menu/002.png',icon:'' },
    //   { title: 'Attendance', component: 'PeoplePage',img:'assets/img/menu/003.png',icon:'' },
    //   { title: 'Maps', component: 'MapPage',img:'assets/img/menu/004.png',icon:'' },
    //   { title: 'Diary', component: 'CoursesPage',img:'assets/img/menu/005.png',icon:'' },
    //   { title: 'Dining', component: 'DiningHallsPage',img:'assets/img/menu/006.png',icon:'' },
    //   { title: 'Athletics', component: 'AthleticsPage',img:'assets/img/menu/007.png',icon:'' },
    //   { title: 'News', component: 'NewsPage',img:'assets/img/menu/008.png',icon:'' },
    //   { title: 'Events', component: 'EventsPage',img:'assets/img/menu/009.png',icon:'' },
    //   { title: 'Bookmarks', component: 'BookmarksPage',img:'',icon:'ios-star' },
    //   { title: 'Setting', component: 'SettingPage',img:'',icon:'md-settings' },
    //   { title: 'Notice', component: 'NoticePage',img:'assets/img/menu/006.png',icon:'' },
    //   { title: 'Album', component: 'AlbumPage',img:'assets/img/menu/albums.png',icon:'' },
    //   { title: 'Time Table', component: 'TimeTablePage',img:'assets/img/menu/timetable.jpg',icon:'' },
    //   { title: 'Parents Diary', component: 'ParentsDiaryPage',img:'assets/img/menu/timetable.jpg',icon:'' },
    // ];

  }
  initializeZoom() {
    console.log("ini xoom")
    this.zoomService.initialize(this.API_KEY, this.API_SECRET)
      .then((success) => {
        console.log(success);
        console.log("done zoom")
      //  this.presentToast(success);
      })
      .catch((error) => {
        console.log(error);
//        this.presentToast(error);
      });

  }
  initializeApp() {
    this.storage.ready().then(() => {
      this.storage.get('currentuser').then(user => {
        this.user = user;
        console.log(user);
        if (user) {
          this.globalVar.current_user = user;
          this.userrole = user.role;
          this.updateCurrentUser();
          this.initializeZoom();
        } else {
           this.openPage('sign-in');
//          this.rootPage = 'SignInPage';

        }
        if (user) {
        }
      });
    });

  }

  async presentToast(text) {
    const toast = await this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    await toast.present();
  }
  updateCurrentUser() {
    let self = this;
    if (this.user.role != null && this.user.role == "driver") {
      // this.rootPage = 'DriverHomePage';
           self.openPage('driver-home');
      //      this.globalVar.current_user = this.user;
      this.backButtonHandler();
    }
    else if (this.user.role != null && this.user.role == "faculty") {
      // this.rootPage = 'HomePage';
           self.openPage('faculty-home');
      this.globalVar.current_user = this.user;
      this.backButtonHandler();
      this.backgroundMode.disable();
    }
    else if (this.user.role != null && this.user.role == "parent") {
      // this.rootPage = 'ParentsHomePage';
      this.globalVar.current_user = this.user;
      self.openPage('parent-home');
      this.backButtonHandler();
      this.backgroundMode.disable();
    }
    else {
      // this.rootPage = 'SignInPage';

           this.openPage('sign-in')
    }
  }
  openPage(page) {
    this.navCtrl.navigateRoot(page);
//    this.nav.setRoot(page);
  }
  logout() {
    let now = moment().format('DD-MMM-YYYY HH:mm:ss');

    this.api.patch("sms_users/remove_login_device", {
      device_id: this.globalVar.device_id,
      current_time: now
    }).subscribe(data => {
      this.api.delete("sms_users/" + this.globalVar.current_user.id + ".json").subscribe(data => {
        console.log(data);
        this.globalVar.current_user = "";
        this.storage.remove("currentuser").then(() => {
          this.openPage('sign-in');
          //  this.nav.setRoot("LandPage");
        });

      })

    })

  }
  initPushNotification() {
    if (!this.platform.is("cordova")) {
      console.warn(
        "Push notifications not initialized. Cordova is not available - Run in physical device"
      );
      return;
    }

    if (this.platform.is("ios")) {
      this.globalVar.os = "ios";
    } else if (this.platform.is("android")) {
      this.globalVar.os = "android";
    }
    const options: PushOptions = {
      android: {
        //   senderID: "235106695213",
        //  sound:"true"
      },
      ios: {
        alert: "true",
        badge: false,
        sound: "true"
      },
      windows: {},
      browser: {
        pushServiceURL: "http://push.api.phonegap.com/v1/push"
      }
    };
    this.push.createChannel({
      id: "TestChannel",
      description: "My first test channel",
      importance: 4,
      sound: "filhall_violin",
    }).then(() => console.log('Channel created'));

    // this.push.createChannel({
    //   id: "TestChannel1",
    //   description: "My first test channel",
    //   importance: 4,
    // }).then(() => console.log('Channel created'));
    // Delete a channel (Android O and above)
    //      this.push.deleteChannel('LorryzChannel1').then(() => console.log('Channel deleted'));
    const pushObject: PushObject = this.push.init(options);

    pushObject.on("registration").subscribe((data: any) => {
      this.globalVar.device_id = data.registrationId;
      console.log("device token ->", data.registrationId);
    });
    //660476
    pushObject.on("notification").subscribe(async (data: any) => {
      console.log('dataObject');
      console.log(data);
      const startTime = Math.floor(Date.now() / 1000);
      let beep = interval(1000).subscribe((res) => {
        this.dialogs.beep(1);
        const currentTime = Math.floor(Date.now() / 1000);
        console.log(currentTime - startTime);
        if (currentTime - startTime == 90) {
          beep.unsubscribe();
        }
      });
      if (data.message.includes(this.notificationTypes[0])) {
        this.faculty_leave_app_page = true;
        this.ptm_teacher_page = false;
        this.ptm_comment_new = false;
        this.ptm_leave_parent = false;
        this.student_attendance = false;
        this.appointment_date_parent = false;
        this.notice_parent = false;
        this.album_parent = false;
        this.resource_parent = false;
        this.result_parent=false;
      }
      else if (data.message.includes(this.notificationTypes[1])) {
        this.faculty_leave_app_page = false;
        this.ptm_teacher_page = true;
        this.ptm_comment_new = false;
        this.ptm_leave_parent = false;
        this.student_attendance = false;
        this.appointment_date_parent = false;
        this.notice_parent = false;
        this.album_parent = false;
        this.resource_parent = false;
        this.result_parent=false;
      }
      else if (data.message.includes(this.notificationTypes[2])) {
        this.faculty_leave_app_page = false;
        this.ptm_teacher_page = false;
        this.ptm_comment_new = true;
        this.ptm_leave_parent = false;
        this.student_attendance = false;
        this.appointment_date_parent = false;
        this.notice_parent = false;
        this.album_parent = false;
        this.resource_parent = false;
        this.result_parent=false;
      }
      else if (data.message.includes(this.notificationTypes[3])) {
        this.faculty_leave_app_page = false;
        this.ptm_teacher_page = false;
        this.ptm_comment_new = false;
        this.ptm_leave_parent = true;
        this.student_attendance = false;
        this.appointment_date_parent = false;
        this.notice_parent = false;
        this.album_parent = false;
        this.resource_parent = false;
        this.result_parent=false;
      }
      else if (data.message.includes(this.notificationTypes[4])) {
        this.faculty_leave_app_page = false;
        this.ptm_teacher_page = false;
        this.ptm_comment_new = false;
        this.ptm_leave_parent = false;
        this.student_attendance = true;
        this.appointment_date_parent = false;
        this.notice_parent = false;
        this.album_parent = false;
        this.resource_parent = false;
        this.result_parent=false;
      }
      else if (data.message.includes(this.notificationTypes[5])) {
        this.faculty_leave_app_page = false;
        this.ptm_teacher_page = false;
        this.ptm_comment_new = false;
        this.ptm_leave_parent = false;
        this.student_attendance = false;
        this.appointment_date_parent = true;
        this.notice_parent = false;
        this.album_parent = false;
        this.resource_parent = false;
        this.result_parent=false;
      }
      else if (data.message.includes(this.notificationTypes[6])) {
        this.faculty_leave_app_page = false;
        this.ptm_teacher_page = false;
        this.ptm_comment_new = false;
        this.ptm_leave_parent = false;
        this.student_attendance = false;
        this.appointment_date_parent = false;
        this.notice_parent = true;
        this.album_parent = false;
        this.resource_parent = false;
        this.result_parent=false;
      }
      else if (data.message.includes(this.notificationTypes[7])) {
        this.faculty_leave_app_page = false;
        this.ptm_teacher_page = false;
        this.ptm_comment_new = false;
        this.ptm_leave_parent = false;
        this.student_attendance = false;
        this.appointment_date_parent = false;
        this.notice_parent = false;
        this.album_parent = true;
        this.resource_parent = false;
        this.result_parent=false;
      }
      else if(data.message.includes(this.notificationTypes[8]))
      {
        this.faculty_leave_app_page=false;
        this.ptm_teacher_page=false;
        this.ptm_comment_new=false;
        this.ptm_leave_parent=false;
        this.student_attendance=false;
        this.appointment_date_parent=false;
        this.notice_parent=false;
        this.album_parent=false;
        this.resource_parent = true;
        this.result_parent=false;
      }
      else if(data.message.includes(this.notificationTypes[9]))
      {
        this.faculty_leave_app_page=false;
        this.ptm_teacher_page=false;
        this.ptm_comment_new=false;
        this.ptm_leave_parent=false;
        this.student_attendance=false;
        this.appointment_date_parent=false;
        this.notice_parent=false;
        this.album_parent=false;
        this.resource_parent = false;
        this.result_parent=true;
      }

      if (this.faculty_leave_app_page) {
        let confirmAlert = await this.alertCtrl.create({
          header: "New Notification",
          message: data.message,
          //BackdropDismiss: true,
          buttons: [
            {
              text: "Exit",
              handler: () => {
                beep.unsubscribe();
              }
            },

            {
              text: "View",
              handler: () => {
                this.teacher_leave();
              }
            },

          ]
        });
        await confirmAlert.present();
        beep.unsubscribe();

      }
      else if (this.ptm_teacher_page) {
        let confirmAlert = await this.alertCtrl.create({
         header: "New Notification",
          message: data.message,
          //enableBackdropDismiss: false,
          buttons: [
            {
              text: "Exit",
              handler: () => {
                beep.unsubscribe();
              }
            },

            {
              text: "View",
              handler: () => {
                this.ptm_teacher();
              }
            },

          ]
        });
        await confirmAlert.present();
        beep.unsubscribe();


      }
      else if (this.album_parent) {
        let confirmAlert = await this.alertCtrl.create({
          header: "New Notification",
          message: data.message,
          //enableBackdropDismiss: false,
          buttons: [
            {
              text: "Exit",
              handler: () => {
                beep.unsubscribe();
              }
            },

            {
              text: "View",
              handler: () => {
                this.notify_parent_album();
              }
            },

          ]
        });
        await confirmAlert.present();
        beep.unsubscribe();


      }

      else if (this.notice_parent) {
        let confirmAlert = await this.alertCtrl.create({
          header: "New Notification",
          message: data.message,
         // enableBackdropDismiss: false,
          buttons: [
            {
              text: "Exit",
              handler: () => {
                beep.unsubscribe();
              }
            },

            {
              text: "View",
              handler: () => {
                this.notify_notice_parent(data.additionalData.student_id)
              }
            },

          ]
        });
        await confirmAlert.present();
        beep.unsubscribe();


      }
      else if (this.appointment_date_parent) {
        let confirmAlert = await this.alertCtrl.create({
         header : "New Notification",
          message: data.message,
          //enableBackdropDismiss: false,
          buttons: [
            {
              text: "Exit",
              handler: () => {
                beep.unsubscribe();
              }
            },

            {
              text: "View",
              handler: () => {
                this.notify_ptm_parent(data.additionalData.student_id, data.additionalData.id);
              }
            },

          ]
        });
        await confirmAlert.present();
        beep.unsubscribe();


      }
      else if (this.student_attendance) {
        let confirmAlert = await this.alertCtrl.create({
          header: "New Notification",
          message: data.message,
         // enableBackdropDismiss: false,
          buttons: [
            {
              text: "Exit",
              handler: () => {
                beep.unsubscribe();
              }
            },

            {
              text: "View",
              handler: () => {
                this.notify_attendance_parent(data.additionalData.student_id);
              }
            },

          ]
        });
       await  confirmAlert.present();
        beep.unsubscribe();


      }
      else if (this.ptm_leave_parent && this.globalVar.current_user.role == "parent") {
        let confirmAlert = await this.alertCtrl.create({
          header: "New Notification",
          message: data.message,
         // enableBackdropDismiss: false,
          buttons: [
            {
              text: "Exit",
              handler: () => {
                beep.unsubscribe();
              }
            },

            {
              text: "View",
              handler: () => {
                this.leave_parent(data.additionalData.student_id);
              }
            },

          ]
        });
       await confirmAlert.present();
        beep.unsubscribe();


      }
      else if (this.ptm_comment_new && this.globalVar.current_user.role == "faculty") {
        let confirmAlert = await this.alertCtrl.create({
          header: "New Notification",
          message: data.message,
          //enableBackdropDismiss: false,
          buttons: [
            {
              text: "Exit",
              handler: () => {
                beep.unsubscribe();
              }
            },

            {
              text: "View",
              handler: () => {
                // var split1 = data.message.split(":");
                // var split2 = split1[1].split(" ");
                this.faculty_comment_page(data.additionalData.id);
              }
            },

          ]
        });
        await confirmAlert.present();
        beep.unsubscribe();


      }
      else if (this.ptm_comment_new && this.globalVar.current_user.role == "parent") {
        let confirmAlert = await this.alertCtrl.create({
          header: "New Notification",
          message: data.message,
         // enableBackdropDismiss: false,
          buttons: [
            {
              text: "Exit",
              handler: () => {
                beep.unsubscribe();
              }
            },

            {
              text: "View",
              handler: () => {
                // var split1 = data.message.split(":");
                // var split2 = split1[1].split(" ");
                // console.log(split2[1])
                this.parent_comment_page(data.additionalData.id);
              }
            },

          ]
        });
        await confirmAlert.present();
        beep.unsubscribe();


      }
      else if(this.result_parent)
      {
        let confirmAlert = await this.alertCtrl.create({
          header: "New Notification",
          message: data.message,
         // enableBackdropDismiss: false,
          buttons: [
            {
              text: "Exit",
              handler: () => {
                beep.unsubscribe();
              }
            },

            {
              text: "View",
              handler: () => {
                this.teacher_result_notify(data.additionalData.student_id);
              }
            },

          ]
        });
        await confirmAlert.present();
        beep.unsubscribe();

      }
      else if(this.resource_parent)
      {
        let confirmAlert = await this.alertCtrl.create({
          header: "New Notification",
          message: data.message,
         // enableBackdropDismiss: false,
          buttons: [
            {
              text: "Exit",
              handler: () => {
                beep.unsubscribe();
              }
            },

            {
              text: "View",
              handler: () => {
                this.teacher_resouce_notify(data.additionalData.student_id);
              }
            },

          ]
        });
        await confirmAlert.present();
        beep.unsubscribe();

      }
      else {
        let confirmAlert = await this.alertCtrl.create({
          header: "New Notification",
          message: data.message,
          //enableBackdropDismiss: false,
          buttons: [

            {
              text: "OK",
              handler: () => {
                // this.newRatingCargoNotification();
                //            beep.unsubscribe();
              }
            },

          ]
        });
        await confirmAlert.present();
        beep.unsubscribe();
      }
    });
  }
  async checkNetwork() {
    console.log("hello network")
    if (this.network.type == "none") {
      let toast = await this.toastCtrl
        .create({
          message: "Sorry, no Internet connectivity detected",
          duration: 3000,
          position: "top",
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger"
        })
        // tslint:disable-next-line: align
        toast.present();
    }
    this.networkProvider.initializeNetworkEvents();

    // Offline event
    this.events.subscribe("network:offline", async () => {
      // alert('network:offline ==> '+this.network.type);
      let toast = await this.toastCtrl
        .create({
          message: "Sorry, no Internet connectivity detected",
          duration: 3000,
          position: "top",
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger"
        })
        toast.present();
    });

    // Online event
    this.events.subscribe("network:online", async () => {
      // this.nav.setRoot("LoginPage");
      let toast = await this.toastCtrl
        .create({
          message: `You are now online via ${this.network.type}`,
          duration: 3000,
          position: "top",
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-success"
        })
        toast.present();
    });
  }

  backButtonHandler() {
    this.platform.backButton.subscribe(data => {
      console.log(data);
      debugger;
      if (this.router.url == '/parent-home' || this.router.url == '/faculty-home' || this.router.url == '/driver-home'){ 
        //please change if your path name is different for login and home
        this.showCloseAlert();
       }
      // let nav = this.app.getActiveNavs()[0];
      // if (nav.canGoBack()) {
      //   //Can we go back?
      //   nav.pop();
      // } else if (
      //   this.nav.getActive().id === 'ParentsHomePage' ||
      //   this.nav.getActive().id === 'DriverHomePage' ||
      //   this.nav.getActive().id === 'HomePage'
      // ) {
      //   this.showCloseAlert();
      // }
      // else {
      //   //   debugger
      //   if (this.globalVar.current_user) {
      //     console.log("here in alert 3")
      //     this.setRootPage();
      //     // this.nav.setRoot(this.rootPage);
      //   } else {
      //     console.log("here in alert 4")
      //     this.showCloseAlert();
      //   }
      // }
    });
  }
  setRootPage() {
    let self = this;
    if (this.globalVar.current_user.role != null && this.globalVar.current_user.role == "driver") {
      self.openPage('driver-home');
    }
    else if (this.globalVar.current_user.role != null && this.globalVar.current_user.role == "faculty") {
      self.openPage('faculty-home');
    }
    else if (this.globalVar.current_user.role != null && this.globalVar.current_user.role == "parent") {
      self.openPage('parent-home');
    }

  }

  async showCloseAlert() {
    console.log("here in alert 1")
    if (this.alert) {
      this.alert.dismiss();
      this.alert = "";
      return;
    }
    console.log("here in alert 2")
    this.alert = await this.alertCtrl.create({
      header: "App termination",
      message: "Do you want to close the app?",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            this.alert = "";
          }
        },
        {
          text: "Close App",
          handler: () => {
            navigator['app'].exitApp();
            //this.platform.exitApp(); // Close this application
          }
        }
      ],
     // enableBackdropDismiss: false
    });
    this.alert.present();
  }
  notify_parent_album() {
    this.navCtrl.navigateRoot("parent-album");
  }
  notify_notice_parent(id) {
    this.parentParam.notify_notice_parent = id;
    this.navCtrl.navigateRoot('parent-noticedetail');
  }
  notify_ptm_parent(id, cid) {
   this.parentParam.notify_ptm_parent_student_id = id ;
   this.parentParam.notify_ptm_parent_meeting_id = cid; 
   this.navCtrl.navigateRoot('PtmCommunicateteacherPage');
  }
  notify_attendance_parent(id) {
    this.parentParam.notify_attendance_studentid=id;
   this.navCtrl.navigateRoot('attendance-report');
  }
  leave_parent(id) {
    this.parentParam.notify_leave_parent = id;
    this.navCtrl.navigateRoot('parent-leave');
  }
  parent_comment_page(id) {
    this.parentParam.ptm_discussionid = id;
    this.navCtrl.navigateRoot('parent-ptmchat');
  }

  faculty_comment_page(id) {
    this.facultyParam.ptm_discussionmeetingid = id;
    this.navCtrl.navigateRoot('faculty-ptmchat');
  }
    teacher_result_notify(id)
  {
    this.navCtrl.navigateRoot('parent-allresult');
  }
  teacher_resouce_notify(id)
  {
    this.navCtrl.navigateRoot('parent-resource');
  }
  parentsHome() {
    // { title: 'Home', component: 'ParentsHomePage',img:'assets/img/menu/001.png',icon:'' },
    this.navCtrl.navigateRoot('parent-home');      
  }
  faculty_Result() {
    this.navCtrl.navigateRoot('faculty-allresult');
  }
  faculty_Progress() {
    this.navCtrl.navigateRoot('faculty-allprogress');
  }
  parent_Result() {
    this.navCtrl.navigateRoot('parent-allresult');
  }
  studentsDiary() {
    // { title: 'Students Diary', component: 'ParentsDiaryPage',img:'assets/img/menu/timetable.jpg',icon:'' },
    this.navCtrl.navigateRoot('parent-diary');
  }
  ChildSummary() {
    // { title: 'Childs Summary', component: 'NewsDetailsPage',img:'assets/img/home/001.png',icon:'' },
    this.navCtrl.navigateRoot('parent-studentsummary');
  }
  ChidAttendance() {
    // { title: 'Childs Summary', component: 'NewsDetailsPage',img:'assets/img/home/001.png',icon:'' },
    this.navCtrl.navigateRoot('attendance-report');
  }
  trackStudent() {
    // { title: 'Track Student', component: 'TrackStudentPage',img:'assets/img/road.png',icon:'' },
    this.navCtrl.navigateRoot('parent-trackingmap');
  }
  home() {
    // { title: 'Home', component: 'HomePage',img:'assets/img/menu/001.png',icon:'' },
    this.navCtrl.navigateRoot('faculty-home');
  }
  diary() {
    // { title: 'Diary', component: 'CoursesPage',img:'assets/img/menu/005.png',icon:'' },
    this.navCtrl.navigateRoot('faculty-newdiary');

  }
  notice() {
    // { title: 'Notice', component: 'NoticePage',img:'assets/img/menu/006.png',icon:'' },
    this.navCtrl.navigateRoot('faculty-newnotice');

  }
  album() {
    // { title: 'Album', component: 'AlbumPage',img:'assets/img/menu/albums.png',icon:'' },
    this.navCtrl.navigateRoot('album')

  }
  attendance() {
    // { title: 'Attendance', component: 'PeoplePage',img:'assets/img/menu/003.png',icon:'' },
    this.navCtrl.navigateRoot('faculty-attendance');

  }
  parent_resouce() {

    this.navCtrl.navigateRoot('parent-resource');

  }
  teacher_resouce() {

    this.navCtrl.navigateRoot('faculty-allresources');

  }
  school_events() {

    this.navCtrl.navigateRoot('events')

  }
  teacher_video() {
    this.navCtrl.navigateRoot('faculty-allvideo');
  }
  parent_video() {
    this.navCtrl.navigateRoot('parent-allvideo');
  }
  Album() {
    // { title: 'Album', component: 'ParentAlbumPage',img:'assets/img/home/albums.png',icon:'' },
    this.navCtrl.navigateRoot('parent-album');

  }
  Notice() {
    // { title: 'Students Notice', component: 'ParentNoticePage',img:'assets/img/home/006.png',icon:'' },
    this.navCtrl.navigateRoot('parent-noticedetail');
  }
  faculty_timetable() {
    // { title: 'Time Table', component: 'TimeTablePage',img:'assets/img/menu/timetable.jpg',icon:'' },
   this.navCtrl.navigateRoot("faculty-timetable");
  }
  parent_timetable() {
    // { title: 'Time Table', component: 'TimeTablePage',img:'assets/img/menu/timetable.jpg',icon:'' },
   this.navCtrl.navigateRoot("parent-timetable");
  }
   resetPassword() {
    // { title: 'Time Table', component: 'TimeTablePage',img:'assets/img/menu/timetable.jpg',icon:'' },
   this.navCtrl.navigateRoot("reset-password");
  }
    payment() {
    // { title: 'Payment', component: 'ParentPaymentPage',img:'assets/img/home/albums.png',icon:'' },
    this.navCtrl.navigateRoot('parent-payment');
  }
  ptm() {
    // { title: 'PTM', component: 'ParentPtmPage',img:'assets/img/ptm.png',icon:'' },
    this.navCtrl.navigateRoot('parent-ptm');
  }
  calenders() {
    // { title: 'Calender', component: 'ParentCalendersPage',img:'assets/img/007.png',icon:'' },
    this.navCtrl.navigateRoot('events');
  }
  ptm_teacher() {
    // {img:'assets/img/home/timetable.jpg',title:'Teacher Parent Interactions',component:'TeacherCommentPage'},

    this.navCtrl.navigateRoot('faculty-ptm');
  }
  parent_survey() {
    this.navCtrl.navigateRoot('parent-surveydetail');
  }
  parent_leave() {
    // { title: 'Leave', component: 'ParentLeavePage',img:'assets/img/leavepage.png',icon:'' },
    this.navCtrl.navigateRoot('parent-leave');
  }
  teacher_leave() {
    // {img:'assets/img/leavepage.png',title:'Leave',component:'TeacherLeavePage'},
    this.navCtrl.navigateRoot('faculty-leave');

  }
  LoginUser()
  {
    this.navCtrl.navigateRoot('login')
  }
  NonLoginUser()
  {
    this.navCtrl.navigateRoot('non-login')
  }
}
