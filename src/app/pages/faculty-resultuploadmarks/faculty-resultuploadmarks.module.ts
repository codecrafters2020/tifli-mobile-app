import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacultyResultuploadmarksPageRoutingModule } from './faculty-resultuploadmarks-routing.module';

import { FacultyResultuploadmarksPage } from './faculty-resultuploadmarks.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacultyResultuploadmarksPageRoutingModule
  ],
  declarations: [FacultyResultuploadmarksPage]
})
export class FacultyResultuploadmarksPageModule {}
