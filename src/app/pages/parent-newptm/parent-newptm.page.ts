import { Component , OnInit} from '@angular/core';
import { NavController, ToastController, LoadingController } from '@ionic/angular';
import { Api } from '../../providers/api/api.service';
import { GlobalVars } from '../../providers/global-vars.service';

@Component({
  selector: 'app-parent-newptm',
  templateUrl: './parent-newptm.page.html',
  styleUrls: ['./parent-newptm.page.scss'],
})
export class ParentNewptmPage implements OnInit{
  student: any;
  studentname: any;
  subject: any;
  description;
  today: any;
  studentid;
  from_date;
  unformated_pickup_time;
  parentid: any;
  loading: any;
  constructor(
    public navCtrl: NavController,
    public api: Api,
    public globalVar: GlobalVars,
    public toastCtrl: ToastController,
    private loadingController: LoadingController,
  ) {
  }

  ngOnInit() {
    console.log('ionViewDidLoad PtmRequestappointmentPage');
    this.today = Date.now()
  }
  disabled() {
    if (!this.description) {
      return true;
    }
    else {
      return false;
    }
  }
  cancel() {
    // this.navCtrl.pop();
    this.navCtrl.navigateRoot('parent-ptm');
  }
  async upload() {
    const loading = await this.loadingController.create({
    });
    await loading.present();

    this.api.post("parent/parent_teacher_meetings.json", {
      parent_id: this.globalVar.current_user.information.id,
      faculty_id: this.globalVar.student.class_section.faculty.id,
      student_id: this.globalVar.student.id,
      description: this.description,
      subject: this.subject,
    }).subscribe(async data => {
      console.log(data);
      loading.dismiss();
      this.navCtrl.navigateRoot('parent-home');
      let toast = await this.toastCtrl.create({
        message: "PTM Request Sent",
        duration: 4000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-success"
      });
      await toast.present();
    }, async err => {
      loading.dismiss();
      let toast = await this.toastCtrl.create({
        message: "Something went wrong.",
        duration: 4000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger"
      });
      await toast.present();
    });
  }
  previouspage()
  {
    this.navCtrl.navigateRoot('parent-ptm');
  }
}
