import { Component,OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ParentParamsService } from 'src/app/providers/parent-params.service';
import { GlobalVars } from '../../providers/global-vars.service';

@Component({
  selector: 'app-document-image',
  templateUrl: './document-image.page.html',
  styleUrls: ['./document-image.page.scss'],
})
export class DocumentImagePage implements OnInit {

  imageUrl: any;
  diary: any;
  subject: any;
  diaryinfo: any;
  albumtrue: any;

  constructor(
    public navCtrl: NavController, 
    public parentparam : ParentParamsService,
    public globalVar: GlobalVars,

    ) {
  }

  ngOnInit() {
    console.log('ionViewDidLoad DocumentImagePage');
    this.imageUrl = this.parentparam.doc_img;
    // this.albumtrue = this.navParams.get("album")
    console.log(this.imageUrl);
    if(this.parentparam.diary_detaildoc!=null)
    {
      this.diary=this.parentparam.diary_detaildoc;
      this.subject = this.diary.subject;
      this.diaryinfo = this.diary.diaryinfo;
      console.log(this.diary)
    }
  }
  previouspage()
  {
    if(this.globalVar.current_user.role == 'parent')
    {
      this.navCtrl.navigateRoot('parent-leave');
    }
    else
    {
      this.navCtrl.navigateRoot('faculty-leave');
    }
  }

}
