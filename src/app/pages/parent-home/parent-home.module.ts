import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentHomePageRoutingModule } from './parent-home-routing.module';

import { ParentHomePage } from './parent-home.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentHomePageRoutingModule
  ],
  declarations: [ParentHomePage]
})
export class ParentHomePageModule {}
