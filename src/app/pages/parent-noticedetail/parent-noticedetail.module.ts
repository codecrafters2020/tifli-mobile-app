import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentNoticedetailPageRoutingModule } from './parent-noticedetail-routing.module';

import { ParentNoticedetailPage } from './parent-noticedetail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentNoticedetailPageRoutingModule
  ],
  declarations: [ParentNoticedetailPage]
})
export class ParentNoticedetailPageModule {}
