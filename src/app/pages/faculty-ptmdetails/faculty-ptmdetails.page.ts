import { Component,OnInit } from '@angular/core';
import { NavController} from '@ionic/angular';
import { Api } from '../../providers/api/api.service';
import { FacultyParamsService } from '../../providers/faculty-params.service';
@Component({
  selector: 'app-faculty-ptmdetails',
  templateUrl: './faculty-ptmdetails.page.html',
  styleUrls: ['./faculty-ptmdetails.page.scss'],
})
export class FacultyPtmdetailsPage implements OnInit{
  meeting: any;
  meetingcmnts: any;
  cmntsobject
  cmntShow: any;
  comment;
  from_date;
  unformated_pickup_time;
  constructor(
    public navCtrl: NavController,
    public api: Api,
    public facultyparams: FacultyParamsService
    ) {
      this.meeting=this.facultyparams.ptm_details;
      this.meetingcmnts=[];
      this.cmntShow=[];
      this.cmntsobject=[];
  }

  ngOnInit() {
    console.log('ionViewDidLoad TeacherCommentmodalPage');

  }
  // dismiss()
  // {
  //   this.viewCtrl.dismiss("cancel");
  // }
  setAppointment()
  {
    this.api.patch("faculty/parent_teacher_meetings/set_appointment_date.json",
    {id:this.meeting.id,
      appointment_date :this.from_date,appointment_time :this.unformated_pickup_time}).subscribe(data=>{
          console.log(data);
          this.navCtrl.navigateRoot('faculty-home');
          // this.viewCtrl.dismiss();
        },err=>{

        });

  }
  discussion()
  {
    this.facultyparams.ptm_discussionmeetingid = this.meeting.id;
    this.facultyparams.ptm_discussionsubject = this.meeting.subject
    this.navCtrl.navigateRoot('faculty-ptmchat');
  }
  previouspage()
  {
    this.navCtrl.navigateRoot('faculty-ptm');
  }
}
