import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParentMeetingdetailsPage } from './parent-meetingdetails.page';

const routes: Routes = [
  {
    path: '',
    component: ParentMeetingdetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParentMeetingdetailsPageRoutingModule {}
