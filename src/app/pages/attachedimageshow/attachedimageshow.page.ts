import { Component, OnInit } from '@angular/core';
import { ParentParamsService } from 'src/app/providers/parent-params.service';
import { GlobalVars } from '../../providers/global-vars.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-attachedimageshow',
  templateUrl: './attachedimageshow.page.html',
  styleUrls: ['./attachedimageshow.page.scss'],
})
export class AttachedimageshowPage implements OnInit {

  imageUrl;
  constructor(
    public parentparam : ParentParamsService,
    public globalVar: GlobalVars,
    public navCtrl: NavController,

  ) { }

  ngOnInit() {
    this.imageUrl = this.parentparam.doc_img;
  }
  previouspage()
  {
    if(this.parentparam.diary_detaildoc == 'diary')
    {
      this.navCtrl.navigateRoot('parent-diary');
    }
    else
    {
      this.navCtrl.navigateRoot('parent-noticedetail');
    }
  }
}
