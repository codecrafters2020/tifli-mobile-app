import { Component,OnInit } from '@angular/core';
import { NavController,ToastController, LoadingController } from '@ionic/angular';
import { Api } from '../../providers/api/api.service';
import { GlobalVars } from '../../providers/global-vars.service';
import * as moment from 'moment';

@Component({
  selector: 'app-parent-allresult',
  templateUrl: './parent-allresult.page.html',
  styleUrls: ['./parent-allresult.page.scss'],
})
export class ParentAllresultPage implements OnInit{


  courseimg='assets/img/course_name.png';
  studentcard='assets/img/student_card.png';
  show_result:any;
  filtermonthwise
  constructor(
    public navCtrl: NavController,
    public api: Api,
    public toastCtrl: ToastController,
    private loadingController: LoadingController,
    public globalVar: GlobalVars,
) {
    this.show_result=[];
  }

  ngOnInit() {
    var date=new Date();
    var d=moment(date);
    this.loadResult(d.month()+1);
    console.log('ionViewDidLoad ParentAllresultPage');
    
  }
  loadResult(month)
  {
    this.show_result=[];
    this.api.get("parent/result/get_filtered_results.json",{student_id:this.globalVar.student.id,month:month})
    .subscribe(async data=>{
        console.log(data);
        if(!data["message"])
        {
          this.show_result=data["student_results"]
        }
        else{
          let toast = await this.toastCtrl.create({
            message:"No Result Found.",
            duration: 4000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-danger",
            color:"danger"
          });
          await toast.present();
  
        }
    })
  }
}
