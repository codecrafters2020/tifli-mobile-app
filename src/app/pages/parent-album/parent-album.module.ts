import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentAlbumPageRoutingModule } from './parent-album-routing.module';

import { ParentAlbumPage } from './parent-album.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentAlbumPageRoutingModule
  ],
  declarations: [ParentAlbumPage]
})
export class ParentAlbumPageModule {}
