import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacultyIndividualdiaryPage } from './faculty-individualdiary.page';

describe('FacultyIndividualdiaryPage', () => {
  let component: FacultyIndividualdiaryPage;
  let fixture: ComponentFixture<FacultyIndividualdiaryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacultyIndividualdiaryPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacultyIndividualdiaryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
