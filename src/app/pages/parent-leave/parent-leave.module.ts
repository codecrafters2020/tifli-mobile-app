import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentLeavePageRoutingModule } from './parent-leave-routing.module';

import { ParentLeavePage } from './parent-leave.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentLeavePageRoutingModule
  ],
  declarations: [ParentLeavePage]
})
export class ParentLeavePageModule {}
