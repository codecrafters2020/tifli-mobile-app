import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParentDiaryPage } from './parent-diary.page';

const routes: Routes = [
  {
    path: '',
    component: ParentDiaryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParentDiaryPageRoutingModule {}
