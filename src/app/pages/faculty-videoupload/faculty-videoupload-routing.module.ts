import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacultyVideouploadPage } from './faculty-videoupload.page';

const routes: Routes = [
  {
    path: '',
    component: FacultyVideouploadPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacultyVideouploadPageRoutingModule {}
