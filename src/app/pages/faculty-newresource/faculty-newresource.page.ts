import { Component,OnInit, NgZone } from '@angular/core';
import { NavController,ToastController, LoadingController, ActionSheetController } from '@ionic/angular';
import { Api } from '../../providers/api/api.service';
import { AwsProvider } from '../../providers/aws.service';
import { DocumentViewer } from '@ionic-native/document-viewer/ngx';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/camera/ngx';
import b64toBlob from "b64-to-blob";
import { GlobalVars } from '../../providers/global-vars.service';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
@Component({
  selector: 'app-faculty-newresource',
  templateUrl: './faculty-newresource.page.html',
  styleUrls: ['./faculty-newresource.page.scss'],
})
export class FacultyNewresourcePage implements OnInit{
  description;
  subject;
  resource_name;
  documentToUpload: any;
  filename: any;
  document: any;
  loader: any;
  classdata: any;
  courseid;
  sectionsfromapi: any;
  sectionid;
  course: any;
  courseidtosend;
  private win: any = window;

  constructor(
    public navCtrl: NavController,
    public api: Api,
    public toastCtrl: ToastController,
    private loadingController: LoadingController,
    public camera: Camera,
    private actionSheetController: ActionSheetController,
    public zone: NgZone,
    private awsProvider: AwsProvider,
    private viewer: DocumentViewer,
    public globalVar: GlobalVars,
    private FilePath:FilePath,
    private fileChooser:FileChooser  
  ) {
    this.classdata = [];
    this.sectionsfromapi = [];
    this.course = [];
  }

  async ngOnInit() {
    const loading = await this.loadingController.create({
      // content:"loading data..."
    });
    await loading.present();
    console.log('ionViewDidLoad TeacherNewresourcePage');
    this.api.get("faculty/section_courses/get_faculty_teached_class.json",
      { faculty_id: this.globalVar.current_user.information.id }).subscribe(data => {
        loading.dismiss();
        this.classdata = data["classes"];
        console.log(this.classdata)
        //  this.coursesArray={class:this.coursedata[i].school_class_id,section:this.sections};
      }, err => {
        loading.dismiss();
      });

  }
  previouspage()
  {
    this.navCtrl.navigateRoot('faculty-allresources');
  }
  async sendCourse(courseid) {
    const loading = await this.loadingController.create({
      // content:"loading data..."
    });
    await loading.present();

    this.api.get("faculty/section_courses/get_faculty_teached_section.json",
      { faculty_id: this.globalVar.current_user.information.id, school_class_id: courseid })
      .subscribe(data => {
        this.sectionsfromapi = data["class_sections"];
        //  for(var i=0;i<this.sectionsfromapi.length;i++)
        //  {
        //    this.alignSections(this.sectionsfromapi[i].section.section_name,this.sectionsfromapi[i].section.id);
        //  }
        loading.dismiss();
      }, err => {
        loading.dismiss();
      });

  }
  async getCourse(sectionid) {
    const loading = await this.loadingController.create({
      // content:"loading data..."
    });
    await loading.present();

    this.api.get("faculty/section_courses/get_section_courses.json", {
      section_id: sectionid,
      faculty_id: this.globalVar.current_user.information.id
    }).
      subscribe(data => {
        this.course = data["section_courses"]
        // for(var i=0;i<this.course.length;i++)
        // {
        //   this.Courses(this.course[i].course.course_name,this.course[i].id,this.course[i].students);
        // }
        loading.dismiss();
      }, err => {
        loading.dismiss();
      });

  }
  cancel() {
    this.navCtrl.navigateForward('faculty-home');
  }
  takeDocuments()
  {
    this.fileChooser.open().then(uri => {
      this.FilePath.resolveNativePath(uri).then(async nativepath => {
        this.documentToUpload = this.win.Ionic.WebView.convertFileSrc(nativepath);
        var fileName = nativepath.substr(this.documentToUpload.lastIndexOf('/') + 1);
        this.filename = fileName;
        debugger;
        var fileExtension = fileName.substr(fileName.lastIndexOf('/') + 1);
        if(fileExtension=='jpg' || fileExtension=='png' || fileExtension=='mp4')
        {
          this.documentToUpload=null;
          let toast = await this.toastCtrl.create({
            message: "Select resource similar to pdf",
            duration: 4000,
            position: 'top',
            showCloseButton: true,
            closeButtonText: "x",
            cssClass: "toast-danger",
            color:"danger"
          });
        }
     }, err => {
        alert(JSON.stringify(err));
      })
    }, err => {
      alert(JSON.stringify(err));
    })
  }
//   takeDocuments(sourceType) {

//     const options: CameraOptions = {
//       quality: 100,
//       correctOrientation: true,
//       destinationType: this.camera.DestinationType.FILE_URI,
//       //    encodingType: this.camera.EncodingType.JPEG,
//       mediaType: this.camera.MediaType.ALLMEDIA,
//       sourceType: sourceType
//     }

//     let that = this;
//     this.camera.getPicture(options).then((imageData) => {
//       this.documentToUpload = this.win.Ionic.WebView.convertFileSrc(imageData);
// //      this.documentToUpload = imageData;
//       var fileName = imageData.substr(this.documentToUpload.lastIndexOf('/') + 1);
//       this.filename = fileName;
//     }, async (err) => {
//       let toast = await this.toastCtrl.create({
//         message: "Unable to Access Gallery. Please Try Later",
//         duration: 6000,
//         position: 'top',
//         showCloseButton: true,
//         closeButtonText: "x",
//         cssClass: "toast-danger",
//         color:"danger"
//       });
//       toast.present();
//       console.log('err: ', err);
//     });
//   }

  // async presentActionSheetForDocuments() {
  //   let actionSheet = await this.actionSheetController.create({
  //     header: 'Select Document Source',
  //     buttons: [
  //       {
  //         text: 'Load from Library',
  //         handler: () => {
  //           this.takeDocuments(this.camera.PictureSourceType.PHOTOLIBRARY);
  //         }
  //       },
  //       // {
  //       //   text: 'Use Camera',
  //       //   handler: () => {
  //       //     this.takeDocuments(this.camera.PictureSourceType.CAMERA);
  //       //   }
  //       // },
  //       {
  //         text: 'Cancel',
  //         role: 'cancel'
  //       }
  //     ]
  //   });
  //   actionSheet.present();
  // }
  async uploadDocumentResource() {
    if(this.documentToUpload)
    {
    this.loader = await this.loadingController.create({
    });
    await this.loader.present();
    let that = this;
    if (this.documentToUpload != null) {
      var fileName = this.documentToUpload.substr(this.documentToUpload.lastIndexOf('/') + 1);
      this.filename = fileName;
      var fileExtension = fileName.substr(fileName.lastIndexOf('/') + 1);
      let ext = fileExtension;
      let type = 'application/' + fileExtension;
      let newName = this.awsProvider.randomString(6) + new Date().getTime() + '.' + ext;
      console.log("**new NAme **", newName);

      this.awsProvider.getSignedUploadRequest(newName, ext, type).subscribe(data => {

        console.log("** I got success in getSignedUploadRequest", data);
        this.document = data["public_url"];
        that.zone.run(() => {
          let request = new XMLHttpRequest();
          request.open('GET', this.documentToUpload, true);
          request.responseType = 'blob';
          request.timeout = 360000;
          request.onload = function () {
            that.zone.run(() => {
              let reader = new FileReader();
              reader.readAsDataURL(request.response);

              console.log("Step 1 reader", reader);
              console.log("Step 1 got stuck in request.response", request)

              reader.onload = function () {
                that.zone.run(() => {
                  console.log("Step 2 Loaded", reader.result);
                  var b1 = <string>reader.result
                  let blob = b64toBlob(b1 .replace(/^data:application\/\w+;base64,/, ""), fileExtension);
                  // let blob = b64toBlob(reader.result.replace(/^data:application\/\w+;base64,/, ""), fileExtension);
                  console.log("blob", blob);
                  that.awsProvider.uploadFile(data["presigned_url"], blob).subscribe(_result => {
                    console.log("result that i got after uploading to aws", _result)
                    that.api.post("faculty/resources.json", {
                      faculty_id: that.globalVar.current_user.information.id,
                      section_course_id: that.courseidtosend, description: that.description,
                      resource_url: that.document
                    })
                      .subscribe(async data => {
                        let toast = await that.toastCtrl.create({
                          message: "Resource Uploaded Successfully",
                          duration: 4000,
                          position: 'top',
                          showCloseButton: true,
                          closeButtonText: "x",
                          cssClass: "toast-success",
                          color:"success"
                        });
                        toast.present();
                        that.loader.dismiss();
                        that.navCtrl.navigateForward('faculty-home');
                      }, async err => {
                        that.loader.dismiss();
                        let toast = await that.toastCtrl.create({
                          message: "Something went wrong",
                          duration: 4000,
                          position: 'top',
                          showCloseButton: true,
                          closeButtonText: "x",
                          cssClass: "toast-danger",
                          color:"danger"
                        });
                        toast.present();
                      })
                  }, (err) => {
                    console.log('err in uploadFile:::::::: ', err);
                  });
                });
              }

              reader.onerror = function (e) {
                console.log('We got an error in file reader:::::::: ', e);
              };

              reader.onabort = function (event) {
                console.log("reader onabort", event)
              }
            })
          };

          request.onerror = function (e) {
            that.loader.dismiss();
            console.log("** I got an error in XML Http REquest", e);
          };

          request.ontimeout = function (event) {
            that.loader.dismiss();
            console.log("xmlhttp ontimeout", event)
          }

          request.onabort = function (event) {
            that.loader.dismiss();
            console.log("xmlhttp onabort", event);
          }


          request.send();
        })
      }, async (err) => {
        that.loader.dismiss();
        console.log('getSignedUploadRequest timeout error: ', err);

        let toast = await this.toastCtrl.create({
          message: "Unable to send request. Check wifi connection",
          duration: 6000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger",
          color:"danger"
        });
        toast.present();

      });


    }
  }
  else{
    let toast = await this.toastCtrl.create({
      message: "No Resource Attached",
      duration: 4000,
      position: 'top',
      showCloseButton: true,
      closeButtonText: "x",
      cssClass: "toast-danger",
      color:"danger"
    });
    toast.present();

  
      }
  }

}
