import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ParentNoticePage } from './parent-notice.page';

describe('ParentNoticePage', () => {
  let component: ParentNoticePage;
  let fixture: ComponentFixture<ParentNoticePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentNoticePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ParentNoticePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
