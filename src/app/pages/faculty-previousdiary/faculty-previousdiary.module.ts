import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacultyPreviousdiaryPageRoutingModule } from './faculty-previousdiary-routing.module';

import { FacultyPreviousdiaryPage } from './faculty-previousdiary.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacultyPreviousdiaryPageRoutingModule
  ],
  declarations: [FacultyPreviousdiaryPage]
})
export class FacultyPreviousdiaryPageModule {}
