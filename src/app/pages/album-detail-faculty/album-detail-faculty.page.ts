import { Component,OnInit, NgZone } from '@angular/core';
import {
  NavController, 
  ActionSheetController, ToastController, Platform, LoadingController, ModalController
} from '@ionic/angular';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/camera/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { AwsProvider } from '../../providers/aws.service';
import b64toBlob from "b64-to-blob";
import { Api } from '../../providers/api/api.service';
import { GlobalVars } from '../../providers/global-vars.service';
import { FacultyParamsService } from '../../providers/faculty-params.service';
@Component({
  selector: 'app-album-detail-faculty',
  templateUrl: './album-detail-faculty.page.html',
  styleUrls: ['./album-detail-faculty.page.scss'],
})
export class AlbumDetailFacultyPage implements OnInit{
  images = [];
  imageResponse: any;
  options: any;
  showgrid: any;
  private win: any = window;
  id: any;
  avatar: any;
  uploadurl: any;
  loading;
  constructor(
    public navCtrl: NavController,
    private camera: Camera,
    private actionSheetController: ActionSheetController,
    private imagePicker: ImagePicker,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public zone: NgZone,
    private awsProvider: AwsProvider,
    public api: Api,
    public globalVar: GlobalVars,
    private loadingController: LoadingController,
    public facultyParams: FacultyParamsService
    // private viewCtrl: ViewController,

  ) {
    this.uploadurl = [];
    this.imageResponse = [];
    this.showgrid = false;
  }

  ngOnInit() {
    console.log('ionViewDidLoad AlbumPage');
    this.id = this.facultyParams.addmoreimagesalbum;
    console.log(this.id);
  }
  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          //  this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          this.imagePicker.hasReadPermission().then(data => {
            if (data != true) {
              this.imagePicker.requestReadPermission().then(read => {
                this.getImages()
              })
            }
            else {
              this.getImages()
            }
          })

        }
      },
      // {
      //   text: 'Use Camera',
      //   handler: () => {
      //    this.takePicture(this.camera.PictureSourceType.CAMERA);
      //   //this.takePhoto();
      //   }
      // },
      {
        text: 'Cancel',
        role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }
  previouspage()
  {
    this.navCtrl.navigateRoot('album');
  }
  getImages() {
    this.options = {
      // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
      // selection of a single image, the plugin will return it.
      maximumImagesCount: 15,

      // max width and height to allow the images to be.  Will keep aspect
      // ratio no matter what.  So if both are 800, the returned image
      // will be at most 800 pixels wide and 800 pixels tall.  If the width is
      // 800 and height 0 the image will be 800 pixels wide if the source
      // is at least that wide.
      // width: 200,
      // height: 200,

      // quality of resized image, defaults to 100
      quality: 100,

      // output type, defaults to FILE_URIs.
      // available options are
      // window.imagePicker.OutputType.FILE_URI (0) or
      // window.imagePicker.OutputType.BASE64_STRING (1)
      outputType: 0
    };
    let self = this;
//    this.imageResponse = [];
    self.imagePicker.getPictures(this.options).then((results) => {
      for (var i = 0; i < results.length; i++) {
        // this.imageResponse.push('data:image/jpeg;base64,' + results[i]);
        // self.imageResponse.push(results[i]);
        this.imageResponse.push(this.win.Ionic.WebView.convertFileSrc(results[i]));

      }
      // if(this.id!=undefined || this.id!=null)
      // {
      //     this.upload2aws();
      // }
    }, (err) => {
      alert(err);
    });
  }
  takPhoto() {
    this.camera.getPicture({
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.CAMERA,
      encodingType: this.camera.EncodingType.PNG,
      saveToPhotoAlbum: true
    }).then(imageData => {
      this.imageResponse.push('data:image/jpeg;base64,' + imageData)
      // this.uploadPhoto();
    }, error => {
      console.log("ERROR -> " + JSON.stringify(error));
    });
  }
  uploadPhoto() {
    this.facultyParams.album_create_array=this.imageResponse;
    this.navCtrl.navigateForward('album-modal');
  }
  async upload2aws() {
    const loading = await this.loadingController.create({
      // content:"loading data..."
    });
    for (var im of this.imageResponse) {
      await this.upload2awspart1(im);
    }
  }
  async upload2awspart1(im) {
  this.loading = await this.loadingController.create({
      // content:"loading data..."
    });
   await this.loading.present();
    await this.delay();
    let that = this;

    let ext = 'png';
    let type = 'image/png';
    let newName = this.awsProvider.randomString(6) + new Date().getTime() + '.' + ext;

    this.awsProvider.getSignedUploadRequest(newName, ext, type).subscribe(data => {
      console.log("Success in getSignedUploadRequest", data)
      that.avatar = data["public_url"]
      console.log("i am in public url", data["public_url"])


      that.zone.run(() => {
        let request = new XMLHttpRequest();
        request.open('GET', im, true);
        request.responseType = 'blob';
        request.timeout = 360000;
        request.onload = function () {
          let reader = new FileReader();
          reader.readAsDataURL(request.response);
          reader.onload = function (e) {
            that.zone.run(async () => {
              console.log("error", e);
              var b1 = <string>reader.result
              let blob = b64toBlob(b1 .replace(/^data:image\/\w+;base64,/, ""), 'png');
              // let blob = b64toBlob(reader.result.replace(/^data:image\/\w+;base64,/, ""), 'jpg');
              await that.upload2awspart2(data["presigned_url"], blob, data["public_url"]);
            })

          }


          reader.onerror = function (e) {
            console.log('We got an error in file reader:::::::: ', e);
          };

          reader.onabort = function (event) {
            console.log("reader onabort", event)
          }

        };

        request.onerror = function (e) {
          console.log("** I got an error in XML Http REquest", e);
        };

        request.ontimeout = function (event) {
          console.log("xmlhttp ontimeout", event)
        }

        request.onabort = function (event) {
          console.log("xmlhttp onabort", event);
        }


        request.send();
      });
    }, async (err) => {
      let toast = await this.toastCtrl.create({
        message: "Unable to send request. Check wifi connection",
        duration: 6000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger",
        color:"danger"
      });
      await toast.present();
      console.log('getSignedUploadRequest timeout error: ', err);
    });

  }
  async upload2awspart2(url, blob, public_url) {
    await this.delay();
    let that = this;
    that.awsProvider.uploadFile(url, blob).subscribe(_result => {
      that.uploadurl.push(public_url);
      console.log("result that i got after uploading to aws", _result)
      // return "success";
      if (that.uploadurl.length == that.imageResponse.length) {
        that.uploadImage(that.uploadurl);
      }

    });
  }
  delay() {
    return new Promise(resolve => setTimeout(resolve, 300));
  }

  async uploadImage(imageurl) {
    const loading = await this.loadingController.create({
      // content:"loading data..."
    });
    await loading.present();
    this.api.post("faculty/picture.json", {
      album_id: this.id, images: imageurl,
      faculty_id: this.globalVar.current_user.information.id
    }
    ).subscribe(async data => {
      let toast = await this.toastCtrl.create({
        message: "Album Added successfully",
        duration: 6000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-success",
        color:"success"
      });
      await toast.present();
      console.log(data);
      this.navCtrl.navigateRoot('faculty-home');
      loading.dismiss();
      this.loading.dismiss();
    }, async err => {
      loading.dismiss();
      this.loading.dismiss();
      let toast = await this.toastCtrl.create({
        message: "Something went wrong",
        duration: 4000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger",
        color:"danger"
      });
      await toast.present();
    });

  }

}