import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentTrackingmapPageRoutingModule } from './parent-trackingmap-routing.module';

import { ParentTrackingmapPage } from './parent-trackingmap.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentTrackingmapPageRoutingModule
  ],
  declarations: [ParentTrackingmapPage]
})
export class ParentTrackingmapPageModule {}
