import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParentLeavePage } from './parent-leave.page';

const routes: Routes = [
  {
    path: '',
    component: ParentLeavePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParentLeavePageRoutingModule {}
