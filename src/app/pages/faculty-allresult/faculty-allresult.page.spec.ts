import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacultyAllresultPage } from './faculty-allresult.page';

describe('FacultyAllresultPage', () => {
  let component: FacultyAllresultPage;
  let fixture: ComponentFixture<FacultyAllresultPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacultyAllresultPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacultyAllresultPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
