import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacultyResultuploadmarksPage } from './faculty-resultuploadmarks.page';

describe('FacultyResultuploadmarksPage', () => {
  let component: FacultyResultuploadmarksPage;
  let fixture: ComponentFixture<FacultyResultuploadmarksPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacultyResultuploadmarksPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacultyResultuploadmarksPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
