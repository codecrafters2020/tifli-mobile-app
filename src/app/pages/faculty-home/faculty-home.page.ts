import { Component, ViewChild } from '@angular/core';
import { NavController, IonSlides } from '@ionic/angular';
import { GlobalVars } from '../../providers/global-vars.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-faculty-home',
  templateUrl: './faculty-home.page.html',
  styleUrls: ['./faculty-home.page.scss'],
})
export class FacultyHomePage {

  @ViewChild('slides2') slides2: IonSlides;
  item='assets/img/bgcover.png';

  constructor(public navCtrl: NavController,
    public globalVar: GlobalVars,
    public storage: Storage,
    ) {
        console.log(this.globalVar.current_user)
  }
  slide2items=[
    {img:'assets/img/album recent.png'},
    {img:'assets/img/user recent.png'},
    {img:'assets/img/notice recent.png'},
    {img:'assets/img/event recent.png'},
  ]

  items=[
    { img: 'assets/img/attendance.png', title: 'Attendance', component: 'faculty-attendance' },
    { img: 'assets/img/diary.png', title: 'Diary', component: 'faculty-newdiary' },
    // {img:'assets/img/home/007.png',title:'Time Table',component:'TimeTablePage'},
    { img: 'assets/img/notice.png', title: 'Notice', component: 'faculty-newnotice' },
    { img: 'assets/img/leave.png', title: 'Leave', component: 'faculty-leave' },
    { img: 'assets/img/meeting.png', title: 'Teacher Parent Interactions', component: 'faculty-ptm' },
    { img: 'assets/img/events.png', title: 'Events', component: 'events' },
    { img: 'assets/img/album.png', title: 'Album', component: 'album' },
    { title: 'Videos', component: 'faculty-allvideo', img: 'assets/img/videos.png', icon: '' },
    { title: 'Resources', component: 'faculty-allresources', img: 'assets/img/resources.png', icon: '' },
    { title: 'Result', component: 'faculty-allresult', img: 'assets/img/result.png', icon: '' },
    { title: 'Progress', component: 'faculty-allprogress', img: 'assets/img/progresstile.png', icon: '' },
    { title: 'Time Table', component: 'faculty-timetable', img: 'assets/img/timetable.png', icon: '' },
    { title: 'Zoom', component: 'login', img: 'assets/img/zoom1.png', icon: '' },
  ]

  // go to another Page
  openPage(page){
    this.navCtrl.navigateRoot(page);
  }
  logout()
  {
    this.globalVar.current_user = "";
    this.storage.remove("currentuser").then(() => {
      this.openPage('LandPage');
    //  this.nav.setRoot("LandPage");
    });
  }


}
