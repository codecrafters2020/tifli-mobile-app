import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParentTrackingmapPage } from './parent-trackingmap.page';

const routes: Routes = [
  {
    path: '',
    component: ParentTrackingmapPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParentTrackingmapPageRoutingModule {}
