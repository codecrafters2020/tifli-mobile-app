import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AlbumDetailFacultyPageRoutingModule } from './album-detail-faculty-routing.module';

import { AlbumDetailFacultyPage } from './album-detail-faculty.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AlbumDetailFacultyPageRoutingModule
  ],
  declarations: [AlbumDetailFacultyPage]
})
export class AlbumDetailFacultyPageModule {}
