import { Component,OnInit } from '@angular/core';
import { NavController , ToastController} from '@ionic/angular';
import { GlobalVars } from '../../providers/global-vars.service';
import { Api } from '../../providers/api/api.service';
import { StreamingMedia, StreamingVideoOptions } from '@ionic-native/streaming-media/ngx';
import * as moment from 'moment';

@Component({
  selector: 'app-parent-allvideo',
  templateUrl: './parent-allvideo.page.html',
  styleUrls: ['./parent-allvideo.page.scss'],
})
export class ParentAllvideoPage implements OnInit{

  allvideo: any;
  classsectiontosend: any;
  students: any;
  filtermonthwise;
  constructor(
    public navCtrl: NavController,
    public globalVar: GlobalVars,
    public api: Api,
    private streamingMedia: StreamingMedia,
    public toastCtrl: ToastController,

) {
  this.classsectiontosend=[];

  }

  ngOnInit() {
    var date=new Date();
    var d=moment(date);
    this.students = this.globalVar.current_user.students;
    for(var i=0; i<this.students.length;i++)
    {
    //  this.classsectionid= {classsectionid:this.students[i].class_section.id}
      this.classsectiontosend.push(this.students[i].class_section.id);
      }
      this.filtermonthwise=d.month()+1;
      this.loadResource(this.classsectiontosend);


  }
  loadResource(classSection)
  {
    this.allvideo=[];
    this.api.get("parent/videos/show_all_videos.json",{class_section_ids:classSection,month:this.filtermonthwise}).subscribe(async data=>{
      if(!data["message"])
      {
        this.allvideo=data;
      }
      else{
        let toast = await this.toastCtrl.create({
          message:"No Videos Found.",
          duration: 4000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger",
          color:"danger"
        });
        await toast.present();
      }    
    });
  }
  playvideo(videourl)
  {
    let options: StreamingVideoOptions = {
      successCallback: () => { console.log('Video played') },
      errorCallback: (e) => { console.log('Error streaming') },
    //  orientation: 'landscape',
      shouldAutoClose: true,
      controls: true
    };

    this.streamingMedia.playVideo(videourl, options);

    // this.videoPlayer.play(videourl,{
    //   volume:0.7
    // });
    // setTimeout(data=>{
    //     this.videoPlayer.close();
    // },3000);

  }

}
