import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ParentAlbumdetailPage } from './parent-albumdetail.page';

describe('ParentAlbumdetailPage', () => {
  let component: ParentAlbumdetailPage;
  let fixture: ComponentFixture<ParentAlbumdetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentAlbumdetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ParentAlbumdetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
