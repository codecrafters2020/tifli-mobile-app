import { Component,OnInit } from '@angular/core';
// import { NavController, NavParams, ModalController, ViewController } from '@ionic/angular';
import { NavController,ModalController } from '@ionic/angular';
import { ParentParamsService } from 'src/app/providers/parent-params.service';
import { FacultyParamsService } from '../../providers/faculty-params.service';
import { FacultyNewprogressPage } from '../faculty-newprogress/faculty-newprogress.page'
@Component({
  selector: 'app-parent-albumdetail',
  templateUrl: './parent-albumdetail.page.html',
  styleUrls: ['./parent-albumdetail.page.scss'],
})
export class ParentAlbumdetailPage implements OnInit{
  images: any;
  albumname: any;

  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public parentparam: ParentParamsService,
    public facutyparam: FacultyParamsService
    ) {
      this.images=this.parentparam.album_images;
      this.albumname = this.images.albumname;

    }

  ngOnInit() {
    console.log('ionViewDidLoad ParentAlbumDetailPage');
  }
  async openFull(url)
  {
    this.facutyparam.albummodal_img = this.images;
//    this.navCtrl.navigateRoot('faculty-newprogress');
  //  this.navCtrl.push("DocumentImagePage",{img:this.images.images,album:'yes'});
    let showgrid = await this.modalCtrl.create({
      component: FacultyNewprogressPage,
      componentProps: { player: this.images , index:url}
    });
        showgrid.present();
  }
  previouspage()
  {
      this.navCtrl.navigateRoot('parent-album');
  }

}
