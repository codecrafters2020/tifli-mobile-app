import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ParentDiarydetailPage } from './parent-diarydetail.page';

describe('ParentDiarydetailPage', () => {
  let component: ParentDiarydetailPage;
  let fixture: ComponentFixture<ParentDiarydetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentDiarydetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ParentDiarydetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
