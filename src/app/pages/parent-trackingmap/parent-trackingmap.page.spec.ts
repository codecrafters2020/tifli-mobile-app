import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ParentTrackingmapPage } from './parent-trackingmap.page';

describe('ParentTrackingmapPage', () => {
  let component: ParentTrackingmapPage;
  let fixture: ComponentFixture<ParentTrackingmapPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentTrackingmapPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ParentTrackingmapPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
