import { Component, OnInit } from '@angular/core';
import { NavController, ToastController, LoadingController } from '@ionic/angular';
import { Api } from '../../providers/api/api.service';
import { GlobalVars } from '../../providers/global-vars.service';
@Component({
  selector: 'app-faculty-newptm',
  templateUrl: './faculty-newptm.page.html',
  styleUrls: ['./faculty-newptm.page.scss'],
})
export class FacultyNewptmPage implements OnInit {
  student: any;
  studentname: any;
  subject: any;
  description;
  today: any;
  studentid;
  from_date;
  unformated_pickup_time;
  parentid: any;
  loading: any;
  constructor(
    public navCtrl: NavController,
    public api: Api,
    public globalVar: GlobalVars,
    public toastCtrl: ToastController,
    private loadingController: LoadingController,

  ) { }

  ngOnInit() {
    console.log('ionViewDidLoad PtmRequestappointmentPage');
    this.today = Date.now()
  }
  disabled() {
    if (!this.description) {
      return true;
    }
    else {
      return false;
    }
  }
  cancel_faculty() {
    // this.navCtrl.pop();
    this.navCtrl.navigateRoot('faculty-ptm');
  }
  async uploadfromfaculty() {
    this.loading = await this.loadingController.create({
    });
    await this.loading.present();
    for (var i = 0; i < this.globalVar.current_user.section_students.length; i++) {
      if (this.globalVar.current_user.section_students[i].id == this.studentid) {
        this.parentid = this.globalVar.current_user.section_students[i].parent_id
        this.uploadrequest2(this.parentid);
        break;
      }
    }
  }
  uploadrequest2(parentid) {
    this.api.post("faculty/parent_teacher_meetings.json", {
      parent_id: parentid,
      faculty_id: this.globalVar.current_user.information.id,
      student_id: this.studentid,
      description: this.description,
      subject: this.subject,
      appointment_date: this.from_date,
      appointment_time: this.unformated_pickup_time
    }).subscribe(async data => {
      console.log(data);
      this.loading.dismiss();
      this.navCtrl.navigateRoot("faculty-home");
      let toast = await this.toastCtrl.create({
        message: "PTM Request Sent",
        duration: 4000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-success"
      });
      await toast.present();
    }, async err => {
      this.loading.dismiss();
      let toast = await this.toastCtrl.create({
        message: "Something went wrong.",
        duration: 4000,
        position: 'top',
        showCloseButton: true,
        closeButtonText: "x",
        cssClass: "toast-danger"
      });
      await toast.present();
    });
  }
  previouspage()
  {
    this.navCtrl.navigateRoot('faculty-ptm');
  }

}
