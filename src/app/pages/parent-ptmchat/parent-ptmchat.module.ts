import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParentPtmchatPageRoutingModule } from './parent-ptmchat-routing.module';

import { ParentPtmchatPage } from './parent-ptmchat.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ParentPtmchatPageRoutingModule
  ],
  declarations: [ParentPtmchatPage]
})
export class ParentPtmchatPageModule {}
