import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacultyNewprogressPage } from './faculty-newprogress.page';

describe('FacultyNewprogressPage', () => {
  let component: FacultyNewprogressPage;
  let fixture: ComponentFixture<FacultyNewprogressPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacultyNewprogressPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacultyNewprogressPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
