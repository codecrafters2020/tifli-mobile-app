import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ParentSurveyPage } from './parent-survey.page';

describe('ParentSurveyPage', () => {
  let component: ParentSurveyPage;
  let fixture: ComponentFixture<ParentSurveyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentSurveyPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ParentSurveyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
