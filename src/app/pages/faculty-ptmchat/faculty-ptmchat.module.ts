import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacultyPtmchatPageRoutingModule } from './faculty-ptmchat-routing.module';

import { FacultyPtmchatPage } from './faculty-ptmchat.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacultyPtmchatPageRoutingModule
  ],
  declarations: [FacultyPtmchatPage]
})
export class FacultyPtmchatPageModule {}
