import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FacultyIndividualdiaryPageRoutingModule } from './faculty-individualdiary-routing.module';

import { FacultyIndividualdiaryPage } from './faculty-individualdiary.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FacultyIndividualdiaryPageRoutingModule
  ],
  declarations: [FacultyIndividualdiaryPage]
})
export class FacultyIndividualdiaryPageModule {}
