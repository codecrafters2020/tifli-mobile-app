import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FacultyAllvideoPage } from './faculty-allvideo.page';

const routes: Routes = [
  {
    path: '',
    component: FacultyAllvideoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FacultyAllvideoPageRoutingModule {}
