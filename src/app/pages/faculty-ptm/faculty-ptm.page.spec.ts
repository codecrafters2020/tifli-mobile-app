import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacultyPtmPage } from './faculty-ptm.page';

describe('FacultyPtmPage', () => {
  let component: FacultyPtmPage;
  let fixture: ComponentFixture<FacultyPtmPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacultyPtmPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacultyPtmPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
