import { Component, NgZone, ChangeDetectorRef,OnInit } from '@angular/core';
import { ActionSheetController, NavController, ToastController, Platform, LoadingController } from '@ionic/angular';
import { Api } from '../../providers/api/api.service';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/camera/ngx';
import { AwsProvider } from '../../providers/aws.service';
import b64toBlob from "b64-to-blob";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GlobalVars } from '../../providers/global-vars.service';
import { File } from '@ionic-native/file/ngx';

@Component({
  selector: 'app-parent-newleave',
  templateUrl: './parent-newleave.page.html',
  styleUrls: ['./parent-newleave.page.scss'],
})
export class ParentNewleavePage implements OnInit{

  students: any;
  today: number;
  subject: any;
  description: any;
  from_date;
  to_date;
  images: any;
  imagelength: boolean;
  studentname;
  avatar: any;
  headers: any;
  private win: any = window;
  bgimage='assets/img/roundedRectangle.png';
  constructor(
    public navCtrl: NavController,
    public api: Api,
    public camera: Camera,
    private actionSheetController: ActionSheetController,
    private awsProvider: AwsProvider,
    public zone: NgZone,
    private file: File,
    public toastCtrl: ToastController,
    public http: HttpClient,
    public globalVar: GlobalVars,
    public platform: Platform,
    private loadingController: LoadingController,
  ) {
    // this.students = this.navParams.get("student");
    //this.studentname = this.students.first_name + this.students.last_name;
  }

  ngOnInit() {
    console.log('ionViewDidLoad ParentNewleavePage');
    this.today = Date.now();
  }

  async post_leave() {
    const loading = await this.loadingController.create({
    });
    await loading.present();

    if (this.images != null) {
      let that = this;

      let ext = 'jpg';
      let type = 'image/jpeg'
      let newName = this.awsProvider.randomString(6) + new Date().getTime() + '.' + ext;

      this.awsProvider.getSignedUploadRequest(newName, ext, type).subscribe(data => {
        console.log("Success in getSignedUploadRequest", data)
        that.avatar = data["public_url"]
        console.log("i am in public url", data["public_url"])

        that.zone.run(() => {
          let request = new XMLHttpRequest();
          request.open('GET', this.images, true);
          request.responseType = 'blob';
          request.timeout = 360000;
          request.onload = function () {
            let reader = new FileReader();
            reader.readAsDataURL(request.response);
            reader.onload = function (e) {
              that.zone.run(() => {
                console.log("error", e);
                var b1 = <string>reader.result
                let blob = b64toBlob(b1 .replace(/^data:image\/\w+;base64,/, ""), 'jpg');
                // let blob = b64toBlob(reader.result.replace(/^data:image\/\w+;base64,/, ""), 'jpg');

                that.awsProvider.uploadFile(data["presigned_url"], blob).subscribe(_result => {

                  console.log("result that i got after uploading to aws", _result)
                  that.api.post("parent/leave_applications.json", {
                    student_id: that.globalVar.student.id,
                    leave_reason: that.description, start_date: that.from_date, end_date: that.to_date,
                    leave_status: "1", subject: that.subject, image_url: that.avatar
                  }).subscribe(async data => {
                    console.log(data);
                    let toast = await that.toastCtrl.create({
                      message: "Leave Request Sent",
                      duration: 4000,
                      position: 'top',
                      showCloseButton: true,
                      closeButtonText: "x",
                      cssClass: "toast-success",
                      color:"success"
                    });
                    await toast.present();
                    that.navCtrl.navigateRoot("parent-home");
                    loading.dismiss();
                  }, async err => {
                    let toast = await that.toastCtrl.create({
                      message: "Unable to send request.",
                      duration: 4000,
                      position: 'top',
                      showCloseButton: true,
                      closeButtonText: "x",
                      cssClass: "toast-danger",
                      color:"danger"
                    });
                    loading.dismiss();
                    await toast.present();
                  });
                });
              })

            }

            reader.onerror = function (e) {
              console.log('We got an error in file reader:::::::: ', e);
              loading.dismiss();
            };

            reader.onabort = function (event) {
              console.log("reader onabort", event)
              loading.dismiss();
            }

          };

          request.onerror = function (e) {
            console.log("** I got an error in XML Http REquest", e);
            loading.dismiss();
          };

          request.ontimeout = function (event) {
            console.log("xmlhttp ontimeout", event)
            loading.dismiss();
          }

          request.onabort = function (event) {
            console.log("xmlhttp onabort", event);
            loading.dismiss();
          }


          request.send();
        });
      }, async (err) => {
        let toast = await this.toastCtrl.create({
          message: "Unable to send request. Check wifi connection",
          duration: 6000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger",
          color:"danger"
        });
        await toast.present();
        loading.dismiss();
        console.log('getSignedUploadRequest timeout error: ', err);
      });

    }
    else {
      this.api.post("parent/leave_applications.json", {
        student_id: this.globalVar.student.id,
        leave_reason: this.description, start_date: this.from_date, end_date: this.to_date,
        leave_status: "1", subject: this.subject, image_url: this.avatar
      }).subscribe(async data => {
        console.log(data);
        this.navCtrl.navigateRoot("parent-home");
        let toast = await this.toastCtrl.create({
          message: "Leave Request Sent",
          duration: 4000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-success",
          color:"success"
        });
        await toast.present();
        loading.dismiss();
      }, async err => {
        let toast = await this.toastCtrl.create({
          message: "Unable to send request.",
          duration: 4000,
          position: 'top',
          showCloseButton: true,
          closeButtonText: "x",
          cssClass: "toast-danger",
          color:"danger"
        });
        await toast.present();
        loading.dismiss();
      });
    }

  }
  disabled() {
    if (this.to_date == null || this.to_date == undefined) {
      return true;
    }
    else {
      return false;
    }
  }
  cancel() {
    //this.navCtrl.pop();
    this.navCtrl.navigateRoot('parent-leave');
  }

  takePicture(sourceType) {
    const options: CameraOptions = {
      quality: 100,
      correctOrientation: true,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: sourceType
    }

    this.camera.getPicture(options).then((imageData) => {
      this.images = this.win.Ionic.WebView.convertFileSrc(imageData);


    });
  }

  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          //this.selectPhoto();
          //  this.images="../../assets/img/female-logo.jpg"
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.takePicture(this.camera.PictureSourceType.CAMERA);
          //this.takePhoto();
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }
  previouspage()
  {
    this.navCtrl.navigateRoot('parent-leave');
  }
}
