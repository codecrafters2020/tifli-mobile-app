import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FacultyNewresultPage } from './faculty-newresult.page';

describe('FacultyNewresultPage', () => {
  let component: FacultyNewresultPage;
  let fixture: ComponentFixture<FacultyNewresultPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacultyNewresultPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FacultyNewresultPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
